﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Level
{
    public string name;

    public int star1Requirement;
    public int star2Requirement;
    public int star3Requirement;

    public int waves;
    public string dialogue;
    public string waveOverride;

    public ChestType chestReward;
    public BlockSettings blockSettings;

}
