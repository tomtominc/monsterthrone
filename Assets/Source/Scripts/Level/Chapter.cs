﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Chapter
{
    public string name;
    public List < Level > levels;

    public Level getLevel(string p_level)
    {
        return levels.Find(x => x.name == p_level);
    }
}
