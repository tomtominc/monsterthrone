﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class SpritePostProcessor : AssetPostprocessor
{
    public const float DEFAULT_FRAME_TIME = 0.066666F;

    private void OnPreprocessTexture()
    {
        if (isGif(assetPath))
        {
            //TODO: GIF STUFF
        }
        else
        {
            TextureImporter l_textureImporter = assetImporter as TextureImporter;
            l_textureImporter.spritePixelsPerUnit = 32;
            l_textureImporter.textureType = TextureImporterType.Sprite;
            l_textureImporter.textureCompression = TextureImporterCompression.Uncompressed;
            l_textureImporter.filterMode = FilterMode.Point;
            l_textureImporter.spriteImportMode = isAnimated() ? SpriteImportMode.Multiple : SpriteImportMode.Single;
            l_textureImporter.mipmapEnabled = false;

            TextureImporterSettings textureSettings = new TextureImporterSettings();
            l_textureImporter.ReadTextureSettings(textureSettings);
            textureSettings.spriteMeshType = SpriteMeshType.FullRect;
            textureSettings.spriteExtrude = 0;
            l_textureImporter.SetTextureSettings(textureSettings);
        }
    }

    public void OnPostprocessTexture(Texture2D p_texture)
    {
        string l_name = getAssetName();

        if (!l_name.Contains("_"))
            return;
        
        string[] l_nameSplit = l_name.Split('_');
        string l_sizeFormat = l_nameSplit[1];

        if (l_sizeFormat.Split('x').Length < 2)
            return;

        int l_width = int.Parse(l_sizeFormat.Split('x')[0]);
        int l_height = int.Parse(l_sizeFormat.Split('x')[1]);

        int l_rows = p_texture.height / l_height;
        int l_columns = p_texture.width / l_width;

        List<SpriteMetaData> l_metas = new List<SpriteMetaData>();

        for (int r = 0; r < l_rows; r++)
        {
            for (int c = 0; c < l_columns; c++)
            {
                SpriteMetaData l_meta = new SpriteMetaData();
                l_meta.rect = new Rect(c * l_width, r * l_height, l_width, l_height);
                l_meta.name = string.Format("{0}x{1}", c, r);
                l_metas.Add(l_meta);
            }
        }

        TextureImporter l_textureImporter = assetImporter as TextureImporter;
        l_textureImporter.spritesheet = l_metas.ToArray();
    }

    private bool isGif(string p_path)
    {
        return p_path.Contains(".gif");
    }

    private bool isAnimated()
    {
        return getAssetName().Contains("@");
    }

    private string getAssetName()
    {
        string[] l_pathSplit = assetPath.Split('/');
        return l_pathSplit[l_pathSplit.Length - 1].Replace(".png", "");
    }

    private string getAnimationAssetSavePath()
    {
        return "Assets/Source/Animations/";
    }
}
