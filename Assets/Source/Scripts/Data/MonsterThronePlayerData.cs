﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonsterThronePlayerData : PlayerData
{
    protected const string PLAYER_DATA_CONTENT = "PLAYER_DATA_CONTENT";

    #region Members

    protected PlayerDataContent m_content;

    #endregion

    #region Properties

    public PlayerDataContent content
    {
        get { return m_content; }
    }

    #endregion

    protected override void configureData()
    {
        if (m_newlyCreatedAccount)
        {
            createNewPlayer();
        }
        else if (m_data.ContainsKey(PLAYER_DATA_CONTENT))
        {
            m_content = PlayerDataContent.getData(m_data[PLAYER_DATA_CONTENT]);
        }
        else
        {
            // fallback to just creating a new player
            createNewPlayer();
        }

        updateCardStates(m_serviceProvider);

        Debug.LogFormat("SUCCESS LOGGING IN PLAYER: {0}", m_content.ToString());
    }

    protected void createNewPlayer()
    {
        // create new data content
        m_content = new PlayerDataContent();

        updateCardStates(m_serviceProvider);

        // set the player to level 1 
        m_content.level = 1;
        m_content.experience = 0;

        // unlock the chiblets
        CardStateModel l_chibletAlpha = m_content.cardStateDatabase.getCard("Chiblet Alpha");
        CardStateModel l_chibletBeta = m_content.cardStateDatabase.getCard("Chiblet Beta");
        l_chibletBeta.unlocked = true;
        l_chibletAlpha.unlocked = true;

        // create a new deck with the chiblets - this is temporary until the player picks their starter monster
        m_content.deck = new DeckModel();
        m_content.deck.contents = new List < string >()
        {
            "Chiblet Alpha", "Chiblet Beta"    
        };

        // configure chest slot data
        m_content.chestSlots = new List < ChestSlotData>
        {
            new ChestSlotData(), new ChestSlotData(), new ChestSlotData(), new ChestSlotData()
        };

        // configure the daily chest model
        m_content.dailyChestModel = new DailyChestModel();
        m_content.dailyChestModel.lastOpened = DateTime.Now.AddHours(-TimeUtils.getHoursForFreeChest());

        // configure the players inventory
        m_content.inventory = new Inventory();
        m_content.inventory.addItem("coin", 0);
        m_content.inventory.addItem("gem", 0);
        m_content.inventory.addItem("princessToken", 0);

        // set up the player progress
        m_content.playerProgress = new PlayerProgress();
        m_content.playerProgress.currentCastle = "Tutorial";
        m_content.playerProgress.currentChapter = "The Basics";
        m_content.playerProgress.currentLevel = "The Queens Orb";

        // set up the tutorial model
        m_content.tutorial = new TutorialModel();
        m_content.tutorial.complete = false;
        m_content.tutorial.tutorialProgress = 0;

    }

    protected void setupObjects()
    {
        if (m_content.inventory == null)
            m_content.inventory = new Inventory();
        if (m_content.playerProgress == null)
            m_content.playerProgress = new PlayerProgress();
    }

    protected void updateCardStates(GameServiceProvider p_serviceProvider)
    {
        MonsterThroneGameModel l_model = ((MonsterThroneTitleService)p_serviceProvider.titleService).gameModel;

        if (l_model == null)
        {
            Debug.LogWarning("game model is null!");
            return;
        }

        CardDatabaseData l_database = l_model.cardDatabase;

        if (l_database == null)
        {
            Debug.LogWarning("database is null!");
            return;
        }

        List < Card > l_cards = l_database.cards;

        if (m_content.cardStateDatabase == null)
            m_content.cardStateDatabase = new CardStateDatabase();
        
        foreach (var l_card in l_cards)
        {
            if (m_content.cardStateDatabase.contains(l_card.name) == false)
            {
                CardStateModel l_cardState = new CardStateModel();
                l_cardState.initialize(m_serviceProvider);
                l_cardState.setCardId(l_card.name);
                m_content.cardStateDatabase.cardStates.Add(l_cardState);
            }
        }

        foreach (CardStateModel l_cardState in m_content.cardStateDatabase.cardStates)
        {
            l_cardState.initialize(m_serviceProvider);
        }

    }

    public override Dictionary<string, string> getSaveData()
    {
        return new Dictionary<string,string>()
        {
            { PLAYER_DATA_CONTENT, content.ToString() }  
        };
    }
}