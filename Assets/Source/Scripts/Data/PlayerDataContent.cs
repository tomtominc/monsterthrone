﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

[Serializable]
public class PlayerDataContent
{
    public int level;
    public int experience;
    public TutorialModel tutorial;
    public DeckModel deck;
    public List<ChestSlotData> chestSlots;
    public CardStateDatabase cardStateDatabase;
    public DailyChestModel dailyChestModel;
    public PlayerProgress playerProgress;
    public Inventory inventory;

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }

    public static PlayerDataContent getData(string p_json)
    {
        return  JsonConvert.DeserializeObject < PlayerDataContent >(p_json);
    }
}