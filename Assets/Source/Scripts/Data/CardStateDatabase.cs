﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardStateDatabase
{
    public List < CardStateModel > cardStates;

    public bool contains(string p_id)
    {
        if (cardStates == null)
            cardStates = new List < CardStateModel >();
        
        return cardStates.Exists(l_card => l_card.id == p_id);
    }

    public CardStateModel getCard(string p_id)
    {
        return cardStates.Find(l_card => l_card.id == p_id);
    }
}
