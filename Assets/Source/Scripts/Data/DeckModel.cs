﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DeckModel
{
    public string leader;
    public List < string > contents;

    public bool contains(string p_cardName)
    {
        return contents.Contains(p_cardName);
    }

    public void swapCards(string p_adding, string p_removing)
    {
        int l_index = contents.IndexOf(p_removing);
        contents[l_index] = p_adding;
    }

    public void setCardAsFirst(string p_id)
    {
        if (contains(p_id))
        {
            int index = contents.IndexOf(p_id);
            string l_id = contents[0];
            contents[index] = l_id;
            contents[0] = p_id;
        }
    }

}
