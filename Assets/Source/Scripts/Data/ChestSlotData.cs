﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;

public enum ChestSlotStatus
{
    EMPTY,
    WAIT,
    UNLOCKING,
    READY
}

public enum ChestType
{
    MONSTER_CHEST,
    EPIC_CHEST,
    LEGENDARY_CHEST,
    DAILY_CHEST,
    PRINCESS_CHEST
}

[SerializeField]
public class ChestSlotData
{
    public ChestSlotStatus chestStatus;
    public ChestSlotStatus lastStatus;
    public ChestType chestType;
    public DateTime timeUntilUnlock;

    public ChestSlotData()
    {
    }

    public void startUnlock(DateTime p_timeUntilUnlock)
    {
        timeUntilUnlock = p_timeUntilUnlock;
    }

    public int getUnlockEarlyCost()
    {
        return TimeUtils.getGemCostOfTime((int)getTimeLeft().TotalMinutes);
    }

    public TimeSpan getTimeLeft()
    {
        return timeUntilUnlock - DateTime.UtcNow;
    }

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }
}
