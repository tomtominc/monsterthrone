﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceDataUpdater : MonoBehaviour
{
    [SerializeField][Sirenix.OdinInspector.BoxGroup("Difficulty Settings")]
    private DifficultySettings m_difficultySettings;
    [SerializeField][Sirenix.OdinInspector.BoxGroup("Castle Settings")]
    private List<CastleSettings> m_castleSettings;
}
