﻿using UnityEngine;

public enum Element
{
    NORMAL,
    FIRE,
    ELECTRIC,
    PLANT,
    ICE,

}

[System.Serializable]
public class DamageInfo
{
    public float delay;
    public int amount;
    public Element element;
    public int range;
}
