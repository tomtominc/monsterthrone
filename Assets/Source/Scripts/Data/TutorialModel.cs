﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TutorialModel
{
    public bool complete;
    public int tutorialProgress;
}
