﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using DG.DOTweenEditor.Core;

[CustomEditor(typeof(CardDatabase))]
public partial class CardDatabaseEditor : Editor
{
    private CardDatabase cardDatabase;
    private CardType currentCardType;
    private Card currentCard;

    private CardDatabase copyDatabase;
    private CardType copyCardType;

    private int selectionId = 0;

    private void OnEnable()
    {
        cardDatabase = (CardDatabase)target;

        if (cardDatabase.cards == null)
            cardDatabase.cards = new List < Card >();
    }

    public override void OnInspectorGUI()
    {
        List < Card > cards = cardDatabase.cards;

        EditorGUIExtensions.BeginContent(Color.white);
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                if (GUILayout.Button("<", EditorStyles.toolbarButton))
                {
                    selectionId = selectionId.Wrap(-1, cards.Count);
                }

                EditorGUILayout.LabelField(string.Format("{0}/{1}", selectionId + 1, cards.Count), EditorStyles.centeredGreyMiniLabel);

                if (GUILayout.Button(">", EditorStyles.toolbarButton))
                {
                    selectionId = selectionId.Wrap(1, cards.Count);
                }

               
            }
            EditorGUILayout.EndHorizontal();

            if (cards.Count <= 0)
            {
                EditorGUILayout.HelpBox("NO CARDS OF TYPE {0} PLEASE USE THE '+' BUTTON TO ADD SOME.", MessageType.Warning);
            }
            else
            {
                currentCard = cards[selectionId];
                DrawCard(currentCard);
            }

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            {
                if (GUILayout.Button("+", EditorStyles.toolbarButton))
                {
                    Card card = new Card();
                    cardDatabase.cards.Add(card);

                    Undo.RecordObject(target, "Adding Card");
 
                }

                if (GUILayout.Button("-", EditorStyles.toolbarButton))
                {
                    cardDatabase.cards.Remove(currentCard);

                    Undo.RecordObject(target, "Removing Card");
                }

            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUIExtensions.EndContent();

        EditorPrefs.SetInt(string.Format("SELECTION_ID_{0}", cardDatabase.name), selectionId);

        EditorUtility.SetDirty(target);
    }
    
}
