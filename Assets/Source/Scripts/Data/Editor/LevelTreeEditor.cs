﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public partial class CardDatabaseEditor
{
    public void DrawLevelTree(Card card)
    {
        
        if (card.levelTree == null)
            card.levelTree = new List<CardLevel>();
        
        int removeId = -1;

        if (GUILayout.Button("+", EditorStyles.miniButtonRight))
        {
            card.levelTree.Add(new CardLevel());
            Undo.RecordObject(target, "Add Tree");
            EditorUtility.SetDirty(target);
        }

        for (int i = 0; i < card.levelTree.Count; i++)
        {
            if (EditorGUIExtensions.DrawHeader(string.Format("Level: {0}", i + 1), GUILayout.ExpandWidth(true)))
            {
                CardLevel cardLevel = card.levelTree[i];
                DrawCardLevel(cardLevel);   

                if (GUILayout.Button("-", EditorStyles.toolbarButton))
                {
                    removeId = i;
                }
            }
        }

        if (removeId > -1)
        {
            card.levelTree.RemoveAt(removeId);
            Undo.RecordObject(target, "Remove Tree");
            EditorUtility.SetDirty(target);
        }

       
    }

    public void DrawCardLevel(CardLevel level)
    {
        EditorGUIExtensions.BeginContent(EditorColor.greenyellow);
        level.hitPoints = EditorGUILayout.IntField("HP", level.hitPoints);
        level.power = EditorGUILayout.IntField("Power", level.power);
        level.healAmount = EditorGUILayout.IntField("Heal Amount", level.healAmount);
        level.count = EditorGUILayout.IntField("Count", level.count);
        level.turns = EditorGUILayout.IntField("Turns", level.turns);
        level.percent = EditorGUILayout.FloatField("Percent", level.percent);
        EditorGUIExtensions.EndContent();
    }
}
