﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public partial class CardDatabaseEditor
{
    public void DrawCard(Card card)
    {
        if (!EditorGUIExtensions.DrawHeader(card.name, GUILayout.ExpandWidth(true)))
            return;

        //Color cardColor = card.CardType == CardType.Block ? Color.blue : Color.yellow;

        EditorGUIExtensions.BeginContent(Color.white);

        card.name = EditorGUILayout.TextField("Name", card.name);
        
        card.cardType = (CardType)EditorGUILayout.
            EnumPopup("Card Type", card.cardType);

        switch (card.cardType)
        {
            case CardType.Block:
                DrawBlockGUI(card);
                break;
            case CardType.Monster:
                DrawUnitGUI(card);
                break;
            case CardType.Ability:
                DrawAbilityGUI(card);
                break;
        }

        EditorGUIExtensions.EndContent();
    }

    private void DrawAbilityGUI(Card card)
    {
        DrawGenericProperties(card);

        card.cost = EditorGUILayout.IntField("Cost", card.cost);
        card.prefab = EditorGUILayout.TextField("Prefab", card.prefab);

        drawDescription(card);

        DrawLevelTree(card);
    }

    private void DrawBlockGUI(Card card)
    {
        DrawGenericProperties(card);

        card.prefab = EditorGUILayout.TextField("Prefab", card.prefab);

        drawDescription(card);

        drawDamageInfo(card);
    }

    private void DrawUnitGUI(Card card)
    {
        DrawGenericProperties(card);

        card.cost = EditorGUILayout.IntField("Cost", card.cost);
        card.attackEffect = EditorGUILayout.TextField("Attack Effect", card.attackEffect);
        drawAbilityProperty(card);

        drawDescription(card);
        DrawLevelTree(card);


    }

    private void DrawGenericProperties(Card card)
    {
        card.portrait = EditorGUILayout.TextField("Portrait", card.portrait);
        card.animationAsset = EditorGUILayout.TextField("Animation Asset", card.animationAsset);
        
    }

    private void drawAbilityProperty(Card card)
    {
        card.abilityId = EditorGUILayout.TextField("AbilityId", card.abilityId);
    }

    private void drawDescription(Card card)
    {
        EditorStyles.textField.wordWrap = true;
        card.description = EditorGUILayout.TextField("Description", card.description, GUILayout.Height(80f));
        EditorStyles.textField.wordWrap = false;
    }

    private void drawDamageInfo(Card card)
    {
        if (card.damageInfo == null)
            card.damageInfo = new DamageInfo();
        
        card.damageInfo.amount = EditorGUILayout.IntField("Damage Amount", card.damageInfo.amount);
        card.damageInfo.delay = EditorGUILayout.FloatField("Damage Delay", card.damageInfo.delay);
        card.damageInfo.element = (Element)EditorGUILayout.EnumPopup("Damage Element", card.damageInfo.element);
        card.damageInfo.range = EditorGUILayout.IntField("Damage Range", card.damageInfo.range);
    }
}
