﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameDialogueData
{
    public List < string > dialogue;

    private Queue < string > m_dialogue;

    public void initialize()
    {
        m_dialogue = new Queue<string>();

        for (int i = 0; i < dialogue.Count; i++)
            m_dialogue.Enqueue(dialogue[i]);
    }

    public bool isEmpty()
    {
        return m_dialogue.Count <= 0;
    }

    public string getNext()
    {
        return m_dialogue.Dequeue();
    }
}
