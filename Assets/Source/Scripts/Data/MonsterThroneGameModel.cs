﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class MonsterThroneGameModel
{
    public const string MONSTER_DATABASE_KEY = "MONSTER-DATABASE";
    public const string ABILITY_DATABASE_KEY = "ABILITY-DATABASE";
    public const string BLOCK_DATABASE_KEY = "BLOCK-DATABASE";
    public const string DIFFICULTY_SETTINGS_KEY = "DIFFICULTY-SETTINGS";
    public const string CASTLE_SETTINGS_KEY = "CASTLE-SETTINGS";

    public CardDatabase monsterDatabase;
    public CardDatabase abilityDatabase;
    public CardDatabase blockDatabase;
    public DifficultySettings difficultySettings;
    public List < CastleSettings > castleSettings;
    public CardDatabaseData cardDatabase;

    public MonsterThroneGameModel()
    {
        cardDatabase = new CardDatabaseData();
        cardDatabase.cards = new List < Card >();
    }

    public CastleSettings getCastle(string p_castle)
    {
        return castleSettings.Find(x => x.name == p_castle);
    }

    public void createMonsterDatabase(string p_json)
    {
        monsterDatabase = JsonConvert.DeserializeObject<CardDatabase>(p_json);
        cardDatabase.cards.AddRange(monsterDatabase.cards);
    }

    public void createAbilityDatabase(string p_json)
    {
        abilityDatabase = JsonConvert.DeserializeObject<CardDatabase>(p_json);
        cardDatabase.cards.AddRange(abilityDatabase.cards);
    }

    public void createBlockDatabase(string p_json)
    {
        blockDatabase = JsonConvert.DeserializeObject<CardDatabase>(p_json);
        cardDatabase.cards.AddRange(blockDatabase.cards);
    }

    public void createDifficultySettings(string p_json)
    {
        difficultySettings = JsonConvert.DeserializeObject<DifficultySettings>(p_json);
    }

    public void createCastleSettings(string p_json)
    {
        castleSettings = JsonConvert.DeserializeObject < List < CastleSettings > >(p_json);
    }
}
