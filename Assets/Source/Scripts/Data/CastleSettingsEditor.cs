﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu]
public class CastleSettingsEditor : ScriptableObject
{
    public CastleSettings castleSettings;
}
