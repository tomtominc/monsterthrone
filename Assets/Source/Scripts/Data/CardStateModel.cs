﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class CardStateModel
{
    private GameServiceProvider m_serviceProvider;
    private CardDatabaseData m_cardDatabase;

    public string id;
    public int level = 1;
    public int experience;
    public bool unlocked;

    private int m_hitPointTokens;
    private int m_powerTokens;
    private CardStateModel m_ability;
    private CardStatusData m_cardStatusData;
    private bool m_hasPower;
    private bool m_hasHeal;
    private bool m_hasCount;
    private bool m_hasTurns;
    private bool m_hasPercent;
    private string m_description;

    #region Properties

    [JsonIgnore]
    public Card card
    {
        get
        { 
            return m_cardDatabase.GetCard(id); 
        }
    }

    [JsonIgnore]
    public CardLevel cardLevel
    {
        get { return card.GetLevelData(level); }
    }

    [JsonIgnore]
    public int power
    {
        get { return Mathf.Clamp(cardLevel.power + m_powerTokens, 0, cardLevel.power + 1); }
    }

    [JsonIgnore]
    public int hitPoints
    {
        get{ return Mathf.Clamp(cardLevel.hitPoints + m_hitPointTokens, 0, cardLevel.hitPoints + 1); }
    }



    [JsonIgnore]
    public int experienceForNextLevel
    {
        get { return Algorithm.getExperienceForCardLevel(this); }
    }

    [JsonIgnore]
    public CardStateModel abilityState
    {
        get
        {
            if (m_ability == null)
            {
                m_ability = new CardStateModel();
                m_ability.initialize(m_serviceProvider);
                m_ability.setCardId(card.abilityId);
                m_ability.level = level;
            }

            return m_ability; 
        }
    }

    [JsonIgnore]
    public bool upgradable
    {
        get { return experience >= experienceForNextLevel; }
    }

    [JsonIgnore]
    public CardStatusData cardStatusData
    {
        get
        { 
            if (m_cardStatusData == null)
                m_cardStatusData = new CardStatusData() { cardStatus = CardStatus.NORMAL };
            
            return m_cardStatusData; 
        }
        set { m_cardStatusData = value; }
    }

    #endregion

    public void initialize(GameServiceProvider p_serviceProvider)
    {
        m_serviceProvider = p_serviceProvider;
        m_cardDatabase = ((MonsterThroneTitleService)m_serviceProvider.titleService)
            .gameModel.cardDatabase;
    }

    public void setCardId(string p_id)
    {
        id = p_id;
        setDescription();
    }

    public void setDescription()
    {
        m_description = card.description;
        CardLevel l_level = cardLevel;

        if (l_level == null)
            return;
        
        m_hasPower = m_description.Contains("{power}");
        m_hasHeal = m_description.Contains("{heal}");
        m_hasCount = m_description.Contains("{count}");
        m_hasTurns = m_description.Contains("{turns}");
        m_hasPercent = m_description.Contains("{percent}");

        if (m_hasPower)
            m_description = m_description.Replace("{power}", l_level.power.ToString());
        if (m_hasHeal)
            m_description = m_description.Replace("{heal}", l_level.healAmount.ToString());
        if (m_hasCount)
            m_description = m_description.Replace("{count}", l_level.count.ToString());
        if (m_hasTurns)
            m_description = m_description.Replace("{turns}", l_level.turns.ToString());
        if (m_hasPercent)
            m_description = m_description.Replace("{percent}", (l_level.percent * 100f).ToString());
    }

    public CardLevel getLevelStats(int p_level)
    {
        return card.GetLevelData(p_level);
    }

    public string getDescription()
    {
        if (string.IsNullOrEmpty(m_description))
            setDescription();

        return m_description;
    }

    public bool getHasPower()
    {
        return m_hasPower;
    }

    public bool getHasHeal()
    {
        return m_hasHeal;
    }

    public bool getHasCount()
    {
        return m_hasCount;
    }

    public bool getHasTurns()
    {
        return m_hasTurns;
    }

    public bool getHasPercent()
    {
        return m_hasPercent;
    }

    public void modifyHitPoints(int p_modifier)
    {
        m_hitPointTokens += p_modifier;
    }

    public void modifyPower(int p_modifier)
    {
        m_powerTokens += p_modifier;
    }
  
}
