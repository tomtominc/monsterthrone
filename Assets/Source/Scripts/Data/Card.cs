﻿using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

[System.Serializable]
public class Card
{
    public int Id;
    public string name;
    public string portrait;
    public string animationAsset;
    public CardType cardType;
    public int cost;
    public List < CardLevel > levelTree;
    public string abilityId;
    public float spawnProbability = 1f;
    public string attackEffect;
    public string prefab;
    public string description;
    public DamageInfo damageInfo;

    [JsonIgnore]
    private GameObject m_prefab;
    [JsonIgnore]
    private SpriteAnimationAsset m_animationAsset;
    [JsonIgnore]
    private Sprite m_portrait;

    public GameObject getPrefab()
    {
        if (m_prefab == null)
            m_prefab = Resources.Load<GameObject>(prefab);
        return m_prefab;
    }

    public SpriteAnimationAsset getAnimationAsset()
    {
        if (m_animationAsset == null)
        {
            m_animationAsset = Resources.Load<SpriteAnimationAsset>(animationAsset);
        }
        return m_animationAsset;
    }

    public Sprite getPortrait()
    {
        if (m_portrait == null)
        {
            m_portrait = SpriteManager.getSprite(portrait);
        }
        
        return m_portrait;
    }

    public CardLevel GetLevelData(int p_level)
    {
        if (levelTree.Count < p_level)
        {
            return null;
        }
        return levelTree[p_level - 1];
    }
}

public enum CardStatus
{
    NORMAL,
    SLIMED,
    PARALYZED,
    BURNED,
    FROZEN,
    CURSED
}

public class CardStatusData
{
    public CardStatus cardStatus;
    public int turns;
    public int power;
}

[System.Serializable]
public class CardLevel
{
    public int hitPoints;
    public int healAmount;
    public int turns;
    public int count;
    public int power;
    public float percent;
}


