﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu()]
[System.Serializable]
public class CardDatabase : ScriptableObject
{
    [SerializeField]
    public List < Card > cards;

    public List < Card > GetCards(CardType cardType)
    {
        return cards.Where(card => card.cardType == cardType).ToList();
    }

    public Card GetCard(string cardId)
    {
        return cards.Find(x => x.name == cardId);
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}


public class CardDatabaseData
{
    public List < Card > cards;

    public List < Card > GetCards(CardType cardType)
    {
        return cards.Where(card => card.cardType == cardType).ToList();
    }

    public Card GetCard(string cardId)
    {
        Card l_card = cards.Find(x => x.name == cardId);

        if (l_card == null)
            l_card = cards.Find(x => x.name == "None");

        return l_card;
    }
}