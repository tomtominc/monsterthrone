﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerProgress
{
    public string currentCastle;
    public string currentChapter;
    public string currentLevel;

    public List < CastleProgress > castles;

    private void init()
    {
        if (castles == null)
            castles = new List < CastleProgress >();
    }

    public CastleProgress getCastleProgress(string p_castle)
    {
        init();

        CastleProgress l_castle = castles.Find(i => i.name == p_castle);

        if (l_castle == null)
        {
            l_castle = new CastleProgress();
            l_castle.name = p_castle;
            castles.Add(l_castle);
        }

        return l_castle;
    }
}

[System.Serializable]
public class CastleProgress
{
    public string name;
    public int stars;
    public List < ChapterProgress > chapters;

    private void init()
    {
        if (chapters == null)
            chapters = new List < ChapterProgress >();
    }

    public ChapterProgress getChapterProgress(string p_chapter)
    {
        init();
        ChapterProgress l_chapter = chapters.Find(i => i.name == p_chapter);

        if (l_chapter == null)
        {
            l_chapter = new ChapterProgress();
            l_chapter.name = p_chapter;
            chapters.Add(l_chapter);
        }

        return l_chapter;

    }
}

[System.Serializable]
public class ChapterProgress
{
    
    public string name;
    public int stars;
    public List < LevelProgress > levels;

    private void init()
    {
        if (levels == null)
            levels = new List < LevelProgress >();
    }

    public LevelProgress getLevelProgress(string p_level)
    {
        init();

        LevelProgress l_level = levels.Find(i => i.name == p_level);

        if (l_level == null)
        {
            l_level = new LevelProgress();
            l_level.name = p_level;
            levels.Add(l_level);
        }

        return l_level;
    }
}

[System.Serializable]
public class LevelProgress
{
    public string name;
    public int stars;
}