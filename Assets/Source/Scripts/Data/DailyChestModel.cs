﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SerializeField]
public class DailyChestModel
{
    public DateTime lastOpened;

    public void open(DateTime p_openTime)
    {
        lastOpened = p_openTime;
    }

    public bool isReadyNow()
    {
        TimeSpan l_timeSinceLastOpened = lastOpened - DateTime.UtcNow;
        return l_timeSinceLastOpened.Duration().Hours >= TimeUtils.getHoursForFreeChest();
    }

    public string getTimeUntilNextChest()
    {
        TimeSpan l_totalTime = TimeSpan.FromHours(TimeUtils.getHoursForFreeChest());
        TimeSpan l_timePassed = lastOpened - DateTime.UtcNow;
        return TimeUtils.getTimeFormat(l_totalTime - l_timePassed.Duration());
    }
}
