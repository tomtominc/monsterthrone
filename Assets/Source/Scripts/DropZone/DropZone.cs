﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    private GameController m_gameController;
    private GameStateManager m_gameStateManager;
    private GameHud m_gameHud;

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameStateManager = m_gameController.gameStateManager;
        m_gameHud = m_gameController.gameHud;
    }

    public void OnPointerEnter(PointerEventData p_data)
    {
    }

    public void OnPointerExit(PointerEventData p_data)
    {
    }

    public void OnDrop(PointerEventData p_data)
    {
        GameCardView l_cardView = p_data.pointerDrag.GetComponent < GameCardView >();

        if (l_cardView == null)
            return;

        if (l_cardView.Equals(m_gameHud.currentlyDraggedCard))
        {
            CardStateModel l_cardState = l_cardView.cardState;

            if (m_gameHud.hasEnoughEssenceForAction(l_cardState))
            {
                m_gameStateManager.changeState(GameStateKey.SWAP_MONSTER);
            }
            else
            {
                Debug.LogFormat("Not enough essence for action!");
            }

        }
    }
}
