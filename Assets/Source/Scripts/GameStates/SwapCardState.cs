﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SwapCardState : GameState
{
    public override void startState()
    {
        GameCardView l_cardView = m_gameHud.currentlyDraggedCard;
        m_gameHud.playerHand.animateHandSwap(l_cardView);

        l_cardView.summon().OnComplete(() =>
            {
                m_gameWorld.summonMonsterCard(l_cardView);
                m_gameHud.modifyEssence(-l_cardView.cardState.card.cost);
                l_cardView.setAsActiveCard().OnComplete(() =>
                    m_gameStateManager.changeState(GameStateKey.WAIT_FOR_PLAYER_INTERACTION));
            });
    }

    public override void endState()
    {
        
    }
}
