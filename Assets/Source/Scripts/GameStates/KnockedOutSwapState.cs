﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockedOutSwapState : GameState
{
    public override void startState()
    {
        m_gameHud.knockedOutInstructions.gameObject.SetActive(true);
    }

    public override void endState()
    {
        m_gameHud.knockedOutInstructions.gameObject.SetActive(false);
    }
}
