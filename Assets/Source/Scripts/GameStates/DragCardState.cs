﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragCardState : GameState
{
    public override void startState()
    {
        if (!m_gameWorld.isMonsterKnockedOut)
            m_gameWorld.monsterView.playAnimation(MonsterView.State.GLOW);
    }

    public override void endState()
    {
        if (!m_gameWorld.isMonsterKnockedOut)
            m_gameWorld.monsterView.playAnimation(MonsterView.State.IDLE);
    }
}
