﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameStartState : GameState
{
    [SerializeField] private int m_essenceStartAmount = 1;

    public override void startState()
    {
        m_gameHud.show().OnComplete(startGame);
    }

    private void startGame()
    {
        if (m_playerData.content.deck.contents.Count == 3)
        {
            m_gameWorld.summonStarterCard();
            m_gameHud.modifyEssence(m_essenceStartAmount);
        }

        m_gameStateManager.changeState(GameStateKey.WAIT_FOR_PLAYER_INTERACTION);

        EventManager.publish(this, GameEvent.LEVEL_START, null);
    }
}
