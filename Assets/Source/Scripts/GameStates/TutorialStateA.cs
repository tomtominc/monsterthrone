﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TutorialStateA : GameState
{
    private const float CONTINUE_DELAY = 5.0F;
        
    [SerializeField]
    private Animator m_tutorial;
    [SerializeField]
    private RectTransform m_dialogueBox;
    [SerializeField]
    private RectTransform m_marble;
    [SerializeField]
    private RectTransform m_marbleShadow;
    [SerializeField]
    private Shake m_worldShake;
    [SerializeField]
    private Transform m_princess;

    [SerializeField]
    private List < GameObject > m_disabledObjects;

    private bool m_finishedPhase1;
    private bool m_finishedPhase2;
    private bool m_finishedPhase3;
    private bool m_finishedPhase4;
    private bool m_finishedPhase5;

    private bool m_update;
    private int m_lastTouchState;
    private float m_delay;

    public override void startState()
    {
        if (!m_finishedPhase1)
        {
            tutorialPhase1();
        }
        else if (!m_finishedPhase2)
        {
            tutorialPhase2();
        }
        else if (!m_finishedPhase3)
        {
            tutorialPhase3();
        }
        else if (!m_finishedPhase4)
        {
            tutorialPhase4();
        }
        else if (!m_finishedPhase5)
        {
            tutorialPhase5();
        }
    }

    public override void updateState()
    {
        if (m_update)
        {
            if (!m_finishedPhase3)
            {
                updatePhase3();
            }
            else if (!m_finishedPhase4)
            {
                updatePhase4();
            }
            else if (!m_finishedPhase5)
            {
                updatePhase5();
            }
        }
    }

    #region TUTORIAL PHASE 1

    private void tutorialPhase1()
    {
        m_dialogueBox.gameObject.SetActive(true);

        m_disabledObjects.ForEach(x => x.SetActive(false));
        m_dialogueBox.DOAnchorPos(new Vector2(0f, -60f), 0.5f)
            .SetDelay(1f).From()
            .SetEase(Ease.OutBack)
            .OnComplete(completeTutorialPhase1);
    }

    private void completeTutorialPhase1()
    {
        m_finishedPhase1 = true;
        m_marble.DoJumpSequence(0.2f, 1.52f, 20, m_marbleShadow);
        EventManager.publish(this, GameEvent.TUTORIAL_COMPLETE, null);
    }

    #endregion

    #region TUTORIAL PHASE 2

    private void tutorialPhase2()
    {
        m_princess.gameObject.SetActive(true);
        m_princess.position = new Vector3(0f, 7f);
        m_princess.DOMove(new Vector3(0f, -5f), 5f)
            .SetDelay(1f)
            .SetEase(Ease.Linear)
            .OnComplete(completeTutorialPhase2);

        m_worldShake.shakeConstant(new Vector2(0f, 1f));
    }

    private void completeTutorialPhase2()
    {
        m_finishedPhase2 = true;
        m_princess.gameObject.SetActive(false);
        m_worldShake.stopShake();
        EventManager.publish(this, GameEvent.TUTORIAL_COMPLETE, null);
    }

    #endregion

    #region TUTORIAL PHASE 3

    private void tutorialPhase3()
    {
        m_update = true;
        m_tutorial.Play("TOUCH_DIRECTIONS");
    }

    private void updatePhase3()
    {
        int l_touchState;

        m_player.updateTrajectoryTutorial(new Vector2(2.0f, 2.2f), out l_touchState);

        if (m_lastTouchState != l_touchState)
        {
            m_lastTouchState = l_touchState;

            switch (l_touchState)
            {
                case 0: 
                    m_tutorial.Play("TOUCH_DIRECTIONS");
                    break;
                case 1:
                    Vector2 l_currentTouch = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    Debug.Log(l_currentTouch);

                    if (l_currentTouch.inTriangle(new Vector2(0, -1), new Vector2(-4f, -1f), new Vector2(-4f, 5f)))
                    {
                        m_tutorial.Play("SWIPE_DIRECTIONS_LEFT");
                    }
                    else
                    {
                        m_tutorial.Play("SWIPE_DIRECTIONS");
                    }
                    break;
                case 2:
                    m_tutorial.Play("SHOOT_DIRECTIONS");
                    break;
                case 3:
                    completePhase3();
                    break;
            }
        }
    }

    private void completePhase3()
    {
        m_update = false;
        m_tutorial.Play("WAIT");
        m_finishedPhase3 = true;
        EventManager.publish(this, GameEvent.TUTORIAL_COMPLETE, null);
    }

    #endregion

    #region TUTORIAL PHASE 4

    public void tutorialPhase4()
    {
        m_update = true;
        m_delay = CONTINUE_DELAY;
        m_tutorial.Play("WAVES_DIRECTIONS");
    }

    public void updatePhase4()
    {
        m_delay -= Time.deltaTime;

        if (m_delay <= 0)
            completePhase4();
    }

    public void completePhase4()
    {
        m_update = false;
        m_tutorial.Play("WAIT");
        m_finishedPhase4 = true;
        EventManager.publish(this, GameEvent.TUTORIAL_COMPLETE, null);
    }

    #endregion

    #region TUTORIAL PHASE 5

    public void tutorialPhase5()
    {
        m_update = true;
        m_delay = CONTINUE_DELAY;
        m_tutorial.Play("HEALTH_DIRECTIONS");
    }

    public void updatePhase5()
    {
        m_delay -= Time.deltaTime;

        if (m_delay <= 0)
            completePhase5();
    }

    public void completePhase5()
    {
        m_update = false;
        m_tutorial.Play("WAIT");
        m_finishedPhase5 = true;
        EventManager.publish(this, GameEvent.TUTORIAL_COMPLETE, null);
    }

    #endregion
}


