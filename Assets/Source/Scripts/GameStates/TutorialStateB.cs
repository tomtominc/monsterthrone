﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class TutorialStateB : GameState
{
    public const float PHASE_3_DELAY = 5F;

    [SerializeField]
    private Animator m_tutorialAnimator;
    [SerializeField]
    private GameObject m_selectMonsterLabel;
    [SerializeField]
    private PointerCutout m_benchedMonstersCutout;
    [SerializeField]
    private PointerCutout m_activeMonsterCutout;
    [SerializeField]
    private PointerCutout m_activeFieldMonsterCutout;
    [SerializeField]
    private PointerCutout m_abilityCutout;
    [SerializeField]
    private Image m_background;
    [SerializeField]
    private RectTransform m_dialogueBox;
    [SerializeField]
    private Text m_dialogueText;
    [SerializeField]
    private List < GameObject > m_bottomHUDObjects;
    [SerializeField]
    private List < StarterCard > m_starterCards;
    [SerializeField]
    private List < string > m_starterCardIds;

    public override void initialize(GameController p_gameController)
    {
        base.initialize(p_gameController);

        m_phases = new List<Action>();
        m_phases.Add(startPhase0);
        m_phases.Add(startPhase1);
        m_phases.Add(startPhase2);
        m_phases.Add(startPhase3);
        m_phases.Add(startPhase4);
        m_phases.Add(startPhase5);
        m_phases.Add(startPhase6);
    }


    public override void startState()
    {
        if (m_tutorialPhase < m_phases.Count)
            m_phases[m_tutorialPhase].Invoke();
    }

    public override void updateState()
    {
        if (m_update)
        {
            m_delay -= Time.deltaTime;

            if (m_delay <= 0)
            {
                m_update = false;
                completeTutorialPhase();
            }
        }
    }

    #region TUTORIAL PHASE 0

    private void startPhase0()
    {
        m_dialogueBox.gameObject.SetActive(true);
        m_dialogueText.text = string.Empty;
        m_dialogue.setDialogue(m_dialogueText);

        m_bottomHUDObjects.ForEach(x => x.SetActive(false));
        m_dialogueBox.DOAnchorPos(new Vector2(0f, -60f), 0.5f)
            .SetDelay(1f).From()
            .SetEase(Ease.OutBack)
            .OnComplete(completeTutorialPhase);
    }

    #endregion

    #region TUTORIAL PHASE 1

    // Phase 1
    private void startPhase1()
    {
        for (int i = 0; i < m_starterCards.Count; i++)
        {
            StarterCard l_card = m_starterCards[i];
            l_card.initialize(m_gameController, m_starterCardIds[i]);
            l_card.addCardButtonAction(onPressCardButton);
        }

        m_tutorialAnimator.Play("SHOW_STARTER_CARDS");
    }

    private void onPressCardButton(StarterCard p_card)
    {
        AnimatorStateInfo l_info = m_tutorialAnimator.GetCurrentAnimatorStateInfo(0);
        bool l_descriptionIsUp = l_info.IsName("SHOW_DESCRIPTION_NO_MOVEMENT");
        bool l_removeDescription = m_currentCard != null && m_currentCard.Equals(p_card);
        m_currentCard = p_card;

        if (l_descriptionIsUp && l_removeDescription)
        {
            m_currentCard = null;
            m_tutorialAnimator.Play("HIDE_DESCRIPTION");
        }
        else if (l_descriptionIsUp)
        {
            m_currentCard.setupAbilityPopup();
        }
        else
        {
            m_currentCard.setupAbilityPopup();
            m_tutorialAnimator.Play("SHOW_DESCRIPTION");
        }
    }

    public void onPressChooseButton()
    {
        if (m_currentCard == null)
            return;
        
        for (int i = 0; i < m_starterCards.Count; i++)
        {
            StarterCard l_card = m_starterCards[i];
            l_card.removeCardButtonAction(onPressCardButton);
        }

        m_tutorialAnimator.Play("HIDE_DESCRIPTION");
        AnimatorStateInfo l_info = m_tutorialAnimator.GetCurrentAnimatorStateInfo(0);

        Sequence l_sequence = DOTween.Sequence();
        l_sequence.AppendInterval(l_info.length);
        l_sequence.AppendCallback(() =>
            {
                m_tutorialAnimator.enabled = false;

                m_currentCard.chooseCard();

                CardStateModel l_monsterCard = m_currentCard.getMonsterCard();
                l_monsterCard.unlocked = true;
                m_playerData.content.deck.contents.Add(l_monsterCard.id);
                m_playerData.content.deck.setCardAsFirst(l_monsterCard.id);

                for (int i = 0; i < m_starterCards.Count; i++)
                {
                    StarterCard l_card = m_starterCards[i];

                    if (!l_card.Equals(m_currentCard))
                    {
                        l_card.removeCard();
                    }
                }
            });
        l_sequence.Append(m_background.DOFade(0f, 1f));
        l_sequence.AppendCallback(completeTutorialPhase);
    }

    #endregion

    #region TUTORIAL PHASE 2

    private void startPhase2()
    {
        m_selectMonsterLabel.gameObject.SetActive(false);
        m_bottomHUDObjects.ForEach(x => x.SetActive(true));
        m_dialogueBox.DOAnchorPos(new Vector2(0f, 134f), 0.5f)
            .SetEase(Ease.OutBack)
            .OnComplete(completeTutorialPhase);
    }

    #endregion

    #region TUTORIAL PHASE 3

    private void startPhase3()
    {
        m_benchedMonstersCutout.open("RIGHT_DOWN");
        Sequence l_sequence = DOTween.Sequence();
        l_sequence.AppendInterval(PHASE_3_DELAY);
        l_sequence.AppendCallback(m_benchedMonstersCutout.close);
        l_sequence.AppendCallback(completeTutorialPhase);
    }

    #endregion

    #region TUTORIAL PHASE 4

    private void startPhase4()
    {
        m_activeMonsterCutout.open("RIGHT_DOWN");
        Sequence l_sequence = DOTween.Sequence();
        l_sequence.AppendInterval(PHASE_3_DELAY);
        l_sequence.AppendCallback(m_activeMonsterCutout.close);
        l_sequence.AppendCallback(completeTutorialPhase);
    }

    #endregion

    #region TUTORIAL PHASE 5

    private void startPhase5()
    {
        m_activeFieldMonsterCutout.open("RIGHT_UP");
        Sequence l_sequence = DOTween.Sequence();
        l_sequence.AppendInterval(PHASE_3_DELAY);
        l_sequence.AppendCallback(m_activeFieldMonsterCutout.close);
        l_sequence.AppendCallback(completeTutorialPhase);
    }

    #endregion

    #region TUTORIAL PHASE 6

    private void startPhase6()
    {
        m_abilityCutout.open("RIGHT_DOWN");
        Sequence l_sequence = DOTween.Sequence();
        l_sequence.AppendInterval(PHASE_3_DELAY);
        l_sequence.AppendCallback(m_abilityCutout.close);
        l_sequence.AppendCallback(completeTutorialPhase);
    }

    #endregion

    // Tutorial Complete Helper
    private void completeTutorialPhase()
    {
        m_tutorialPhase++;
        EventManager.publish(this, GameEvent.TUTORIAL_COMPLETE, null);
    }

    private int m_tutorialPhase;
    private StarterCard m_currentCard;
    private List < Action > m_phases;
    private Animator m_pointerAnimator;
    private float m_delay;
    private bool m_update;
}
