﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AbilityActiveState : GameState
{
    [SerializeField]
    private AbilityShow m_abilityShow;

    public override void startState()
    {
        CardStateModel l_ability = m_gameWorld.abilityCard;

        if (m_gameHud.hasEnoughEssenceForAction(l_ability))
        {
            m_gameHud.modifyEssence(-l_ability.card.cost);
            m_abilityShow.showAbility(m_gameHud.playerHand.activeCard.cardState).OnComplete 
            (() => m_abilityManager.useAbility(l_ability, 
                    () => m_gameStateManager.changeState(GameStateKey.WAIT_FOR_PLAYER_INTERACTION)));
        }
        else
        {
            Debug.Log("not enough essence to use this!");
        }
      
    }
}
