﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridUpdateState : GameState
{

    public override void startState()
    {
        nextTurn();
    }

    private void nextTurn()
    {
        if (isPlayerWinner())
        {
            m_gameStateManager.changeState(GameStateKey.WIN_GAME);
            return;
        }

        m_gameData.turns++;
        StartCoroutine(gridUpdate());
    }

    private bool isPlayerWinner()
    {
        return m_gridManager.GridViews.Count <= 0 && m_gameData.turns >= m_level.waves;
    }

    private IEnumerator gridUpdate()
    {
        EventManager.publish(this, GameEvent.ENEMY_TURN_START, null);

        yield return StartCoroutine(m_gridManager.startTurn(m_gameData.turns));
        yield return new WaitForSeconds(0.5f);

        m_player.startTurn();

        m_gameStateManager.changeState(GameStateKey.WAIT_FOR_PLAYER_INTERACTION);

        EventManager.publish(this, GameEvent.ENEMY_TURN_END, null);
    }
}
