﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverState : GameState
{
    public override void startState()
    {
        m_gameHud.showGameOverScreen();       
    }
}
