﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameStateKey
{
    START_GAME,
    WAIT_FOR_PLAYER_INTERACTION,
    ABILITY_ACTIVE,
    DRAG_CARD,
    SWAP_MONSTER,
    GRID_UPDATE,
    GAME_OVER,
    KNOCKED_OUT_SWAP,
    TUTORIAL_A,
    WIN_GAME,
    TUTORIAL_B
}

public class GameState : MonoBehaviour
{
    public GameStateKey gameStateKey;

    protected GameController m_gameController;
    protected GameStateManager m_gameStateManager;
    protected GameHud m_gameHud;
    protected GameWorld m_gameWorld;
    protected MonsterThronePlayerData m_playerData;
    protected CastleManager m_castleManager;
    protected AbilityManager m_abilityManager;
    protected GridManager m_gridManager;
    protected Player m_player;
    protected GameData m_gameData;
    protected GameDialogue m_dialogue;
    protected Level m_level;

    public virtual void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameStateManager = m_gameController.gameStateManager;
        m_playerData = m_gameController.playerData;
        m_gameHud = m_gameController.gameHud;
        m_gameWorld = m_gameController.gameWorld;
        m_abilityManager = m_gameController.abilityManager;
        m_gridManager = m_gameController.gridManager;
        m_player = m_gameController.player;
        m_gameData = m_gameController.gameData;
        m_castleManager = m_gameController.castleManager;
        m_level = m_castleManager.level;
        m_dialogue = m_gameController.dialogue;
    }

    public virtual void startState()
    {
        
    }

    public virtual void updateState()
    {
        
    }

    public virtual void endState()
    {
        
    }
}
