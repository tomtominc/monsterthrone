﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForPlayerInteractionState : GameState
{
    public override void startState()
    {
    }

    public override void updateState()
    {
        m_player.updateTrajectory();
    }
}
