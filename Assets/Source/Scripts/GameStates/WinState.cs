﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinState : GameState
{
    [SerializeField]
    private WinPopup m_winPopup;

    public override void startState()
    {
        EventManager.publish(this, GameEvent.LEVEL_COMPLETE, null);

        m_winPopup.initialize(m_gameController);
    }
}
