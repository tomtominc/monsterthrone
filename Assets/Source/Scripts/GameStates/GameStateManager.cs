﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    [SerializeField]
    private GameStateKey m_startState;

    #region Members

    protected GameController m_gameController;
    [SerializeField]
    protected GameStateKey m_currentGameState;
    protected Dictionary < GameStateKey , GameState > m_gameStates;

    #endregion

    #region Properties

    public GameStateKey currentGameState
    {
        get { return m_currentGameState; }
    }

    #endregion

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameStates = new Dictionary < GameStateKey , GameState >();

        var l_gameStates = GetComponentsInChildren < GameState >();

        for (int i = 0; i < l_gameStates.Length; i++)
        {
            GameState l_state = l_gameStates[i];
            l_state.initialize(m_gameController);
            m_gameStates.Add(l_gameStates[i].gameStateKey, l_gameStates[i]);
        }

        changeState(m_startState);
    }

    private void Update()
    {
        if (m_gameStates == null)
            return;
        
        if (m_gameStates.ContainsKey(m_currentGameState))
            m_gameStates[m_currentGameState].updateState();
    }

    #region State Helpers

    public bool isState(GameStateKey p_key)
    {
        return m_currentGameState == p_key;
    }

    public void changeState(GameStateKey gameStateKey)
    {
        if (m_gameStates.ContainsKey(m_currentGameState))
            m_gameStates[m_currentGameState].endState();

        m_currentGameState = gameStateKey;

        if (m_gameStates.ContainsKey(m_currentGameState))
            m_gameStates[m_currentGameState].startState();
        
    }

    public void changeStartingState(GameStateKey p_startingState)
    {
        m_startState = p_startingState;
    }

    #endregion
}
