﻿using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;
using UnityEngine;
using PlayFab.SharedModels;

public class MonsterThronePlayerService : PlayerService
{
    protected override void handleRequestSuccess(PlayFabResultCommon p_result)
    {
        LoginResult l_result = p_result as LoginResult;

        m_playerData = new MonsterThronePlayerData();
        m_playerData.setGameServiceProvider(m_gameServiceProvider);
        m_playerData.setPlayerData(l_result);

        base.handleRequestSuccess(p_result);
    }
}
