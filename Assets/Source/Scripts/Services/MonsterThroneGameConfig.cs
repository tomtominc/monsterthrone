﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterThroneGameConfig : GameConfig
{
    protected MonsterThroneGameModel m_gameModel;

    public override string getValue()
    {
        return m_gameModel.ToString();
    }
}
