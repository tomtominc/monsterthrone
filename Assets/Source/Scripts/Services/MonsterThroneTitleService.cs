﻿using System.Collections;
using System.Collections.Generic;
using PlayFab.ServerModels;
using UnityEngine;
using PlayFab.SharedModels;
using PlayFab;
using Sirenix.OdinInspector;
using Newtonsoft.Json;

public class MonsterThroneTitleService : TitleService
{
    [SerializeField]
    public MonsterThroneGameModel gameModel;

    public override void Initialize(GameServiceProvider p_gameServiceProvider)
    {
        base.Initialize(p_gameServiceProvider);

        m_configDataKeys = new List<string>()
        {
            MonsterThroneGameModel.MONSTER_DATABASE_KEY,
            MonsterThroneGameModel.ABILITY_DATABASE_KEY,
            MonsterThroneGameModel.BLOCK_DATABASE_KEY,
            MonsterThroneGameModel.DIFFICULTY_SETTINGS_KEY,
            MonsterThroneGameModel.CASTLE_SETTINGS_KEY
        };
    }

    protected override void handleRequestSuccess(PlayFabResultCommon p_result)
    {
        GetTitleDataResult l_result = p_result as GetTitleDataResult;

        gameModel = new MonsterThroneGameModel();
        gameModel.createMonsterDatabase(l_result.Data[MonsterThroneGameModel.MONSTER_DATABASE_KEY]);
        gameModel.createAbilityDatabase(l_result.Data[MonsterThroneGameModel.ABILITY_DATABASE_KEY]);
        gameModel.createBlockDatabase(l_result.Data[MonsterThroneGameModel.BLOCK_DATABASE_KEY]);
        gameModel.createDifficultySettings(l_result.Data[MonsterThroneGameModel.DIFFICULTY_SETTINGS_KEY]);
        gameModel.createCastleSettings(l_result.Data[MonsterThroneGameModel.CASTLE_SETTINGS_KEY]);

        base.handleRequestSuccess(p_result);
    }

    public override void updateData()
    {
    }

    [Button]
    public void saveMonsterDatabase()
    {
        SetTitleDataRequest l_monsterDatabaseRequest = new SetTitleDataRequest();
        l_monsterDatabaseRequest.Key = MonsterThroneGameModel.MONSTER_DATABASE_KEY;
        l_monsterDatabaseRequest.Value = JsonConvert.SerializeObject(gameModel.monsterDatabase);
        PlayFabServerAPI.SetTitleData(l_monsterDatabaseRequest, handleUpdateSuccess, handleUpdateFailure);
    }

    [Button]
    public void saveAbilityDatabase()
    {
        SetTitleDataRequest l_abilityDatabaseRequest = new SetTitleDataRequest();
        l_abilityDatabaseRequest.Key = MonsterThroneGameModel.ABILITY_DATABASE_KEY;
        l_abilityDatabaseRequest.Value = JsonConvert.SerializeObject(gameModel.abilityDatabase);
        PlayFabServerAPI.SetTitleData(l_abilityDatabaseRequest, handleUpdateSuccess, handleUpdateFailure);
    }

    [Button]
    public void saveBlockDatabase()
    {
        SetTitleDataRequest l_blockDatabaseRequest = new SetTitleDataRequest();
        l_blockDatabaseRequest.Key = MonsterThroneGameModel.BLOCK_DATABASE_KEY;
        l_blockDatabaseRequest.Value = JsonConvert.SerializeObject(gameModel.blockDatabase);
        PlayFabServerAPI.SetTitleData(l_blockDatabaseRequest, handleUpdateSuccess, handleUpdateFailure);
    }

    [Button]
    public void saveSettings()
    {
        SetTitleDataRequest l_difficultySettingsRequest = new SetTitleDataRequest();
        l_difficultySettingsRequest.Key = MonsterThroneGameModel.DIFFICULTY_SETTINGS_KEY;
        l_difficultySettingsRequest.Value = JsonConvert.SerializeObject(gameModel.difficultySettings);
        PlayFabServerAPI.SetTitleData(l_difficultySettingsRequest, handleUpdateSuccess, handleUpdateFailure);

        SetTitleDataRequest l_castleSettingsRequest = new SetTitleDataRequest();
        l_castleSettingsRequest.Key = MonsterThroneGameModel.CASTLE_SETTINGS_KEY;
        l_castleSettingsRequest.Value = JsonConvert.SerializeObject(gameModel.castleSettings);
        PlayFabServerAPI.SetTitleData(l_castleSettingsRequest, handleUpdateSuccess, handleUpdateFailure);
    }
}
