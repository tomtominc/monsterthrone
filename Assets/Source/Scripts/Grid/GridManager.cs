﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using System.Linq;

public class GridManager : MonoBehaviour
{
    #region Members

    [SerializeField]
    protected WaveHud m_waveHud;
    [SerializeField] 
    protected int m_rows = 5;
    [SerializeField] 
    protected int m_columns = 5;
    [SerializeField] 
    protected Transform m_blockPrefab;
    [SerializeField] 
    protected Transform m_itemPrefab;


    private int m_currentMinHitPoints;
    private int m_currentMaxHitPoints;
    private int m_stepX = 1;

    private Vector3 m_bottomLimit = new Vector3(0f, -1f, 0f);
    private Vector2 m_bottomGrid;
    private List < GridView > m_gridViews;
    private bool[,] m_gridMap;
    private Rect m_screenRect;
    
    private GameController m_gameController;
    private GameWorld m_gameWorld;
    private GameHud m_gameHud;
    private CastleManager m_castleManager;
    private Level m_level;
    private DifficultySettings m_difficultySettings;
    private BlockSettings m_blockSettings;
    private ItemSettings m_itemSettings;
    private int m_currentWave;
    private bool m_hasLevelOverride;

    #endregion

    #region Properties

    public int rows
    {
        get { return m_rows; }
    }

    public int columns
    {
        get { return m_columns; }
    }

    public Vector3 BottomLimit
    {
        get { return m_bottomLimit; }
    }

    public Vector3 bottom
    {
        get { return m_bottomGrid; }
    }

    public List < GridView > GridViews
    {
        get { return m_gridViews; }
    }

    public Level level
    {
        get { return m_level; }
    }

    #endregion

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameWorld = p_gameController.gameWorld;
        m_gameHud = p_gameController.gameHud;
        m_screenRect = CameraTools.GetScreenRect();    
        m_bottomGrid = GetPositionFromGrid(m_rows, m_columns);
        m_gridMap = new bool[m_rows, m_columns + 1];
        m_castleManager = m_gameController.castleManager;
        m_difficultySettings = m_castleManager.difficultySettings;
        m_level = m_castleManager.level;
        m_blockSettings = m_level.blockSettings;
        m_waveHud.initialize(m_gameController);
        m_gameController.inventory.setItem("Ball", 1);

        m_currentMinHitPoints = m_difficultySettings.startingMinBlockHitPoints;
        m_currentMaxHitPoints = m_difficultySettings.startingMaxBlockHitPoints;

        transform.position = new Vector3(-3f, m_screenRect.yMax);
        m_gridViews = new List < GridView >();
    }

    public void clearGrid()
    {
        transform.DestroyChildren();
        m_gridViews.Clear();
    }

    public IEnumerator startTurn(int p_turn)
    {
        if (m_currentWave < m_level.waves)
        {
            m_currentWave++;
            m_waveHud.setCurrentWaveCount(m_currentWave);

            if (!m_castleManager.hasLevelOverride)
                createLine();
        }

        if (m_castleManager.hasLevelOverride)
        {
            if (p_turn == 1)
            {
                
                for (int i = 0; i < 5; i++)
                {
                    createLineFromLevelOverride();
                    yield return StartCoroutine(updateAttackersSingle());
                    yield return StartCoroutine(updateMovers());
                    
                }
            }
            else
            {
                createLineFromLevelOverride();
                yield return StartCoroutine(updateAttackersSingle());
                yield return StartCoroutine(updateMovers());
            }
        }
        else
        {
            yield return StartCoroutine(updateAttackersSingle());
            yield return StartCoroutine(updateMovers());
        }

        endTurn();
        upgradeDifficulty(p_turn);

        yield break;
    }

    public void upgradeDifficulty(int p_turn)
    {
        if (p_turn % m_difficultySettings.turnsToUpgradeMinBlockHitPoints == 0)
        {
            m_currentMinHitPoints += m_difficultySettings.upgradeMinHitPoints;
        }

        if (p_turn % m_difficultySettings.turnsToUpgradeMaxBlockHitPoints == 0)
        {
            m_currentMaxHitPoints += m_difficultySettings.upgradeMaxHitPoints;
        }
    }

    #region Move Methods

    private IEnumerator updateAttackersSingle()
    {
        List < GridView > l_gridViews = m_gridViews.OrderBy(x => x.position.x).ToList();

        foreach (GridView l_view in l_gridViews)
        {
            bool l_canAttack = l_view.canAttack;

            if (!l_canAttack)
                continue;

            DamageInfo l_damageInfo = l_view.damageInfo;

            if (l_damageInfo == null)
                continue;

            int inRange = columns - l_view.column - l_damageInfo.range;

            if (l_view.column == m_columns)
            {
                yield return StartCoroutine(l_view.doBottomGridAction());
            }

            if (inRange > 0)
                continue;

            l_view.attack();

            yield return new WaitForSeconds(l_damageInfo.delay + m_gameWorld.damageDuration);

            if (m_gameWorld.isMonsterKnockedOut)
            {
                if (m_gameWorld.hasAvailableCards)
                {
                    yield return m_gameWorld.waitForPlayerSwapMonster();
                }
                else
                {
                    m_gameWorld.gameOver();
                    yield break;
                }
            }

        }
    }

    private IEnumerator updateMovers()
    {
        for (int l_column = m_columns - 1; l_column >= 0; l_column--)
        {
            List <GridView> l_columnElements = m_gridViews.FindAll(x => x.column == l_column);
            l_columnElements = l_columnElements.OrderBy(x => x.position.x).ToList();

            foreach (GridView l_gridView in l_columnElements)
            {
                bool l_canWalk = l_gridView.canWalk;

                if (!l_canWalk)
                    continue;
                
                int l_row = l_gridView.row;

                if (m_gridMap[l_row, l_column + 1])
                    continue;
                
                l_gridView.setGridPositionByOffset(0, 1);

                m_gridMap[l_row, l_column] = false;
                m_gridMap[l_row, l_column + 1] = true;

                Vector3 l_endPosition = l_gridView.position - Vector3.up * m_stepX;
                l_gridView.move(l_endPosition, 0.6f);
            }

            if (l_columnElements.Count > 0)
                yield return new WaitForSeconds(0.25f);
        }
    }

    private void endTurn()
    {
       
        m_gameHud.endTurn(m_currentWave);
       
        List < GridView > l_removeViews = new List < GridView  >();
       
        foreach (GridView l_view in m_gridViews)
        {
            l_view.endTurn();

            if (l_view.hitPoints <= 0)
                l_removeViews.Add(l_view);
        }

        foreach (GridView l_removal in l_removeViews)
        {
            l_removal.destroy();
        }
    }

    #endregion

    #region Create Methods

    private void createLineFromLevelOverride()
    {
        LevelOverrideData l_override = m_castleManager.levelOverride;

        for (int x = 0; x < m_rows; x++)
        {
            if (m_gridMap[x, 0])
                continue;

            createBlock(x, 0, l_override.getNext());
        }
    }

    private void createLine()
    {
        for (int x = 0; x < m_rows; x++)
        {
            if (m_gridMap[x, 0])
                continue;

            createBlock(x, 0);
        }
    }

    private void createBlock(int p_row, int p_column, BlockData p_overrideBlock = null)
    {
        BlockData l_randomBlock = null;

        if (null != p_overrideBlock)
            l_randomBlock = p_overrideBlock;
        else
            l_randomBlock = new Selector().Single(m_blockSettings.allowedBlocks);
        

        if (l_randomBlock.blockId.Equals("Empty"))
            return;
        
        CardStateModel l_blockCard = new CardStateModel();
        l_blockCard.initialize(m_gameController.gameServiceProvider);
        l_blockCard.setCardId(l_randomBlock.blockId);

        string l_prefabDirectory = string.Format("Prefabs/Blocks/{0}", l_randomBlock.blockId);
        Transform l_prefab = Resources.Load < Transform >(l_prefabDirectory);

        if (l_prefab == null)
        {
            Debug.LogErrorFormat("Couldn't find prefab : {0}", l_prefabDirectory);
            return;
        }

        Transform l_blockObject = Instantiate(l_prefab);
        l_blockObject.SetParent(transform);
        l_blockObject.localPosition = GetPositionFromGrid(p_row, p_column);

        BlockView l_blockView = l_blockObject.GetComponent < BlockView >();
        l_blockView.initialize(m_gameController);
        l_blockView.updateView(l_blockCard);
        l_blockView.updateView();
        l_blockView.setGridPosition(p_row, p_column);

        if (l_randomBlock.healthOverride <= 0)
            l_blockView.setHitPoints(Random.Range(m_currentMinHitPoints, m_currentMaxHitPoints + 1));
        else
            l_blockView.setHitPoints(l_randomBlock.healthOverride);
        
        l_blockView.destroyed += Block_OnDestroyedByBall;

        m_gridViews.Add(l_blockView);
        m_gridMap[p_row, p_column] = true;
    }

    #endregion

    #region Helpers

    public Vector3 GetPositionFromGrid(int p_row, int p_column)
    {
        float xPos = (m_stepX + p_row * m_stepX);
        float yPos = (-p_column * m_stepX);
        Vector3 position = new Vector3(xPos, yPos);
        return position;
    }

    #endregion

    #region Event Callbacks

    void Block_OnDestroyedByBall(GridView p_gridView)
    {
        m_gridViews.Remove(p_gridView);
        m_gridMap[p_gridView.row, p_gridView.column] = false;

        ItemView l_itemView = p_gridView as ItemView;

        if (l_itemView != null)
        {
            ItemDrop l_itemDrop = l_itemView.getItem();
            m_gameController.inventory.addItem(l_itemDrop.itemId, l_itemDrop.count);
        }
    }

    #endregion
}
