﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class StatusFactory : SerializedMonoBehaviour
{
    [SerializeField]
    private Dictionary < string, Transform > m_statusMap;

    public Transform getStatus(CardStatus p_status)
    {
        string l_status = p_status.ToString();

        if (m_statusMap.ContainsKey(l_status))
        {
            return Instantiate < Transform >(m_statusMap[l_status]);
        }

        return null;
    }
}
