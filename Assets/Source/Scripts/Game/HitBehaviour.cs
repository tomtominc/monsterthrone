﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface HitBehaviour
{
    void hit(DamageInfo p_info);
}
