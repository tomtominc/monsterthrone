﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleView : MonoBehaviour, HitBehaviour
{
    private GameWorld m_gameWorld;

    public void initialize(GameController p_gameController)
    {
        m_gameWorld = p_gameController.gameWorld;
    }

    public void hit(DamageInfo p_damageInfo)
    {
        m_gameWorld.attackMonster(p_damageInfo);
    }
}
