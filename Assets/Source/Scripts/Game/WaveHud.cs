﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveHud : MonoBehaviour
{
    [SerializeField]
    protected RectTransform m_wavePrefab;

    protected GameController m_gameController;
    protected GridManager m_gridManager;
    protected Level m_level;

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gridManager = m_gameController.gridManager;
        m_level = m_gridManager.level;

        createWaves();
    }

    public void createWaves()
    {
        transform.DestroyChildren();

        for (int i = 0; i < m_level.waves; i++)
        {
            RectTransform l_wave = Instantiate(m_wavePrefab);
            l_wave.SetParent(transform, false);
            Toggle l_toggle = l_wave.GetComponent < Toggle >();
            l_toggle.isOn = true;
        }
    }

    public void setCurrentWaveCount(int p_count)
    {
        for (int i = 0; i < p_count; i++)
        {
            RectTransform l_wave = transform.GetChild(i) as RectTransform;
            Toggle l_toggle = l_wave.GetComponent < Toggle >();
            l_toggle.isOn = false;
        }
    }
}
