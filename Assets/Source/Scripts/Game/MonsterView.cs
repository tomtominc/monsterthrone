﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonsterView : MonoBehaviour, HitBehaviour
{
    public enum State
    {
        IDLE,
        ATTACK,
        DAMAGED,
        SUMMON,
        GLOW,
        DEATH
    }

    [SerializeField] 
    protected SpriteAnimation m_attackEffectAnimator;

    protected GameController m_gameController;
    protected CardStateModel m_cardState;
    protected SpriteRenderer m_renderer;
    protected SpriteAnimation m_animator;
    protected State m_currentState;
    protected GameWorld m_gameWorld;

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameWorld = m_gameController.gameWorld;

        m_renderer = GetComponent < SpriteRenderer >();
        m_animator = GetComponent < SpriteAnimation >();
    }

    public void resetView()
    {
        m_animator.Stop();
        m_animator.assets.Clear();
        m_renderer.sprite = null;
    }

    public void updateView(CardStateModel p_cardState)
    {
        m_cardState = p_cardState;
        SpriteAnimationAsset m_asset = p_cardState.card.getAnimationAsset();
        if (m_asset == null)
        {
            Debug.LogFormat("Asset at path {0} not found.", p_cardState.card.animationAsset);
        }
        m_animator.assets.Add(p_cardState.card.getAnimationAsset());

        changeState(State.IDLE, null);
    }


    public void changeState(State p_state, Action p_onComplete)
    {
        m_currentState = p_state;

        m_animator.PlayWithCallBack(m_currentState, (x, y) =>
            {
                if (m_currentState != State.IDLE && m_currentState != State.DEATH)
                {
                    m_currentState = State.IDLE;
                    m_animator.Play(m_currentState);
                }

                if (p_onComplete != null)
                    p_onComplete();
            });
    }

    public void playAnimation(State p_state)
    {
        m_animator.Play(p_state);
    }

    #region Hit Behaviour

    public void hit(DamageInfo p_damageInfo)
    {
        m_gameWorld.attackMonster(p_damageInfo);
    }

    #endregion

    #region Animation Events

    public void playAttackEffect()
    {
        m_attackEffectAnimator.gameObject.SetActive(true);

        m_attackEffectAnimator.PlayWithCallBack(m_cardState.card.attackEffect, 
            (x, y) => m_attackEffectAnimator.gameObject.SetActive(false));
    }

    public void damage(DamageInfo m_damageInfo)
    {
        if (m_cardState.hitPoints > 0)
            changeState(State.DAMAGED, null);
        else
            changeState(State.DEATH, null);
    }

    #endregion
}
