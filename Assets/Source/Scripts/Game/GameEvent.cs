﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameEvent
{
    LEVEL_START,
    PLAYER_TURN_START,
    PLAYER_TURN_END,
    START_FIRING,
    END_FIRING,
    BLOCK_IN_FRONT_ROW,
    LEVEL_COMPLETE,
    LEVEL_FAILURE,
    ENEMY_TURN_START,
    ENEMY_TURN_END,
    TUTORIAL_COMPLETE,

}
