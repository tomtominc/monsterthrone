﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityHud : MonoBehaviour
{
    [SerializeField] protected Image m_abilityIcon;
    [SerializeField] protected Button m_abilityButton;
    [SerializeField] protected GameObject m_essence;
    [SerializeField] protected UISpriteText m_essenceCostLabel;

    protected GameController m_gameController;
    protected GameHud m_gameHud;
    protected GameStateManager m_gameStateManager;
    protected CardStateModel m_ability;
    protected AbilityStateView m_abilityStateView;
    protected int m_essenceAmount;
    protected bool m_usable;

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameHud = m_gameController.gameHud;
        m_gameStateManager = m_gameController.gameStateManager;
        m_abilityStateView = GetComponent < AbilityStateView >();
    }

    public void setEssence(int p_amount)
    {
        if (null == m_ability)
            return;
        
        m_essenceAmount = p_amount;
        m_usable = m_ability.card.cost <= m_essenceAmount;

        updateView();
    }

    public void updateView(CardStateModel p_cardState)
    {
        m_ability = p_cardState;

        updateView();
    }

    public void updateView()
    {
        m_abilityStateView.initialize(m_gameController.gameServiceProvider, m_ability);
        m_abilityStateView.updateView();

        // check to see if it's an implemented ability 
        if (string.IsNullOrEmpty(m_ability.card.prefab))
        {
            // disable things..
        }

        m_abilityIcon.enabled = m_usable;
        m_abilityButton.interactable = m_usable;
        m_essence.SetActive(m_usable);

    }

    public void useAbility()
    {
        //  if (m_gameStateManager.currentGameState == GameStateKey.WAIT_FOR_PLAYER_INTERACTION)
        m_gameStateManager.changeState(GameStateKey.ABILITY_ACTIVE);
    }
}
