﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameHud : MonoBehaviour
{
    #region Members

    [SerializeField] protected RectTransform m_dragContainer;
    [SerializeField] protected AbilityHud m_abilityHud;
    [SerializeField] protected RectTransform m_playerHandContainer;
    [SerializeField] protected RectTransform m_cardViewPrefab;
    [SerializeField] protected RectTransform m_bottomHudRect;
    [SerializeField] protected UIEnergyBar m_essenceBar;
    [SerializeField] protected DropZone m_monsterDropZone;
    [SerializeField] protected RectTransform m_knockedOutInstructions;
    [SerializeField] protected RectTransform m_defeatedPopup;


    protected Sequence m_moveInSequence;
    protected PlayerHand m_playerHand;

    protected GameCardView m_activateCardView;
    protected GameController m_gameController;
    protected MonsterThronePlayerData m_playerData;
    protected CardStateDatabase m_cardStateDatabase;
    protected GameCardView m_currentlyDraggedCard;

    #endregion

    #region Properties

    public AbilityHud abilityHud
    {
        get { return m_abilityHud; }
    }

    public RectTransform bottomHudRect
    {
        get { return m_bottomHudRect; }
    }

    public RectTransform dragContainer
    {
        get { return m_dragContainer; }
    }

    public PlayerHand playerHand
    {
        get { return  m_playerHand; }
    }

    public GameCardView activeCardView
    {
        get { return m_activateCardView; }
    }

    public GameCardView currentlyDraggedCard
    {
        get { return m_currentlyDraggedCard; }
        set { m_currentlyDraggedCard = value; }
    }

    public RectTransform knockedOutInstructions
    {
        get { return m_knockedOutInstructions; }
    }

    #endregion

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_playerData = m_gameController.playerData;
        m_playerHand = m_playerHandContainer.GetComponent < PlayerHand >();
        m_cardStateDatabase = m_playerData.content.cardStateDatabase;

        m_monsterDropZone.initialize(m_gameController);
        m_playerHand.initialize(m_gameController);
        m_abilityHud.initialize(m_gameController);

    }

    #region Animations

    public void showGameOverScreen()
    {
        m_defeatedPopup.gameObject.SetActive(true);
    }

    public Sequence show()
    {
        m_playerHandContainer.DestroyChildren();
        m_moveInSequence = DOTween.Sequence();

        Level l_level = m_gameController.castleManager.level;

        if (m_playerData.content.deck.contents.Count < 3)
            return m_moveInSequence;

        for (int i = 0; i < m_playerData.content.deck.contents.Count; i++)
        {
            string l_cardId = m_playerData.content.deck.contents[i];
            CardStateModel l_cardState = m_cardStateDatabase.getCard(l_cardId);

            RectTransform l_cardView = Instantiate < RectTransform >(m_cardViewPrefab);
            l_cardView.SetParent(m_playerHandContainer, false);

            GameCardView l_gameCard = l_cardView.GetComponent < GameCardView >();
            l_gameCard.initialize(m_gameController, l_cardState);
            l_gameCard.updateView();

            if (i == 0)
            {
                l_gameCard.setCardActive();
                m_activateCardView = l_gameCard;
            }
            else
            {
                l_gameCard.setCardInactive();
            }

            Sequence l_cardSequence = l_gameCard.moveIn();
            l_cardSequence.PrependInterval(0.25f * i);
            m_moveInSequence.Join(l_cardSequence);
        }
      
        m_playerHand.configureHand();

        return m_moveInSequence;
    }

    #endregion

    #region Game Helpers

    public bool hasEnoughEssenceForAction(CardStateModel p_cardState)
    {
        return p_cardState.card.cost <= m_essenceBar.value;
    }

    public void modifyEssence(int m_amount)
    {
        m_essenceBar.FillValue(m_amount);
        int l_current = m_essenceBar.value;

        m_abilityHud.setEssence(l_current);
        m_playerHand.setEssence(l_current);
    }

    public void endTurn(int p_currentWave)
    {
        
    }

    #endregion
}
