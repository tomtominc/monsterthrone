﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TextFx;

public class GameWorld : MonoBehaviour
{
    #region Members

    [SerializeField] 
    protected MonsterView m_monsterView;
    [SerializeField]
    protected CastleView m_castleView;
    [SerializeField]
    protected StatusFactory m_statusFactory;
    [SerializeField] 
    protected SpriteAnimation m_monsterSummonEffect;
    [SerializeField] 
    protected SpriteAnimation m_monsterUnsummonTop;
    [SerializeField] 
    protected SpriteAnimation m_monsterUnsummonBottom;
    [SerializeField] 
    protected SpriteAnimation m_swapSpark;
    [SerializeField]
    protected SpriteRenderer m_castle;
    [SerializeField]
    protected SpriteRenderer m_castleOverlay;
    [SerializeField] 
    protected SpriteRenderer m_shadow;
    [SerializeField]
    protected TextFxUGUI m_damageLabel;

    private GameController m_gameController;
    private GameStateManager m_gameStateManager;
    private GameHud m_gameHud;
    private PlayerHand m_playerHand;
    private GameCardView m_gameCard;
    private CardStateModel m_cardState;
    private CardStateModel m_summonedCard;
    private CardStateModel m_abilityCard;
    private float m_damageDuration = 1f;

    #endregion

    #region Properties

    public MonsterView monsterView
    {
        get { return m_monsterView; }
    }

    public Vector3 monsterWorldPosition
    {
        get { return monsterView.transform.position; }
    }

    public SpriteAnimation swapSpark
    {
        get { return m_swapSpark; }
    }

    public CardStateModel abilityCard
    {
        get { return m_abilityCard; }
    }

    public float damageDuration
    {
        get { return m_damageDuration; }
    }

    public bool isMonsterKnockedOut
    {
        get { return m_summonedCard.hitPoints <= 0; }
    }

    public bool hasAvailableCards
    {
        get { return m_playerHand.hasAvailableCards(); }
    }

    public StatusFactory statusFactory
    {
        get { return m_statusFactory; }
    }

    #endregion

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameStateManager = p_gameController.gameStateManager;
        m_gameHud = m_gameController.gameHud;
        m_playerHand = m_gameHud.playerHand;
        m_monsterView.initialize(m_gameController);
        m_castleView.initialize(m_gameController);
    }

    public void summonMonster(CardStateModel p_cardState)
    {
        if (m_cardState == null)
        {
            m_cardState = p_cardState;
            summonMonster();
        }
        else
        {
            m_cardState = p_cardState;
            unsummonMonster();
        } 
    }

    public void attackMonster(DamageInfo m_damage)
    {
        m_summonedCard.modifyHitPoints(-m_damage.amount);

        Sequence m_damageSequence = DOTween.Sequence();

        m_damageSequence.AppendCallback(() =>
            {
                m_monsterView.damage(m_damage);

                if (m_summonedCard.hitPoints <= 0)
                {
                    m_cardState = null;
                    m_shadow.gameObject.SetActive(false);
                }
                
                m_castleOverlay.gameObject.SetActive(true);
                m_damageLabel.gameObject.SetActive(true);
                m_damageLabel.text = string.Format("-{0}", m_damage.amount);
                m_damageLabel.AnimationManager.PlayAnimation();
                m_gameCard.damage(m_damage);
                m_gameCard.updateView();

            });
        m_damageSequence.Append(m_castle.transform.DOPunchScale(Vector3.up * 0.1f, m_damageDuration));
        m_damageSequence.OnComplete(() =>
            {
                m_castleOverlay.gameObject.SetActive(false);
                m_damageLabel.gameObject.SetActive(false);
               
            });
    }

    public void gameOver()
    {
        m_gameStateManager.changeState(GameStateKey.GAME_OVER);
    }

    public IEnumerator waitForPlayerSwapMonster()
    {
        m_gameHud.modifyEssence(99);

        m_gameStateManager.changeState(GameStateKey.KNOCKED_OUT_SWAP);

        while (!m_gameStateManager.isState(GameStateKey.WAIT_FOR_PLAYER_INTERACTION))
        {
            yield return false;
        }

        if (isMonsterKnockedOut)
            yield return StartCoroutine(waitForPlayerSwapMonster());
           
    }

    public void summonStarterCard()
    {
        summonMonsterCard(m_playerHand.activeCard);
    }

    public void summonMonsterCard(GameCardView p_gameCard)
    {
        m_gameCard = p_gameCard;
        m_summonedCard = m_gameCard.cardState;
        m_abilityCard = m_summonedCard.abilityState;

        summonMonster(m_summonedCard);
        m_gameHud.abilityHud.updateView(m_abilityCard);
        m_playerHand.setActiveCard(m_gameCard);
    }

    public void summonMonster()
    {
        m_monsterView.resetView();
        m_monsterSummonEffect.PlayWithOnKeyFrameEvent("Summon", OnKeyFrameSummon);
    }

    public void unsummonMonster()
    {
        m_monsterUnsummonTop.PlayWithOnKeyFrameEvent("Unsummon_Top", OnKeyFrameUnsummon);
        m_monsterUnsummonBottom.Play("Unsummon_Bottom");
    }

    private void OnKeyFrameSummon(SpriteAnimation p_animation, 
                                  SpriteAnimationData p_data, int p_frame, string p_animationName)
    {
        if (p_frame == 8)
        {
            m_shadow.gameObject.SetActive(true);
        }

        if (p_frame == 22)
        {
            m_monsterView.updateView(m_cardState);
        }
    }

    private void OnKeyFrameUnsummon(SpriteAnimation p_animation, 
                                    SpriteAnimationData p_data, int p_frame, string p_animationName)
    {
        if (p_frame == 5)
        {
            m_monsterView.resetView();
            m_shadow.gameObject.SetActive(false);
        }

        if (p_frame == 10)
        {
            summonMonster();
        }

    }


}
