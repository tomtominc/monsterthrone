﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

public class PlayerHand : MonoBehaviour
{
    private List < GameCardView > m_cardsInHand;
    private GameCardView m_activeCard;
    private GameController m_gameController;

    public GameCardView activeCard
    {
        get { return m_activeCard; }
    }

    public List < GameCardView > cardsInHand
    {
        get { return m_cardsInHand; }
    }

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;  
    }

    public bool hasAvailableCards()
    {
        return m_cardsInHand.Any(x => x.cardState.hitPoints > 0);
    }

    public void configureHand()
    {
        m_cardsInHand = transform.GetComponentsInChildren<GameCardView>().ToList();
        m_activeCard = transform.GetChild(0).GetComponent < GameCardView >();
    }

    public void setEssence(int p_amount)
    {
        m_cardsInHand.ForEach(l_card => l_card.setEssenceAmount(p_amount));
    }

    public void setActiveCard(GameCardView p_activeCard)
    {
        m_activeCard = p_activeCard;
        m_gameController.gameHud.abilityHud.updateView(m_activeCard.cardState.abilityState);

    }

    #region Animations

    public Sequence animateHandSwap(GameCardView p_activeCard)
    {
        Sequence l_sequence = DOTween.Sequence();

        int l_siblingIndex = p_activeCard.siblingIndex;

        for (int i = l_siblingIndex - 1; i >= 0; i--)
        {
            GameCardView l_cardView = transform.GetChild(i).GetComponent < GameCardView >();
            GameCardView l_placement = transform.GetChild(i + 1).GetComponent < GameCardView >();

            l_sequence.Join(l_cardView.moveInHand(l_placement));
        }

        l_sequence.OnComplete(() =>
            {
                for (int i = l_siblingIndex - 1; i >= 0; i--)
                {
                    GameCardView l_cardView = transform.GetChild(i).GetComponent < GameCardView >();
                    l_cardView.siblingIndex = i + 1;
                    l_cardView.setCardInactive();
                    l_cardView.resetContainerPosition();
                }
            });

        return l_sequence;
    }

    #endregion
}
