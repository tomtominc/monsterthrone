﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using Newtonsoft.Json;

[System.Serializable]
public class BlockSettings
{
    public List < BlockData > allowedBlocks;
}

[System.Serializable]
public class BlockData : ISelectable
{
    public string blockId;
    public float spawnProbability;

    public int healthOverride;

    public float GetSelectionWeight()
    {
        return spawnProbability;
    }

    [JsonIgnore]
    public float AdjustedSelectionWeight { get; set; }

}
