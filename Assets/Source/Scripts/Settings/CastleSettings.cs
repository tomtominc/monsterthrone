﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class CastleSettings
{
    public string name;
    public string background;
    [ListDrawerSettings()]
    public List < Chapter > chapters;

    public Chapter getChapter(string p_chapter)
    {
        Chapter l_chapter = chapters.Find(i => i.name == p_chapter);
        return l_chapter;
    }

    public int getChapterIndex(Chapter p_chapter)
    {
        return chapters.IndexOf(p_chapter);
    }
}
