﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using Newtonsoft.Json;

[System.Serializable]
public class ItemSettings
{
    public List < ItemData > allowedItems;
}

[System.Serializable]
public class ItemData : ISelectable
{
    public Item itemId;
    public float spawnProbability;

    public float GetSelectionWeight()
    {
        return spawnProbability;
    }

    [JsonIgnore]
    public float AdjustedSelectionWeight { get; set; }
}
