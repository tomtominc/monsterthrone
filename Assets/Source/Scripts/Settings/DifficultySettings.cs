﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DifficultySettings
{
    [Header("Probabilities & Ratios")]
    public float blockProbability = 5f;
    public float itemProbability = 1f;
    public float coinProbability = 1f;
    public float emptyProbability = 4f;

    [Header("Lower Hit Points Settings")]
    public int startingMinBlockHitPoints = 1;
    public int turnsToUpgradeMinBlockHitPoints = 5;
    public int upgradeMinHitPoints = 1;

    [Header("Upper Hit Points Settings")]
    public int startingMaxBlockHitPoints = 2;
    public int turnsToUpgradeMaxBlockHitPoints = 1;
    public int upgradeMaxHitPoints = 1;
}
