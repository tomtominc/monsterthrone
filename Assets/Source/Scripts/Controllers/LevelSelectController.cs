﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectController : ViewController
{
    [SerializeField]
    private LevelSelectView m_levelSelectView;

    private CastleSettings m_castleSettings;
    private Chapter m_chapter;
    private Level m_level;

    public override void init(ApplicationManager<AppState> p_applicationManager)
    {
        base.init(p_applicationManager);

        m_levelSelectView.initialize(this);
    }

    public void backToMainMenu()
    {
        m_applicationManager.applicationChangeState(AppState.MAIN_MENU);   
    }

    public void changeCastle(CastleSettings p_castleSettings, Chapter p_chapter, Level p_level)
    {
        m_playerData.content.playerProgress.currentCastle = p_castleSettings.name;
        m_playerData.content.playerProgress.currentChapter = p_chapter.name;
        m_playerData.content.playerProgress.currentLevel = p_level.name;

    }

    public void goToGame()
    {
        m_applicationManager.applicationChangeState(AppState.GAME);
    }
}
