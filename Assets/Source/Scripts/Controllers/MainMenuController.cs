﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;
using PlayFab;
using AsPerSpec;

// Aspect resolutions

// 213x284

// 160x284

public class MainMenuController : ViewController
{
    [BoxGroup("Main Menu Carousel")][SerializeField]
    private CarouselToggler m_mainMenuCarousel;
    [BoxGroup("Carousel Menus")][SerializeField]
    private CollectionMenu m_collectionMenu;
    [BoxGroup("Carousel Menus")][SerializeField]
    private RectTransform m_adventureMenu;
    [BoxGroup("Carousel Menus")][SerializeField]
    private RectTransform m_shopMenu;
    [BoxGroup("Currency HUD")][SerializeField]
    private Text levelLabel;
    [BoxGroup("Currency HUD")][SerializeField]
    private UIEnergyBar levelBar;
    [BoxGroup("Currency HUD")][SerializeField]
    private Text coinLabel;
    [BoxGroup("Currency HUD")][SerializeField]
    private Text gemLabel;
    [BoxGroup("Princess Chest")][SerializeField]
    private UIEnergyBar m_princessTokenBar;
    [BoxGroup("Free Chest")][SerializeField]
    private DailyChestView m_dailyChestView;
    [BoxGroup("Chest Slots")][SerializeField]
    private ChestSlotContainer m_chestSlotContainer;
    [BoxGroup("Reward Popup")][SerializeField]
    private RewardPopup m_rewardPopup;

    private const float m_updateInterval = 60f;
    private float m_timeSinceLastUpdate = m_updateInterval;

    public override void init(ApplicationManager<AppState> p_applicationManager)
    {
        base.init(p_applicationManager);

        m_mainMenuCarousel.OnSnapStarted += onSnapStarted;
        m_mainMenuCarousel.OnSnapMoved += onSnapMoved;
        m_mainMenuCarousel.OnSnapEnded += onSnapEnded;
    }

    public override void onPreTransitionIn()
    {
        m_chestSlotContainer.init(this);
        m_dailyChestView.updateView(m_playerData.content.dailyChestModel);
        m_timeSinceLastUpdate = 0f;

        m_collectionMenu.initialize(m_gameServiceProvider);
        m_collectionMenu.switchStates(CollectionMenu.State.INFO);
    }

    public override void onPostTransitionIn()
    {
        coinLabel.text = m_playerData.content.inventory.getItem("coin").count.ToString("D2");
        gemLabel.text = m_playerData.content.inventory.getItem("gem").count.ToString("D2");

        levelLabel.text = string.Format("lv {0}", m_playerData.content.level.ToString("D2"));
        levelBar.SetSliderValues(0, Algorithm.getExperienceForLevel(m_playerData.content.level), 0);
        levelBar.FillValue(m_playerData.content.experience);

        m_princessTokenBar.FillValue(m_playerData.content.inventory.getItem("princessToken").count);
    }

    public override void update()
    {
        m_timeSinceLastUpdate -= Time.deltaTime;

        // update every minute
        if (m_timeSinceLastUpdate <= 0f)
        {
            m_timeSinceLastUpdate = m_updateInterval;
            m_chestSlotContainer.update();
            m_dailyChestView.updateView();
        }
    }

    public void openRewardPopup()
    {
        List < RewardData > l_rewards = new List < RewardData >();
        l_rewards.Add(new RewardData() { id = "Skeleton", value = 20 });
        l_rewards.Add(new RewardData() { id = "Goblin", value = 8 });
        l_rewards.Add(new RewardData() { id = "Harpy", value = 12 });
        m_rewardPopup.open(this, l_rewards);
    }

    #region Scene Switch

    public void goToMainGame()
    {
        m_applicationManager.applicationChangeState(AppState.LEVEL_SELECT);   
    }

    #endregion

    #region Main menu Carousel

    public void onSnapStarted(Toggle l_towards)
    {
        //Debug.LogFormat("On Snap Started {0}", l_towards.name);
    }

    public void onSnapMoved(Toggle l_towards)
    {
        //Debug.LogFormat("On Snap Moved {0}", l_towards.name);
    }

    public void onSnapEnded(Toggle l_towards)
    {
        //Debug.LogFormat("On Snap Ended {0}", l_towards.name);
    }

    #endregion

    #region Daily Chest

    public void openDailyChest()
    {
        m_gameServiceProvider.requestTime(openDailyChest, handleError);
    }

    private void openDailyChest(DateTime p_time)
    {
        m_dailyChestView.open(p_time);
        m_timeSinceLastUpdate = 0f;
        save();
    }

    #endregion

    private void handleError(int p_errorCode, string p_errorMessage)
    {
        Debug.Log(p_errorMessage);
    }
}
