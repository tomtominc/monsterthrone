﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewController : MonoBehaviour
{
    [SerializeField]
    protected bool m_savePeriodically;
    [SerializeField]
    protected float m_saveInterval = 180f;

    protected ApplicationManager<AppState> m_applicationManager;
    protected GameServiceProvider m_gameServiceProvider;
    protected PlayerService m_playerService;
    protected MonsterThronePlayerData m_playerData;
    protected MonsterThroneTitleService m_titleService;
    protected MonsterThroneGameModel m_gameModel;

    protected float m_timeSinceLastSave;


    #region Properties

    public ApplicationManager<AppState> applicationManager
    {
        get { return m_applicationManager; }
    }

    public GameServiceProvider gameServiceProvider
    {
        get { return m_gameServiceProvider; }
    }

    public PlayerService playerService
    {
        get { return m_playerService; }
    }

    public MonsterThronePlayerData playerData
    {
        get { return m_playerData; }
    }

    public MonsterThroneTitleService titleService
    {
        get { return m_titleService; }
    }

    public MonsterThroneGameModel gameModel
    {
        get { return m_gameModel; }
    }

    public Inventory inventory
    {
        get { return playerData.content.inventory; }
    }

    #endregion

    public virtual void init(ApplicationManager<AppState> p_applicationManager)
    {
        m_applicationManager = p_applicationManager;
        m_gameServiceProvider = m_applicationManager.gameServiceProvider;
        m_playerService = m_gameServiceProvider.playerService;
        m_titleService = m_gameServiceProvider.titleService as MonsterThroneTitleService;
        m_playerData = m_playerService.playerData as MonsterThronePlayerData;
        m_gameModel = m_titleService.gameModel;
    }

    public virtual void onPreTransitionIn()
    {
        
    }

    public virtual void onPostTransitionIn()
    {
        
    }

    private void Update()
    {
        update();
        updateSave();
    }

    public virtual void update()
    {
         
    }

    public virtual void updateSave()
    {
        if (!m_savePeriodically)
            return;

        m_timeSinceLastSave += Time.deltaTime;

        if (m_timeSinceLastSave >= m_saveInterval)
            save();
    }

    public virtual void save()
    {
        m_gameServiceProvider.updatePlayerData();
        m_timeSinceLastSave = 0;
    }
}
