﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : ViewController
{
    #region Members

    [SerializeField]
    protected GameStateManager m_gameStateManager;
    [SerializeField]
    protected AbilityManager m_abilityManager;
    [SerializeField]
    protected GameHud m_gameHud;
    [SerializeField]
    protected GridManager m_gridManager;
    [SerializeField]
    protected GameManager m_gameManager;
    [SerializeField]
    protected GameWorld m_gameWorld;
    [SerializeField]
    protected CastleManager m_castleManager;
    [SerializeField]
    protected GameDialogue m_dialogue;
    [SerializeField]
    protected Player m_player;
    [SerializeField]
    protected Camera m_gameCamera;

    #endregion

    #region Properties

    public GameStateManager gameStateManager
    {
        get { return m_gameStateManager; }
    }

    public AbilityManager abilityManager
    {
        get { return m_abilityManager; }
    }

    public GameWorld gameWorld
    {
        get { return m_gameWorld; }
    }

    public GameHud gameHud
    {
        get { return m_gameHud; }
    }

    public GameManager gameManager
    {
        get { return m_gameManager; }
    }

    public GridManager gridManager
    {
        get { return m_gridManager; }
    }

    public CastleManager castleManager
    {
        get { return m_castleManager; }
    }

    public GameDialogue dialogue
    {
        get { return m_dialogue; }
    }

    public Player player
    {
        get { return m_player; }
    }

    public Camera gameCamera
    {
        get { return m_gameCamera; }
    }

    public GameData gameData
    {
        get { return m_gameData; }
    }

    #endregion

    public override void init(ApplicationManager<AppState> p_applicationManager)
    {
        base.init(p_applicationManager);

        m_gameData = new GameData();

        m_gameHud.initialize(this);
        m_gameWorld.initialize(this);
        m_castleManager.initialize(this);
        m_dialogue.initialize(this);
        m_abilityManager.initialize(this);
        m_gridManager.initialize(this);
        m_gameManager.initialize(this);
        m_player.initialize(this);
        m_gameStateManager.initialize(this);

    }

    private GameData m_gameData;
}
