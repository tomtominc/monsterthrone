﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TextFx;
using DG.Tweening;
using System;

public class ServiceLoadingController : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_loadingBar;
    [SerializeField]
    private TextFxUGUI m_loadingLabel;

    private RectTransform m_rectTransform;
    private Vector2 m_barSize;

    private string m_loadingText
    {
        get { return (m_loadingBar.sizeDelta.x / m_barSize.x).ToString("P0"); }
    }

    public void load(float p_duration, float percent)
    {
        m_rectTransform = GetComponent < RectTransform >();

        m_barSize = m_rectTransform.sizeDelta;
        m_barSize.y = m_loadingBar.sizeDelta.y;
        m_barSize.x *= percent;

        m_loadingBar.sizeDelta = new Vector2(0f, m_barSize.y);
        m_loadingLabel.text = "0%";

        m_loadingBar.DOSizeDelta(m_barSize, p_duration)
            .OnUpdate(() =>
            {
                m_loadingLabel.text = m_loadingText;
            });
    }

    public void finishedLoading()
    {
        m_loadingBar.sizeDelta = m_barSize;
        m_loadingLabel.text = m_loadingText;
    }
}
