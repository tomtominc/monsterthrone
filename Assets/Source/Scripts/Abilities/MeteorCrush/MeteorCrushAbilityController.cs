﻿using System;
using UnityEngine;
using System.Collections;
using Framework;
using System.Collections.Generic;
using DG.Tweening;

public class MeteorCrushAbilityController : AbilityController
{
    [SerializeField] protected Transform m_meteorPrefab;

    protected bool m_finished = false;

    public override IEnumerator useAbility()
    {
        m_gameWorld.monsterView.changeState(MonsterView.State.ATTACK, () => MeteorCrush());

        while (!m_finished)
            yield return false;

        Destroy(gameObject);
    }

    protected void MeteorCrush()
    {
        int l_meteorCount = Mathf.Clamp(m_ability.cardLevel.count, 0, m_gridManager.GridViews.Count);

        Sequence l_sequence = DOTween.Sequence();
        Selector l_selector = new Selector();
        List < GridView > l_gridViews = new List < GridView >();

        for (int i = 0; i < l_meteorCount; i++)
        {
            GridView l_gridView = null;

            while (l_gridView == null || l_gridViews.Contains(l_gridView))
                l_gridView = l_selector.Single(m_gridManager.GridViews);

            l_gridViews.Add(l_gridView);
        }

        for (int i = 0; i < l_gridViews.Count; i++)
        {
            BlockView l_blockView = ((BlockView)l_gridViews[i]);
            Transform l_meteor = Instantiate < Transform >(m_meteorPrefab);
            Vector3 l_endPosition = l_blockView.position + Vector3.up * 0.5f;
            Vector3 l_startPosition = l_endPosition + Vector3.up;
            Meteor l_meteorScript = l_meteor.GetComponent < Meteor >();

            l_meteorScript.transform.position = l_startPosition;
            // l_sequence.Append(l_meteorScript.Spawn(l_startPosition, 0.5f, i * l_timeBetweenMeteorSpawns));
            l_sequence.Append(l_meteorScript.transform.DOMove(l_endPosition, 0.5f).SetEase(Ease.InQuad));

            l_sequence.AppendCallback(() =>
                {
                    l_blockView.damage(m_ability.cardLevel.power);
                    l_meteorScript.Explode();
                });
        }

        l_sequence.OnComplete(() => m_finished = true);

    }
}
