﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Meteor : MonoBehaviour
{
    public enum State 
    {
        SPAWN,
        EXECUTE,
    }

    [SerializeField] private SpriteAnimation m_animator;
    [SerializeField] private SpriteRenderer m_renderer;

    public Tween Spawn(Vector2 p_endPosition, float p_duration, float p_delay)
    {
        m_renderer.enabled = false;
        transform.position = p_endPosition + (Vector2.up * 0.2f);

        return transform.DOMove(p_endPosition, p_duration).SetDelay(p_delay)
            .OnStart(() => m_renderer.enabled = true);
    }

    public void Explode()
    {
        m_animator.PlayWithCallBack(State.EXECUTE, (x, y) =>
            {
                Destroy(gameObject);
            });
    }
}
