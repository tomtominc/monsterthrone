﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AbilityManager : MonoBehaviour
{
    protected GameController m_gameController;
    protected GameStateManager m_gameStateManager;
    protected GameHud m_gameHud;

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gameStateManager = m_gameController.gameStateManager;
        m_gameHud = m_gameController.gameHud;
    }

    public virtual void useAbility(CardStateModel p_ability, Action p_onComplete)
    {
        StartCoroutine(doAbility(getAbility(p_ability), p_onComplete));
    }

    protected AbilityController getAbility(CardStateModel p_ability)
    {
        Debug.Log(p_ability.card.prefab);

        GameObject l_abilityObject = Instantiate < GameObject >(p_ability.card.getPrefab()); 
        AbilityController l_abilityController = l_abilityObject.GetComponent < AbilityController >();
        l_abilityController.initialize(m_gameController, p_ability);
        return l_abilityController;
    }

    protected IEnumerator doAbility(AbilityController p_controller, Action p_onComplete)
    {
        yield return StartCoroutine(p_controller.useAbility());

        if (null != p_onComplete)
            p_onComplete();
    }
}
