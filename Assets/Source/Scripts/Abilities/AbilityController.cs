﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityController : MonoBehaviour
{
    protected GameController m_gameController;
    protected AbilityManager m_abilityManager;
    protected GameHud m_gameHud;
    protected CardStateModel m_ability;
    protected GameWorld m_gameWorld;
    protected GridManager m_gridManager;
    protected PlayerHand m_playerHand;
    protected Camera m_gameCamera;

    public virtual void initialize(GameController p_gameController, CardStateModel p_ability)
    {
        m_gameController = p_gameController;
        m_abilityManager = m_gameController.abilityManager;
        m_gameHud = m_gameController.gameHud;
        m_gridManager = m_gameController.gridManager;
        m_gameWorld = m_gameController.gameWorld;
        m_ability = p_ability;
        m_playerHand = m_gameController.gameHud.playerHand;
        m_gameCamera = m_gameController.gameCamera;
    }

    public abstract IEnumerator useAbility();

    public Vector2 getRandomPositionOutsideScreen()
    {
        Bounds l_bounds = m_gameCamera.OrthographicBounds();
        Vector2 l_outerSpawnMin = l_bounds.min;
        Vector2 l_outSpawnMax = l_bounds.max;

        Vector2 l_position = Vector2.zero;
        
        // top/bottom (true) or left/right (false)
        if (Random.Range(0, 2) > 0)
        {

            // set x position randomly
            l_position.x = Random.Range(l_outerSpawnMin.x, l_outSpawnMax.x);

            // spawn on top (true) or bottom (false)
            if (Random.Range(0, 2) > 0)
            {

                // set the z position to top
                l_position.y = l_outSpawnMax.y;

            }
            else
            {

                // set the z position to bottom
                l_position.y = l_outerSpawnMin.y;

            }

        }
        else
        {

            // set z position randomly
            l_position.y = Random.Range(l_outerSpawnMin.y, l_outSpawnMax.y);

            // spawn on left (true) or right (false)
            if (Random.Range(0, 2) > 0)
            {

                // set the x position to left
                l_position.x = l_outerSpawnMin.x;

            }
            else
            {

                // set the x position to right
                l_position.x = l_outerSpawnMin.x;
            }
        }

        return l_position;
    }
}
