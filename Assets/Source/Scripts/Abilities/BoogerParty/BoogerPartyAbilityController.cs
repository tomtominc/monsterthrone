﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;

public class BoogerPartyAbilityController : AbilityController
{
    [SerializeField] protected Transform m_boogerPrefab;

    protected bool m_finished = false;

    public override IEnumerator useAbility()
    {
        m_gameWorld.monsterView.changeState(MonsterView.State.ATTACK, BoogerParty);

        while (!m_finished)
            yield return false;

        Destroy(gameObject);

    }

    protected void BoogerParty()
    {
        int l_boogerCount = Mathf.Clamp(m_ability.cardLevel.count, 0, m_gridManager.GridViews.Count);

        Sequence l_sequence = DOTween.Sequence();
        Selector l_selector = new Selector();
        List < GridView > l_gridViews = new List<GridView>();

        for (int i = 0; i < l_boogerCount; i++)
        {
            GridView l_gridView = null;

            while (l_gridView == null || l_gridViews.Contains(l_gridView))
                l_gridView = l_selector.Single(m_gridManager.GridViews);

            l_gridViews.Add(l_gridView);
        }

        for (int i = 0; i < l_gridViews.Count; i++)
        {
            BlockView l_blockView = ((BlockView)l_gridViews[i]);
            Transform l_booger = Instantiate < Transform >(m_boogerPrefab);
            Vector3 l_endPosition = l_blockView.position;
            Vector3 l_startPosition = new Vector3(l_endPosition.x, -4f);
            //Booger l_boogerScript = l_booger.GetComponent < Booger >();

            l_booger.position = l_startPosition;
            l_sequence.Join(l_booger.DOMove(l_endPosition, 0.5f)
                .SetDelay(i * 0.25f)
                .SetEase(Ease.InQuad)
                .OnComplete(() =>
                    {
                        CardStatusData l_statusData = new CardStatusData();
                        l_statusData.cardStatus = CardStatus.SLIMED;
                        l_statusData.turns = m_ability.cardLevel.turns;

                        l_blockView.damage(m_ability.cardLevel.power);
                        l_blockView.setStatus(l_statusData);

                        Destroy(l_booger.gameObject);
                    }));
        }

        l_sequence.OnComplete(() => m_finished = true);

    }
}
