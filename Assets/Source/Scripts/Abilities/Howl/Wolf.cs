﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : Projectile
{
    public void flipX(bool p_flipX)
    {
        m_spriteRenderer.flipX = p_flipX;
    }

    public override Transform getHitSpark()
    {
        Transform l_hitSpark = base.getHitSpark();
        SpriteRenderer l_hitSparkRenderer = l_hitSpark.GetComponent < SpriteRenderer >();
        l_hitSparkRenderer.flipX = m_spriteRenderer.flipX;
        return l_hitSpark;
    }
}
