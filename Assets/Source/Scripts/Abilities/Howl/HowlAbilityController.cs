﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;

public class HowlAbilityController : AbilityController
{
    [SerializeField] 
    protected Transform m_wolfPrefab;
    protected bool m_finished;

    protected bool m_useAbility;

    public override IEnumerator useAbility()
    {
        m_gameWorld.monsterView.changeState(MonsterView.State.ATTACK, howl);

        while (!m_useAbility)
            yield return false;
        
        int l_wolfCount = m_ability.cardLevel.count;

        for (int i = 0; i < l_wolfCount; i++)
        {
            int l_column = i % 2 > 0 ? 3 : 4;
            int l_row = i % 2 > 0 ? 0 : 5;
            Vector2 l_startPosition = m_gridManager.GetPositionFromGrid(l_row, l_column);
            l_startPosition.x = i % 2 > 0 ? -4f : 4f;
            l_startPosition.y += 4f;

            Vector2 l_direction = i % 2 > 0 ? Vector2.right : Vector2.left;

            Transform l_wolf = Instantiate(m_wolfPrefab);
            l_wolf.position = l_startPosition;

            Wolf l_wolfProjectile = l_wolf.GetComponent<Wolf>();
            l_wolfProjectile.initialize(m_gameController, m_ability);
            l_wolfProjectile.flipX(i % 2 < 1);
            l_wolfProjectile.fire(l_direction, true);

            yield return new WaitForSeconds(1f);
        }

        yield return new WaitForSeconds(1f);

        Destroy(gameObject);
    }

    protected void howl()
    {
        m_useAbility = true;
    }

}
