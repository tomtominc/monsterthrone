﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityCommand
{
    protected AbilityManager m_abilityController;

    public virtual void Init(AbilityManager p_abilityController)
    {
        m_abilityController = p_abilityController;    
    }

    public abstract IEnumerator DoCommand(Card p_ability, Dictionary < string, object > p_data);
}

public class MeteorCrushAbilityCommand : AbilityCommand
{
    public override IEnumerator DoCommand(Card p_ability, Dictionary<string, object> p_data)
    {
        yield break;
    }
}
