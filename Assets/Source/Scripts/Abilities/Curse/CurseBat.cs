﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CurseBat : MonoBehaviour
{
    private CardStateModel m_ability;

    public void initialize(CardStateModel p_ability)
    {
        m_ability = p_ability;    
    }

    public Sequence getMoveSequence(BlockView p_target, Vector2 p_startPos, Vector2 p_endPos, float p_delay)
    {
        Sequence l_sequence = DOTween.Sequence();

        l_sequence.Append(transform.DOMove(p_endPos, 2f)
            .SetSpeedBased()
            .SetDelay(p_delay)
            .SetEase(Ease.InQuad));

        l_sequence.AppendCallback(() =>
            {
                CardStatusData l_statusData = new CardStatusData();
                l_statusData.cardStatus = CardStatus.CURSED;
                l_statusData.turns = m_ability.cardLevel.turns;
                l_statusData.power = m_ability.cardLevel.power;

                p_target.setStatus(l_statusData);
                p_target.changeState(GridView.State.DAMAGE);
            });
        
        l_sequence.Append(transform.DOPunchPosition(Vector3.down, 0.3f, 1, 10));

        l_sequence.Append(transform.DOMove(p_startPos, 2f)
            .SetSpeedBased()
            .SetEase(Ease.InQuad));

        l_sequence.OnComplete(() => Destroy(gameObject));

        return l_sequence;
    }
}
