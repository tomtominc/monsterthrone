﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;

public class CurseAbilityController : AbilityController
{
    [SerializeField]
    private Transform m_curseBatPrefab;
    private bool m_finished;

    public override IEnumerator useAbility()
    {
        m_gameWorld.monsterView.changeState(MonsterView.State.ATTACK, Curse);

        while (!m_finished)
            yield return false;

        Destroy(gameObject);
    }

    private void Curse()
    {
        int l_batCount = Mathf.Clamp(m_ability.cardLevel.count, 0, m_gridManager.GridViews.Count);

        Sequence l_sequence = DOTween.Sequence();
        Selector l_selector = new Selector();
        List < GridView > l_gridViews = new List<GridView>();

        for (int i = 0; i < l_batCount; i++)
        {
            GridView l_gridView = null;

            while (l_gridView == null || l_gridViews.Contains(l_gridView))
                l_gridView = l_selector.Single(m_gridManager.GridViews);

            l_gridViews.Add(l_gridView);
        }

        for (int i = 0; i < l_batCount; i++)
        {
            BlockView l_blockView = l_gridViews[i] as BlockView;

            Transform l_curseBat = Instantiate(m_curseBatPrefab);
            CurseBat l_curseBatScript = l_curseBat.GetComponent < CurseBat >();

            Vector2 l_startPos = new Vector2(Random.Range(-4f, 4f), -4f);
            Vector2 l_endPos = l_blockView.position + Vector3.up;

            l_curseBat.position = l_startPos;
            l_curseBatScript.initialize(m_ability);
            l_sequence.Join(l_curseBatScript.getMoveSequence(l_blockView, l_startPos, l_endPos, i * 0.25f));
        }

        l_sequence.OnComplete(() => m_finished = true);
    }
}
