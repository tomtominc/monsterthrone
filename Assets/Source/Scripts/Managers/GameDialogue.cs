﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TextFx;
using Framework;
using UnityEngine.UI;

public class GameDialogue : MonoBehaviour
{
    private enum DialogueActionType
    {
        WAIT,
        INVOKE
    }

    private enum DialogueAction
    {
        SET_START_STATE,
        EVENT,
        SET_STATE
    }

    [SerializeField]
    private Text m_dialogue;
    [SerializeField]
    private float m_delayTime = 0.1f;

    private GameController m_controller;
    private GameStateManager m_stateManager;
    private Level m_level;
    private GameDialogueData m_dialogueData;
    private GameEvent m_eventPending;
    private bool m_isWaiting;

    public void initialize(GameController p_controller)
    {
        m_controller = p_controller;
        m_stateManager = m_controller.gameStateManager;
        configureDialogue();
        startDialogue();
    }

    public void setDialogue(Text p_dialogue)
    {
        m_dialogue = p_dialogue;
    }

    private void configureDialogue()
    {
        m_level = m_controller.castleManager.level;

        if (string.IsNullOrEmpty(m_level.dialogue))
            return;

        TextAsset l_dialogueAsset = Resources.Load < TextAsset>(m_level.dialogue);
        m_dialogueData = JsonUtility.FromJson<GameDialogueData>(l_dialogueAsset.text);
        m_dialogueData.initialize();
    }

    private void startDialogue()
    {
        StartCoroutine(updateDialogue());
    }

    private IEnumerator updateDialogue()
    {
        while (!m_dialogueData.isEmpty())
        {
            string l_next = m_dialogueData.getNext();
            bool l_isAction = l_next.Contains("<Action>");
            bool l_isContinue = l_next.Contains("<Continue>");

            if (l_isAction)
            {
                DialogueActionType l_actionType = getActionType(l_next);

                if (l_actionType.Equals(DialogueActionType.WAIT))
                {
                    configureWaitAction(l_next);
                }
                else
                {
                    DialogueAction l_action = getAction(l_next);
                    invokeAction(l_action, l_next);
                }

                while (m_isWaiting)
                {
                    yield return false;
                }
            }
            else
            {
                if (l_isContinue)
                    l_next = l_next.Replace("<Continue>", "");
                
                m_dialogue.text = l_next;

                if (!l_isContinue)
                {
                    while (!Input.GetMouseButtonDown(0))
                        yield return false;

                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }

    private DialogueActionType getActionType(string p_text)
    {
        if (p_text.Contains("[WAIT]"))
            return DialogueActionType.WAIT;
         
        return DialogueActionType.INVOKE;  
    }

    private DialogueAction getAction(string p_text)
    {
        string l_actionName = p_text.Split(',')[1];
        DialogueAction l_action = Enum<DialogueAction>.Parse(l_actionName);
        return l_action;
    }

    private void invokeAction(DialogueAction p_action, string p_text)
    {
        switch (p_action)
        {
            case DialogueAction.SET_START_STATE:
                m_stateManager.changeStartingState(getGameStateKey(p_text));
                break;
            case DialogueAction.EVENT:
                EventManager.publish(this, getGameEvent(p_text), null);
                break;
            case DialogueAction.SET_STATE:
                m_stateManager.changeState(getGameStateKey(p_text));
                break;
        }
    }

    private GameStateKey getGameStateKey(string p_text)
    {
        string l_stateName = p_text.Split(',')[2];
        GameStateKey l_state = Enum<GameStateKey>.Parse(l_stateName);
        return l_state;
    }

    private GameEvent getGameEvent(string p_text)
    {
        string l_eventName = p_text.Split(',')[2];
        GameEvent l_event = Enum<GameEvent>.Parse(l_eventName);
        return l_event;
    }

    private void configureWaitAction(string p_text)
    {
        m_isWaiting = true;
        m_eventPending = Enum<GameEvent>.Parse(p_text.Split(',')[1]);
        EventManager.subscribe(m_eventPending, pendingEventHandler);
    }

    private void pendingEventHandler(IMessage p_message)
    {
        EventManager.unsubscribe(m_eventPending, pendingEventHandler);
        m_isWaiting = false;
    }

    public bool hasDialogue()
    {
        return false;
    }

    public IEnumerator displayTypeWriter(string p_text)
    {
        // reset the paragraph text
        m_dialogue.text = string.Empty;

        // keep local start and end tag variables 
        string startTag = string.Empty;
        string endTag = string.Empty;

        for (int i = 0; i < p_text.Length; i++)
        {
            char c = p_text[i];

            // check to see if we're starting a tag
            if (c == '<')
            {
                // make sure we don't already have a starting tag
                // don't check for ending tag because we set these variables at the 
                // same time
                if (string.IsNullOrEmpty(startTag))
                {
                    // store the current index 
                    int currentIndex = i;

                    for (int j = currentIndex; j < p_text.Length; j++)
                    {
                        // add to our starting tag
                        startTag += p_text[j].ToString();

                        // check to see if we're going to end the tag
                        if (p_text[j] == '>')
                        {
                            // set our current index to the end of the tag
                            currentIndex = j;
                            // set our letter starting point to the current index (when we continue this will be currentIndex++)
                            i = currentIndex;

                            // find the end tag that goes with this tag
                            for (int k = currentIndex; k < p_text.Length; k++)
                            {
                                char next = p_text[k];

                                // check to see if we've reached our end tags start point
                                if (next == '<')
                                    break;

                                // if we have not increment currentindex
                                currentIndex++;
                            }
                            break;
                        }
                    }

                    // we start at current index since this is where our ending tag starts
                    for (int j = currentIndex; j < p_text.Length; j++)
                    {
                        // add to the ending tag
                        endTag += p_text[j].ToString();

                        // once the ending tag is finished we break out
                        if (p_text[j] == '>')
                        {
                            break;
                        }
                    }
                }
                else
                {
                    // go through the text and move past the ending tag
                    for (int j = i; j < p_text.Length; j++)
                    {
                        if (p_text[j] == '>')
                        {
                            // set i = j so we can start at the position of the next letter
                            i = j;
                            break;
                        }
                    }
                    // we reset our starting and ending tag
                    startTag = string.Empty;
                    endTag = string.Empty;
                }

                // continue to get the next character in the sequence
                continue;

            }

            m_dialogue.text += string.Format("{0}{1}{2}", startTag, c, endTag);

            yield return new WaitForSeconds(m_delayTime);
        }
    }
}
