﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using PlayFab.ClientModels;
using PlayFab;
using System.Linq;

public enum ChestAwardError
{
    NONE,
    CHEST_SLOTS_FULL
}

public class ChestManager : Manager
{
    private ChestSlotData m_startingChest;
    private Action<ChestAwardError> m_onCompleteAwardChest;
    private Action<bool> m_onCompleteStartChest;

    private PlayerDataContent m_content
    {
        get { return ((MonsterThronePlayerData)m_playerData).content; }
    }

    public override void init(GameServiceProvider p_gameServiceProvider)
    {
        base.init(p_gameServiceProvider);
    }

    public void awardChest(ChestType p_chestType, Action<ChestAwardError> p_onComplete)
    {
        m_onCompleteAwardChest = p_onComplete;

        if (!hasEmptySlot())
        {
            m_onCompleteAwardChest(ChestAwardError.CHEST_SLOTS_FULL);
        }
        else
        {
            ChestSlotData p_data = getEmptySlot();
            p_data.chestType = p_chestType;
            p_data.lastStatus = ChestSlotStatus.EMPTY;
            p_data.chestStatus = ChestSlotStatus.WAIT;
            m_onCompleteAwardChest(ChestAwardError.NONE);
        }
    }

    public void startUnlock(ChestSlotData p_chestSlotData, Action<bool> p_onComplete)
    {
        m_onCompleteStartChest = p_onComplete;
        m_startingChest = p_chestSlotData;

        m_gameServiceProvider.requestTime(unlockChest, handleError);
    }

    private void unlockChest(DateTime p_time)
    {
        int l_hoursToUnlock = TimeUtils.getHoursForUnlock(m_startingChest.chestType);
        m_startingChest.chestStatus = ChestSlotStatus.UNLOCKING;
        m_startingChest.lastStatus = ChestSlotStatus.WAIT;
        m_startingChest.timeUntilUnlock = p_time.AddHours(l_hoursToUnlock);
        m_onCompleteStartChest(true);
    }

    private void handleError(int p_errorCode, string p_errorMessage)
    {
        Debug.Log(p_errorMessage);
        m_onCompleteStartChest(false);
    }

    private bool hasEmptySlot()
    {
        return m_content.chestSlots.Any(l_slot => l_slot.chestStatus == ChestSlotStatus.EMPTY);
    }

    private ChestSlotData getEmptySlot()
    {
        return m_content.chestSlots.First(l_slot => l_slot.chestStatus == ChestSlotStatus.EMPTY);
    }
}
