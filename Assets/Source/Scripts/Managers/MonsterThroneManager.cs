﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AppState
{
    SPLASH_LOADING,
    SERVICE_LOADING,
    MAIN_MENU,
    GAME,
    LEVEL_SELECT
}

public class MonsterThroneManager : ApplicationManager<AppState>
{
    protected override void applicationEnter()
    {

    }

    protected override void applicationUpdate()
    {

    }

    protected override void applicationQuit()
    {

    }
}
