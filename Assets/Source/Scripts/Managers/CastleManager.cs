﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleManager : MonoBehaviour
{
    private GameController m_controller;
    private MonsterThroneGameModel m_gameModel;
    private MonsterThronePlayerData m_playerData;
    private DifficultySettings m_difficultySettings;
    private CastleSettings m_castleSettings;
    private Chapter m_chapter;
    private Level m_level;
    private LevelOverrideData m_levelOverride;

    public CastleSettings castleSettings
    {
        get { return m_castleSettings; }
    }

    public DifficultySettings difficultySettings
    {
        get { return m_difficultySettings; }
    }

    public Chapter chapter
    {
        get { return m_chapter; }
    }

    public Level level
    {
        get { return m_level; }
    }

    public bool hasLevelOverride
    {
        get { return null != m_levelOverride && m_levelOverride.hasContents(); }
    }

    public LevelOverrideData levelOverride
    {
        get { return m_levelOverride; }
    }

    public void initialize(GameController p_controller)
    {
        m_controller = p_controller;

        m_gameModel = m_controller.gameModel;
        m_playerData = m_controller.playerData;
        m_difficultySettings = m_gameModel.difficultySettings;
        m_castleSettings = m_gameModel.getCastle(m_playerData.content.playerProgress.currentCastle);
        m_chapter = m_castleSettings.getChapter(m_playerData.content.playerProgress.currentChapter);
        m_level = m_chapter.getLevel(m_playerData.content.playerProgress.currentLevel);

        configureFormationOverride();
    }

    private void configureFormationOverride()
    {
        string l_override = m_level.waveOverride;

        if (string.IsNullOrEmpty(l_override))
            return;

        TextAsset l_overrideFile = Resources.Load<TextAsset>(l_override);
        m_levelOverride = JsonUtility.FromJson<LevelOverrideData>(l_overrideFile.text);
        m_levelOverride.initialize();
    }
}

[System.Serializable]
public class LevelOverrideData
{
    public void initialize()
    {
        m_formation = new Stack<BlockData>();
        rows = formation.Count / 5;

        for (int i = 0; i < formation.Count; i++)
        {
            string[] l_block = formation[i].Split(':');

            BlockData l_blockData = new BlockData();
            l_blockData.blockId = l_block[0];

            if (l_block.Length >= 2)
                l_blockData.healthOverride = int.Parse(l_block[1]);

            m_formation.Push(l_blockData);
        }
    }

    public int rows;
    public int currentRow;
    public List < string > formation;
    private Stack<BlockData> m_formation;

    public BlockData getNext()
    {
        return m_formation.Pop();
    }

    public bool hasContents()
    {
        return m_formation.Count > 0;
    }
}
