﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : SingletonBehaviour < GameManager >
{
    [SerializeField] private Player player;
    [SerializeField] private AudioKey m_gameOverSound;

    private GameController m_gameController;
    private GridManager m_gridManager;

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gridManager = m_gameController.gridManager;
    }

    private void GameOver()
    {
        m_gridManager.clearGrid();

        DisplayPlayer(false);
    }

    private void DisplayPlayer(bool isShown)
    {
        player.gameObject.SetActive(isShown);
    }

}