﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

public class Player : MonoBehaviour
{
    [SerializeField] 
    private float m_speed = 10;
    [SerializeField] 
    private float m_spawnFrequency = 0.25f;

    [SerializeField]
    private Ball m_ballPrefab;
    [SerializeField]
    private UISpriteText m_remainingBalls;
    [SerializeField]
    private SpriteRenderer m_renderer;
    [SerializeField]
    private Transform m_trajectoryDotParent;

    private GameController m_controller;
    private GameStateManager m_gameStateManager;
    private List<Ball> m_balls;
    private int m_numBallsStopped = 0;
    private SpriteAnimation m_ballAnimator;

    public void initialize(GameController p_controller)
    {
        m_controller = p_controller;
        m_gameStateManager = m_controller.gameStateManager;
        m_ballAnimator = m_renderer.GetComponent < SpriteAnimation >();

        transform.position = m_controller.gridManager.BottomLimit;
        configureBalls();

        gameObject.SetActive(true);
    }

    private void addBalls()
    {
        int l_ballCount = m_controller.inventory.getItem("Ball").count;
        int l_ballsToAdd = l_ballCount - m_balls.Count;

        for (int i = 0; i < l_ballsToAdd; i++)
        {
            addBall();
        }
    }

    public void updateTrajectoryTutorial(Vector2 p_targetAngleMinMax, out int p_touchState)
    {
        //0 = button up not target angle, 

        //1 = button down not at target angle, 

        //2 = button down target angle, 

        //3 button up target angle

        p_touchState = 0;

        if (Input.GetMouseButton(0))
        {
            Vector3 l_currentTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            float l_angle = AngleInRad(transform.position, l_currentTouchPosition);

            if (p_targetAngleMinMax.InBetween(l_angle))
                p_touchState = 2;
            else
                p_touchState = 1;

            if (l_currentTouchPosition.y > transform.position.y)
            {
                revBall(l_currentTouchPosition);
            }
            else
            {
                idleBall();
                p_touchState = 0;
            }
        }
        else
        {
            idleBall();
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector3 l_currentTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            float l_angle = AngleInRad(transform.position, l_currentTouchPosition);

            if (p_targetAngleMinMax.InBetween(l_angle) && l_currentTouchPosition.y > transform.position.y)
            {
                p_touchState = 3;
                endFiring(l_currentTouchPosition - transform.position);
            }
            else
            {
                p_touchState = 0;
                idleBall();
            }
        }
    }

    public float AngleInRad(Vector3 vec1, Vector3 vec2)
    {
        return Mathf.Atan2(vec2.y - vec1.y, vec2.x - vec1.x);
    }


    public void updateTrajectory()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 l_currentTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (l_currentTouchPosition.y > transform.position.y)
            {
                EventManager.publish(this, GameEvent.START_FIRING, null);
            }
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 l_currentTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (l_currentTouchPosition.y > transform.position.y)
            {
                revBall(l_currentTouchPosition);
            }
            else
            {
                idleBall();
            }
        }
        else
        {
            idleBall();
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector3 l_currentTouchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (l_currentTouchPosition.y > transform.position.y)
            {
                endFiring(l_currentTouchPosition - transform.position);
            }
            else
            {
                idleBall();
            }
        }
    }

    private void revBall(Vector3 p_touchPosition)
    {
        if (!m_ballAnimator.currentAnimationName.Equals("REV"))
            m_ballAnimator.Play("REV");

        SetTrajectoryPoints(transform.position, p_touchPosition - transform.position);
    }

    private void idleBall()
    {
        if (!m_ballAnimator.currentAnimationName.Equals("IDLE"))
            m_ballAnimator.Play("IDLE");
        
        m_trajectoryDotParent.gameObject.SetActive(false);
    }

    public void configureBalls()
    {
        m_balls = new List<Ball>();
        addBall();
    }


    public void addBall()
    {
        Ball ball = (Ball)Instantiate(m_ballPrefab);
        ball.transform.position = transform.position;
        ball.gameObject.SetActive(false);
        ball.HitFloor += onBallHitFloor;
        m_balls.Add(ball);
    }

    private void onBallHitFloor(Ball ball)
    {
        ball.gameObject.SetActive(false);

        if (m_numBallsStopped == 0)
        {
            transform.position = new Vector3(ball.transform.position.x, transform.position.y);
            displayPlayer(true);
        }

        ball.transform.position = transform.position;
        m_numBallsStopped++;
        ;

        if (hasAllBallsStopped())
        {
            endTurn();
        }

    }

    public void startTurn()
    {
        addBalls();
        displayBallsRemaining(true);
        setBallsRemaining(m_balls.Count);
    }

    private void endTurn()
    {
        EventManager.publish(this, GameEvent.PLAYER_TURN_END, null);
        m_gameStateManager.changeState(GameStateKey.GRID_UPDATE);
    }

    private void fire(Vector3 movement)
    {
        if (movement == Vector3.zero)
            return;
        SetTrajectoryPoints(transform.position, movement);
    }

    private bool hasAllBallsStopped()
    {
        return m_numBallsStopped == m_balls.Count;
    }

    private void endFiring(Vector3 movement)
    {
        m_trajectoryDotParent.gameObject.SetActive(false);
        displayPlayer(false);
        m_numBallsStopped = 0;
        StartCoroutine(SpawnBallsCoroutine(movement));

        EventManager.publish(this, GameEvent.END_FIRING, null);
    }

    private IEnumerator SpawnBallsCoroutine(Vector3 movement)
    {
        Vector3 startPosition = transform.position;
        movement.z = 0f;

        for (int i = 0; i < m_balls.Count; i++)
        {
            Ball ball = m_balls[i];
            ball.transform.position = startPosition + movement * 0.2f * 0;

            ball.gameObject.SetActive(true);
            ball.Direction = movement.normalized;
            ball.Speed = m_speed;

            setBallsRemaining(m_balls.Count - 1 - i);

            yield return new WaitForSeconds(m_spawnFrequency);
        }
        yield return new WaitForSeconds(2 * m_spawnFrequency);
        displayBallsRemaining(false);
    }

    void setBallsRemaining(int count)
    {
        m_remainingBalls.Text = "+" + count.ToString();
    }

    void displayBallsRemaining(bool isShown)
    {
        m_remainingBalls.gameObject.SetActive(isShown);
    }

    void displayPlayer(bool isShown)
    {
        m_renderer.enabled = isShown;
    }

    void SetTrajectoryPoints(Vector3 p_origin, Vector2 p_direction)
    {
        float l_spacing = 1.25f;
        float l_time = 0.1f;
        Vector2 l_direction = p_direction.normalized;
        Vector2 l_origin = p_origin;
        float l_angle = Mathf.Rad2Deg * (Mathf.Atan2(l_direction.y, l_direction.x));
        Collider2D l_lastHit = null;
        Physics2D.queriesStartInColliders = false;

        for (int i = 0; i < m_trajectoryDotParent.childCount; i++)
        {
            RaycastHit2D l_hit = Physics2D.Raycast(l_origin, l_direction, 
                                     l_spacing * l_time, m_balls[0].collisionLayers);

            Vector3 l_pos = Vector3.zero;

            if (l_hit.transform != null)
            {
                l_pos = l_hit.point;
                l_lastHit = l_hit.collider;
                l_origin = l_hit.point;
                l_direction = Vector2.Reflect(l_direction, l_hit.normal).normalized;
                l_angle = Mathf.Rad2Deg * (Mathf.Atan2(l_direction.y, l_direction.x));
                l_time = 0f;
            }
            else
            {
                float l_dx = l_spacing * l_time * Mathf.Cos(l_angle * Mathf.Deg2Rad);
                float l_dy = l_spacing * l_time * Mathf.Sin(l_angle * Mathf.Deg2Rad);
                l_pos = new Vector3(l_origin.x + l_dx, l_origin.y + l_dy);
            }


            m_trajectoryDotParent.GetChild(i).position = l_pos;
            l_time += 0.1f;
        }

        Physics2D.queriesStartInColliders = true;
        m_trajectoryDotParent.gameObject.SetActive(true);
    }

}