﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Shake : MonoBehaviour
{
    private const int PIXELS_PER_UNIT = 32;
    private const float PIXEL_TO_UNIT = 1f / (float)PIXELS_PER_UNIT;

    private Vector2 m_shakeDirection;
    private Vector3 m_cachedPosition;

    private Action m_complete;
    private bool m_constant;

    private void Awake()
    {
        m_cachedPosition = transform.position;    
    }

    public void shake(Vector2 p_direction, Action p_complete)
    {
        m_shakeDirection = p_direction;
        m_constant = false;
        m_complete = p_complete;

        InvokeRepeating("updateShake", 0.0333333f, 0.0333333f);
    }

    public void shakeConstant(Vector2 p_direction)
    {
        m_shakeDirection = p_direction;
        m_constant = true;
        InvokeRepeating("updateShake", 0.0333333f, 0.0333333f);
    }

    public void stopShake()
    {
        m_constant = false;
        CancelInvoke("updateShake");
    }

    public void Update()
    {
        if (!m_shakeDirection.Equals(Vector2.zero))
        {
            Vector3 l_pos = m_cachedPosition;
            transform.position = l_pos + (Vector3)(m_shakeDirection * PIXEL_TO_UNIT);
        }
        else if (!transform.position.Equals(m_cachedPosition))
        {
            transform.position = m_cachedPosition;
            if (null != m_complete)
            {
                m_complete();
                m_complete = null;
            }
        }
    }

    private void updateShake()
    {
        if (m_shakeDirection.x != 0)
        {
            m_shakeDirection.x = -m_shakeDirection.x;
            if (!m_constant)
            {
                if (m_shakeDirection.x > 0)
                    m_shakeDirection.x--;
                if (m_shakeDirection.x < 0)
                    m_shakeDirection.x++;
            }
        }

        if (m_shakeDirection.y != 0)
        {
            m_shakeDirection.y = -m_shakeDirection.y;
            if (!m_constant)
            {
                if (m_shakeDirection.y > 0)
                    m_shakeDirection.y--;
                if (m_shakeDirection.y < 0)
                    m_shakeDirection.y++;
            }
        }
    }
}
