﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Algorithm
{
    private static int m_experienceConstant = 10;
    private static float m_experienceScale = 1.1f;

    public static int getExperienceForLevel(int p_level)
    {
        return (int)Mathf.Pow(p_level * m_experienceConstant, m_experienceScale);
    }

    public static int getExperienceForCardLevel(CardStateModel p_cardState)
    {
        return (int)Mathf.Pow(p_cardState.level * m_experienceConstant, m_experienceScale);
    }
}
