﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class TimeUtils
{
    private static Dictionary<ChestType, int> m_chestToHoursMap = new Dictionary<ChestType, int>()
    {
        { ChestType.MONSTER_CHEST, 3 },
        { ChestType.EPIC_CHEST, 10 },
        { ChestType.LEGENDARY_CHEST, 24 }
    };

    public static int getHoursForUnlock(ChestType p_chestType)
    {
        return m_chestToHoursMap[p_chestType];
    }

    public static int getHoursForFreeChest()
    {
        return 4;
    }

    public static string getTimeFormat(TimeSpan p_time)
    {
        string l_timeFormated = string.Empty;

        if (p_time.Hours > 0)
        {
            l_timeFormated = string.Format("{0}H {1}M", p_time.Hours, p_time.Minutes);
        }
        else if (p_time.Minutes > 0)
        {
            l_timeFormated = string.Format("{0}M", p_time.Minutes);
        }
        else
        {
            l_timeFormated = string.Format("1M");
        }

        return l_timeFormated;
    }

    public static int getGemCostOfTime(int p_minutes)
    {
        return (int)(p_minutes * 0.3f);
    }
}
