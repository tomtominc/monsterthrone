﻿
public class Constants
{
    public static readonly string BALL_TAG = "Ball";
    public static readonly string FLOOR_TAG = "Floor";
    public static readonly string HITABLE_TAG = "Hitable";
    public static readonly string PICKABLE_TAG = "Pickable";
    public static readonly string WALL_TAG = "Wall";
}
