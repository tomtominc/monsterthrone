﻿using UnityEngine.UI;
using UnityEngine;
using System;

public class Block : MonoBehaviour, IHitable
{
    [SerializeField]
    protected Text m_hitPointsLabel;

    public event Action<IHitable> destroyed = delegate {};

    protected int m_hitPoints;

    public void setHitPoints(int p_hitPoints)
    {
        m_hitPoints = p_hitPoints;
        updateView();
    }

    public void updateView()
    {
        if (m_hitPoints < 0)
            m_hitPoints = 0;

        if (m_hitPointsLabel != null)
            m_hitPointsLabel.text = m_hitPoints.ToString();
    }

    public void collision(Ball ball, Vector3 inDirection, Vector3 inNormal, Vector3 offsetDirection, out Vector3 toDirection)
    {
        m_hitPoints--;//TODO ball.stats.power;
        toDirection = Vector3.Reflect(inDirection, inNormal) + offsetDirection;

        if (m_hitPoints <= 0)
        {
            destroyed(this);
            Destroy(gameObject);
        }
    }
}
