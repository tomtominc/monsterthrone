﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public enum ProjectileCardType
    {
        BLOCK,
        ABILITY
    }

    [SerializeField]
    protected float m_speed;
    [SerializeField]
    protected LayerMask m_collisionLayers;
    [SerializeField]
    protected Transform m_hitSparkPrefab;
    [SerializeField]
    protected ProjectileCardType m_cardType;

    protected bool m_invincible;
    protected Vector2 m_direction;
    protected bool m_canMove;
    protected CardStateModel m_stateModel;
    protected List < HitBehaviour > m_hitBehaviours;
    protected SpriteRenderer m_spriteRenderer;

    public void initialize(GameController p_controller, CardStateModel p_stateModel)
    {
        m_stateModel = p_stateModel;
        m_spriteRenderer = GetComponent < SpriteRenderer >();
    }

    public virtual void fire(Vector2 p_direction, bool p_invincible = false)
    {
        m_direction = p_direction;
        m_canMove = true;
        m_invincible = p_invincible;

        if (m_invincible)
        {
            m_hitBehaviours = new List<HitBehaviour>();
        }

    }

    private void Update()
    {
        if (!m_canMove)
            return;

        transform.position += (Vector3)m_direction * Time.deltaTime * m_speed;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, m_direction, 
                               m_speed * Time.deltaTime * 1.2f,
                               m_collisionLayers);

        if (!hit.transform)
            return;

        HitBehaviour l_hitBehaviour = hit.collider.GetComponent < HitBehaviour >();

        if (l_hitBehaviour == null)
            return;

        if (m_invincible)
        {
            if (m_hitBehaviours.Contains(l_hitBehaviour))
                return;
        }

        if (m_cardType == ProjectileCardType.BLOCK)
            l_hitBehaviour.hit(m_stateModel.card.damageInfo);
        if (m_cardType == ProjectileCardType.ABILITY)
            l_hitBehaviour.hit(new DamageInfo()  { amount = m_stateModel.cardLevel.power });    


        Transform l_hitSpark = getHitSpark();
        l_hitSpark.position = hit.point;
        l_hitSpark.GetComponent < SpriteAnimation >().PlayWithCallBack("Hit", (x, y) => Destroy(l_hitSpark.gameObject));

        if (m_invincible)
        {
            m_hitBehaviours.Add(l_hitBehaviour);
            StartCoroutine(stopTime());
        }
        else
        {
            m_canMove = false;
            Destroy(gameObject);
        }
    }

    public virtual Transform getHitSpark()
    {
        return Instantiate(m_hitSparkPrefab);
    }

    public IEnumerator stopTime()
    {
        Time.timeScale = 0;

        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        Time.timeScale = 1;
    }

}
