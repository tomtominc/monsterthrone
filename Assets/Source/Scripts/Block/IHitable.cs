﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IHitable
{
    event Action<IHitable> destroyed;

    void collision(Ball ball, Vector3 inDirection, 
                   Vector3 inNormal, Vector3 offsetDirection, out Vector3 toDirection);
}

public interface ICollidable
{
    void damage(int amount);
}

