﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAppState : ApplicationState<AppState>
{
    [SerializeField]
    private string m_sceneName = "Game";

    private GameController m_gameController;

    public override void enter()
    {
        m_applicationManager.sceneController.SwitchScenes(m_sceneName, onPreTransitionIn, onPostTransitionIn);
    }

    public override void update()
    {
        
    }

    public override void exit()
    {
        
    }

    private void onPreTransitionIn()
    {
        m_gameController = FindObjectOfType < GameController >();
        m_gameController.init(m_applicationManager);
        m_gameController.onPreTransitionIn();
    }

    private void onPostTransitionIn()
    {
        m_gameController.onPostTransitionIn();
    }
}
