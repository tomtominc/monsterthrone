﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAppState : ApplicationState<AppState>
{
    [SerializeField]
    public string m_sceneName = "MainMenu";

    private MainMenuController m_mainMenuController;

    public override void enter()
    {
        m_applicationManager.sceneController.SwitchScenes(m_sceneName, onPreTransitionIn, onPostTransitionIn);
    }

    public override void update()
    {
        
    }

    public override void exit()
    {
        
    }

    private void onPreTransitionIn()
    {
        m_mainMenuController = FindObjectOfType < MainMenuController >();
        m_mainMenuController.init(m_applicationManager);
        m_mainMenuController.onPreTransitionIn();
    }

    private void onPostTransitionIn()
    {
        m_mainMenuController.onPostTransitionIn();
    }
}
