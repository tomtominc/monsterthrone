﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.SharedModels;
using PlayFab;

public class ServiceLoadingAppState : ApplicationState<AppState>
{
    [SerializeField]
    protected string m_sceneName;
    [SerializeField]
    protected float m_constantLoadTime = 2.5f;

    protected ServiceLoadingController m_controller;
    protected bool m_serviceLoaded = false;

    public override void enter()
    {
        m_serviceLoaded = false;
        m_applicationManager.sceneController.SwitchScenes(m_sceneName, null, loadServices);
    }

    public override void update()
    {
        
    }

    public override void exit()
    {
        
    }

    protected void loadServices()
    {
        setupServiceProviderHooks();
        requestTitleData();

        m_controller = FindObjectOfType < ServiceLoadingController >();
        m_controller.load(m_constantLoadTime, 1f);

        StartCoroutine(loadGame());
    }

    protected void goToMainGame()
    {
        m_serviceLoaded = true;
    }

    protected IEnumerator loadGame()
    {
        yield return new WaitForSeconds(m_constantLoadTime);

        while (!m_serviceLoaded)
            yield return false;

        m_controller.finishedLoading();

        yield return new WaitForSeconds(0.5f);

        MonsterThronePlayerData l_playerData = m_applicationManager.
            gameServiceProvider.
            playerService.playerData as MonsterThronePlayerData;

        l_playerData.content.playerProgress.currentLevel = "Monster Protection";

        if (!l_playerData.content.tutorial.complete)
            m_applicationManager.applicationChangeState(AppState.GAME);
        else
            m_applicationManager.applicationChangeState(AppState.MAIN_MENU);
    }

    #region Game Service Provider Hook

    protected virtual void setupServiceProviderHooks()
    {
        m_applicationManager.gameServiceProvider.requestTitleDataSuccess += onRequestTitleDataSuccess;
        m_applicationManager.gameServiceProvider.requestPlayerDataSuccess += onRequestPlayerDataSuccess;
        m_applicationManager.gameServiceProvider.requestEconomyDataSuccess += onRequestEconomyDataSuccess;

        m_applicationManager.gameServiceProvider.requestTitleDataFailure += onRequestTitleDataFailed;
        m_applicationManager.gameServiceProvider.requestPlayerDataFailure += onRequestPlayerDataFailed;
        m_applicationManager.gameServiceProvider.requestEconomyDataFailure += onRequestEconomyDataFailed;
    }

    #endregion

    #region Request Helpers

    protected virtual void requestTitleData()
    {
        m_applicationManager.gameServiceProvider.requestTitleData();
    }

    protected virtual void requestPlayerData()
    {
        m_applicationManager.gameServiceProvider.requestPlayerData();
    }

    protected virtual void requestEconomyData()
    {
        m_applicationManager.gameServiceProvider.requestEconomyData();
    }

    protected virtual void doDebugStuff()
    {
        ChestManager l_chestManager = m_applicationManager.gameServiceProvider.chestManager;

        l_chestManager.awardChest(ChestType.MONSTER_CHEST, (p_error) =>
            {
                goToMainGame();
            });
    }

    #endregion

    #region Callbacks

    private void onRequestTitleDataSuccess(PlayFabResultCommon p_result)
    {
        requestPlayerData();
    }

    private void onRequestPlayerDataSuccess(PlayFabResultCommon p_result)
    {
        requestEconomyData();
    }

    private void onRequestEconomyDataSuccess(PlayFabResultCommon p_result)
    {
        doDebugStuff();
    }

    private void onRequestTitleDataFailed(PlayFabError p_error)
    {
        Debug.LogWarningFormat("Failed Requesting Title data: [0}", p_error);
    }

    private void onRequestPlayerDataFailed(PlayFabError p_error)
    {
        Debug.LogWarningFormat("Failed Requesting Player data: [0}", p_error);
    }

    private void onRequestEconomyDataFailed(PlayFabError p_error)
    {
        Debug.LogWarningFormat("Failed Requesting Economy data: [0}", p_error);
    }

    #endregion
}
