﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashAppState : ApplicationState<AppState>
{
    [SerializeField]
    protected string m_splashSceneName = "Splash";
    [SerializeField]
    protected float m_timeOut = 2f;

    private float m_currentTime;

    public override void enter()
    {
        m_applicationManager.sceneController.LoadScene(m_splashSceneName);
        m_currentTime = m_timeOut;
    }

    public override void update()
    {
        m_currentTime -= Time.deltaTime;

        if (m_currentTime <= 0f)
        {
            m_applicationManager.applicationChangeState(AppState.SERVICE_LOADING);
            return;
        }

        if (Input.anyKeyDown)
            m_applicationManager.applicationChangeState(AppState.SERVICE_LOADING);
            
    }

    public override void exit()
    {
        
    }
}
