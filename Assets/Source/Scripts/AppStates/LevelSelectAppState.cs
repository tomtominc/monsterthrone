﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectAppState  : ApplicationState<AppState>
{
    [SerializeField]
    public string m_sceneName = "LevelSelect";

    private LevelSelectController m_levelSelectController;

    public override void enter()
    {
        m_applicationManager.sceneController.SwitchScenes(m_sceneName, onPreTransitionIn, onPostTransitionIn);
    }

    public override void update()
    {

    }

    public override void exit()
    {

    }

    private void onPreTransitionIn()
    {
        m_levelSelectController = FindObjectOfType < LevelSelectController >();
        m_levelSelectController.init(m_applicationManager);
        m_levelSelectController.onPreTransitionIn();
    }

    private void onPostTransitionIn()
    {
        m_levelSelectController.onPostTransitionIn();
    }
}
