﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory
{
    public List < Item > items;

    public void init()
    {
        if (items == null)
            items = new List < Item >();
    }

    public void addItem(string p_item, int p_count)
    {
        init();

        Item l_item = items.Find(i => i.name == p_item);

        if (l_item == null)
        {
            l_item = new Item();
            l_item.name = p_item;
            l_item.count = p_count;
            items.Add(l_item);
        }
        else
        {
            l_item.count += p_count;
        }

    }

    public void setItem(string p_item, int p_count)
    {
        init();

        Item l_item = items.Find(i => i.name == p_item);

        if (l_item == null)
        {
            l_item = new Item();
            l_item.name = p_item;
            l_item.count = p_count;
            items.Add(l_item);
        }
        else
        {
            l_item.count = p_count;
        }

    }

    public void useItem(string p_item, int p_count)
    {
        init();

        Item l_item = items.Find(i => i.name == p_item);

        if (l_item == null)
        {
            Debug.LogFormat("Couldn't use {0} it was not added to the inventory.", p_item);
        }
        else
        {
            l_item.count -= p_count;

            if (l_item.count < 0)
                l_item.count = 0;
        }
    }

    public Item getItem(string p_item)
    {
        init();

        Item l_item = items.Find(i => i.name == p_item);

        if (l_item == null)
        {
            l_item = new Item();
            l_item.name = p_item;
            l_item.count = 0;
            items.Add(l_item);
        }

        return l_item;
    }
}
