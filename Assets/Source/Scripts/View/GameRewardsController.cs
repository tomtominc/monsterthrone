﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameRewardsController : MonoBehaviour
{
    [SerializeField]
    private Image m_chest;
    [SerializeField]
    private Text m_chestLabel;
    [SerializeField]
    private Animator m_animator;

    public void initialize(GameController p_controller)
    {
        m_controller = p_controller;
        m_castleManager = m_controller.castleManager;
        m_level = m_castleManager.level;

        m_chest.sprite = SpriteManager.getSprite(m_level.chestReward.ToString());
        m_chestLabel.text = m_level.chestReward.ToString().Replace("_", " ");
      
    }

    public void revealChest()
    {
        m_animator.Play("REVEAL");
    }

    private GameController m_controller;
    private CastleManager m_castleManager;
    private Level m_level;
}

