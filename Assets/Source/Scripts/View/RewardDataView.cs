﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;

public class RewardDataView : MonoBehaviour
{
    [SerializeField]
    private Text m_rewardNameLabel;
    [BoxGroup("Monster Layout")][SerializeField]
    private RectTransform m_monsterLayout;
    [BoxGroup("Monster Layout")][SerializeField]
    private UIEnergyBar m_energyBar;
    [BoxGroup("Monster Layout")][SerializeField]
    private Text m_countLabel;
    [BoxGroup("Resource Layout")][SerializeField]
    private RectTransform m_resourceLayout;
    [BoxGroup("Resource Layout")][SerializeField]
    private Text m_resourceAmountLabel;

    private ViewController m_controller;
    private MonsterThronePlayerData m_playerData;
    private RewardData m_rewardData;
    private ExperienceBar m_experienceBar;

    public void initialize(ViewController p_controller, RewardData p_rewardData)
    {
        m_controller = p_controller;
        m_playerData = m_controller.playerData;
        m_rewardData = p_rewardData;
        m_experienceBar = m_energyBar.GetComponent < ExperienceBar >();
        updateView();
    }

    public void open()
    {
        gameObject.SetActive(true);
    }

    public void close()
    {
        gameObject.SetActive(false);
    }

    public void updateView()
    {
        CardStateModel l_cardState = m_playerData.content.cardStateDatabase.getCard(m_rewardData.id);

        m_rewardNameLabel.text = l_cardState.id;

        if (l_cardState.card.cardType == CardType.Monster)
        {
            m_monsterLayout.gameObject.SetActive(true);
            m_resourceLayout.gameObject.SetActive(false);
            m_energyBar.SetSliderValues(0, l_cardState.experienceForNextLevel, l_cardState.experience);
            m_experienceBar.updateView(l_cardState);
            m_countLabel.text = string.Format("{0}/{1}", l_cardState.experience, l_cardState.experienceForNextLevel);
        }
    }

    public Sequence animateView()
    {
        Sequence l_sequence = DOTween.Sequence();

        CardStateModel l_cardState = m_playerData.content.cardStateDatabase.getCard(m_rewardData.id);

        if (l_cardState.card.cardType == CardType.Monster)
        {
            l_sequence.Append(m_energyBar.FillValueTween(m_rewardData.value));

            int l_experience = l_cardState.experience;

            l_sequence.Join(
                DOTween.To(() => l_experience, x => l_experience = x, 
                    l_experience + m_rewardData.value, 
                    m_rewardData.value * 0.05f).OnUpdate(() =>
                    {
                        m_countLabel.text = string.Format("{0}/{1}", l_experience, 
                            l_cardState.experienceForNextLevel);
                            
                        l_cardState.experience = l_experience;

                        if (l_experience == l_cardState.experienceForNextLevel)
                            m_experienceBar.updateView(l_cardState);

                    }));

            l_sequence.OnComplete(() => m_experienceBar.updateView(l_cardState));
        }

        return l_sequence;
    }
}
