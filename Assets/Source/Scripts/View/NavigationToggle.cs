﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TextFx;
using DG.Tweening;

public class NavigationToggle : ToggleExtension
{
    [SerializeField]
    private RectTransform m_navigationIcon;
    [SerializeField]
    private TextFxUGUI m_navigationLabel;
    [SerializeField]
    private Vector2 m_preferredSizeOn;
    [SerializeField]
    private Vector2 m_preferredSizeOff;

    protected LayoutElement m_layoutElement;

    public override void init(ToggleGroupExtension p_navigationView)
    {
        base.init(p_navigationView);

        m_layoutElement = GetComponent < LayoutElement>();
    }

    public override void onToggleOn()
    {
        m_layoutElement.DOPreferredSize(m_preferredSizeOn, 0.5f);
        m_navigationIcon.DOAnchorPos(new Vector2(0f, 8f), 0.25f);
        m_navigationLabel.gameObject.SetActive(true);
        m_navigationLabel.AnimationManager.PlayAnimation();
    }

    public override void onToggleOff()
    {
        m_layoutElement.DOPreferredSize(m_preferredSizeOff, 0.5f);
        m_navigationIcon.DOAnchorPos(new Vector2(0f, 0f), 0.25f);
        m_navigationLabel.gameObject.SetActive(false);
    }
}
