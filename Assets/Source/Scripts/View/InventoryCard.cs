﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class InventoryCard : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_cardContainer;

    [SerializeField]
    private RectTransform m_buttonLayout;

    [SerializeField]
    private Toggle m_activeToggle;
    [SerializeField]
    private Button m_infoButton;
    [SerializeField]
    private Button m_upgradeButton;
    [SerializeField]
    private Button m_useButton;
    [SerializeField]
    private Button m_cancelButton;
    [SerializeField]
    private RectTransform m_activeBackground;
    [SerializeField]
    private RectTransform m_activeBackgroundFooter;
    [SerializeField]
    private RectTransform m_lockedOverlay;

    private GameServiceProvider m_serviceProvider;
    private MonsterThronePlayerData m_playerData;
    private CardPopup m_cardPopup;
    private CardStateView m_cardView;
    private CardStateModel m_card;
    private CollectionMenu m_collectionMenu;
    private Button m_lockedOverlayButton;

    private bool m_inDeck
    {
        get { return m_playerData.content.deck.contains(m_card.card.name); }
    }

    public Toggle activeToggle
    {
        get { return m_activeToggle; }
    }

    public CardStateModel cardState
    {
        get { return m_card; }
    }

    public RectTransform cardContainer
    {
        get { return m_cardContainer; }
    }

    public void initialize(GameServiceProvider p_serviceProvider, CollectionMenu p_collectionMenu, 
                           CardStateModel p_card, CardPopup p_cardPopup)
    {
        m_serviceProvider = p_serviceProvider;
        m_playerData = m_serviceProvider.playerService.playerData as MonsterThronePlayerData;
        m_cardPopup = p_cardPopup;
        m_card = p_card;
        m_lockedOverlayButton = m_lockedOverlay.GetComponent < Button >();
        m_collectionMenu = p_collectionMenu;
        m_cardView = GetComponent < CardStateView >();
        m_cardView.initialize(m_serviceProvider, m_card);
        m_cardView.updateView();
    }

    public void updateView()
    {
        m_activeToggle.onValueChanged.RemoveAllListeners();
        m_infoButton.onClick.RemoveAllListeners();
        m_upgradeButton.onClick.RemoveAllListeners();
        m_useButton.onClick.RemoveAllListeners();
        m_cancelButton.onClick.RemoveAllListeners();
        m_lockedOverlayButton.onClick.RemoveAllListeners();

        m_activeToggle.onValueChanged.AddListener(onActiveToggle);
        m_infoButton.onClick.AddListener(onInfoButtonClicked);
        m_upgradeButton.onClick.AddListener(onInfoButtonClicked);
        m_useButton.onClick.AddListener(onUseButtonClicked);
        m_lockedOverlayButton.onClick.AddListener(onInfoButtonClicked);

        m_cancelButton.gameObject.SetActive(false);
        m_lockedOverlay.gameObject.SetActive(!m_card.unlocked);

        if (m_inDeck)
        {
            m_useButton.gameObject.SetActive(false);
        }
        else
        {
            m_useButton.gameObject.SetActive(true);
        }

        if (m_card.upgradable)
        {
            m_infoButton.gameObject.SetActive(false);
            m_upgradeButton.gameObject.SetActive(true);
        }
        else
        {
            m_infoButton.gameObject.SetActive(true);
            m_upgradeButton.gameObject.SetActive(false);
        }
    }

    public void updateViewForEditMode()
    {
        m_activeToggle.onValueChanged.RemoveAllListeners();
        m_infoButton.onClick.RemoveAllListeners();
        m_upgradeButton.onClick.RemoveAllListeners();
        m_useButton.onClick.RemoveAllListeners();
        m_cancelButton.onClick.RemoveAllListeners();

        m_activeToggle.onValueChanged.AddListener(onSwapCard);
    }

    public void updateViewForSwapping()
    {
        m_activeToggle.onValueChanged.RemoveAllListeners();
        m_infoButton.onClick.RemoveAllListeners();
        m_upgradeButton.onClick.RemoveAllListeners();
        m_useButton.onClick.RemoveAllListeners();
        m_cancelButton.onClick.RemoveAllListeners();

        m_lockedOverlay.gameObject.SetActive(false);
        m_cancelButton.gameObject.SetActive(true);
        m_cancelButton.onClick.AddListener(onCancelButtonClicked);
    }

    #region Button Event Callbacks

    private void onSwapCard(bool p_isOn)
    {
        m_collectionMenu.swapCardInDeck(this);
    }

    public void onActiveToggle(bool p_isOn)
    {
        if (p_isOn)
        {
            float l_footerHeight = m_inDeck ? 35f : 55f;

            m_useButton.gameObject.SetActive(!m_inDeck);

            m_activeBackground.gameObject.SetActive(true);
            m_activeBackgroundFooter.sizeDelta = new Vector2(47f, 25f);

            m_activeBackgroundFooter.DOSizeDelta(new Vector2(47f, l_footerHeight), 0.1f)
                .SetEase(Ease.OutBack).OnComplete(() =>
                {
                    m_buttonLayout.gameObject.SetActive(true);
                    m_buttonLayout.DOPunchScale(Vector3.one * 0.1f, 0.3f);
                });

            m_collectionMenu.cardButtonsActive(this);
        }
        else
        {
            m_cardContainer.SetParent(transform, true);

            m_activeBackground.gameObject.SetActive(false);
            m_buttonLayout.gameObject.SetActive(false);
        }
       
    }

    public void onInfoButtonClicked()
    {
        m_activeToggle.isOn = false;
        m_cardPopup.open(m_serviceProvider, m_card);
        m_cardPopup.updateViewCollectionMenu(m_collectionMenu);
    }

    public void onCancelButtonClicked()
    {
        m_activeToggle.isOn = false;

        m_collectionMenu.switchStates(CollectionMenu.State.INFO);
    }

    public void onUseButtonClicked()
    {
        m_activeToggle.isOn = false;

        m_collectionMenu.setActiveCardState(m_card);
        m_collectionMenu.switchStates(CollectionMenu.State.EDIT);
    }

    #endregion
}
