﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StarFxController : MonoBehaviour
{
    [SerializeField]
    private List < GameObject > m_stars;
    [SerializeField]
    private List < Text > m_starCounts;
    [SerializeField]
    private UIEnergyBar m_starsBar;

    public void initialize(GameController p_controller)
    {
        m_controller = p_controller;
        m_castleManager = m_controller.castleManager;
        m_level = m_castleManager.level;
        m_gameData = m_controller.gameData;
        updateView();
    }

    private void updateView()
    {
        m_starCounts[0].text = m_level.star1Requirement.ToString();
        m_starCounts[1].text = m_level.star2Requirement.ToString();
        m_starCounts[2].text = m_level.star3Requirement.ToString();

        if (m_gameData.turns <= m_level.star3Requirement)
            m_starsGiven = 3;
        else if (m_gameData.turns <= m_level.star2Requirement)
            m_starsGiven = 2;
        else if (m_gameData.turns <= m_level.star1Requirement)
            m_starsGiven = 1;
        else
            m_starsGiven = 0;
        
        m_starsBar.SetSliderValues(0, m_level.star1Requirement + m_level.star3Requirement, 0);
        m_gameData.starsGiven = m_starsGiven;

        for (int i = 0; i < m_stars.Count; i++)
        {
            m_stars[i].SetActive(false);
        }
    }

    public Tweener updateStars()
    {
        return m_starsBar.FillValueTween(m_starsBar.maxValue - m_gameData.turns).OnUpdate 
            (() =>
            {
               
                int l_value = m_starsBar.maxValue - (int)m_starsBar.slider.value;

                if (l_value == m_level.star1Requirement)
                    m_stars[0].SetActive(true);

                if (l_value == m_level.star2Requirement)
                    m_stars[1].SetActive(true);

                if (l_value == m_level.star3Requirement)
                    m_stars[2].SetActive(true);
            }
        );
    }


    private GameController m_controller;
    private CastleManager m_castleManager;
    private GameData m_gameData;
    private Level m_level;
    private Sequence m_starSequence;
    private int m_starsGiven;
}
