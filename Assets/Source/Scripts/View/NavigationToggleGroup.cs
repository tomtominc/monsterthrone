﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NavigationToggleGroup : ToggleGroupExtension
{
    private IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        // TODO: don't init yourself dick
        init(null);
    }

    public override void init(IContentToggle p_contentToggle)
    {
        base.init(p_contentToggle);
        animateIn();
    }

    public void animateIn()
    {
        m_rectTransform.DOAnchorPosY(-m_rectTransform.rect.height, 0.5f)
            .From()
            .SetDelay(0.5f)
            .SetEase(Ease.OutBack)
            .OnComplete(() => initToggles());
    }
}
