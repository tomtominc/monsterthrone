﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;

public class GridView : MonoBehaviour, ISelectable
{
    public enum State
    {
        IDLE,
        IDLE_WEAK,
        ATTACK,
        DAMAGE,
        DEATH,
        WALK,
    }

    protected GameController m_gameController;
    protected GridManager m_gridManager;
    protected Vector3 m_bottom;
    protected State m_currentState;
    protected SpriteAnimation m_animator;
    protected BoxCollider2D m_collider;
    protected int m_row;
    protected int m_column;

    public int row
    {
        get { return m_row; }
    }

    public int column
    {
        get { return m_column; }
    }

    public Vector3 position
    {
        get { return transform.position; }
    }

    public Vector3 localPosition
    {
        get { return transform.localPosition; }
    }

    public virtual float GetSelectionWeight()
    {
        return 1 / (position.y + 1);
    }

    public virtual DamageInfo damageInfo
    {
        get { return null; }
    }

    public virtual bool shouldRemoveAtBottom
    {
        get { return false; }
    }

    public virtual bool canAttack
    {
        get { return false; }
    }

    public virtual bool canWalk
    {
        get { return false; }
    }

    public virtual State idleState
    {
        get { return State.IDLE; }
    }

    public virtual int hitPoints
    {
        get { return 0; }
    }

    public virtual void updateStatus()
    {
        
    }

    public float AdjustedSelectionWeight { get; set; }

    public void initialize(GameController p_gameController)
    {
        m_gameController = p_gameController;
        m_gridManager = m_gameController.gridManager;
        m_bottom = m_gridManager.bottom;

        m_animator = GetComponentInChildren < SpriteAnimation >();
        m_collider = GetComponent < BoxCollider2D >();
    }

    public void setGridPosition(int p_row, int p_column)
    {
        m_row = p_row;
        m_column = p_column;
    }

    public virtual IEnumerator doBottomGridAction()
    {
        yield break;
    }

    public void setGridPositionByOffset(int p_rowOffset, int p_columnOffset)
    {
        m_row += p_rowOffset;
        m_column += p_columnOffset;
    }

    protected virtual string m_currentAnimation
    {
        get { return m_currentState.ToString(); }
    }

    public virtual void move(Vector2 endPosition, float duration)
    {
        transform.DOMove(endPosition, duration);
    }

    public virtual void endTurn()
    {
    }

    public virtual void attack()
    {
    }

    public virtual void destroy()
    {
        
    }

    public virtual void changeState(State p_state)
    {
        if (m_animator.animationsByName.ContainsKey(p_state.ToString()))
        {
            m_currentState = p_state;
            m_animator.PlayWithCallBack(m_currentAnimation, onAnimationFinished);
        }
    }

    protected void onAnimationFinished(SpriteAnimation p_animation, SpriteAnimationData p_data)
    {
        if (m_currentState == State.DEATH)
        {
            Destroy(gameObject);
        }
        else if (m_currentState != idleState)
        {
            m_currentState = idleState;
            m_animator.Play(m_currentAnimation);
        }
    }
}
