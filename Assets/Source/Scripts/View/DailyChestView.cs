﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using Sirenix.OdinInspector;

public class DailyChestView : MonoBehaviour
{
    [BoxGroup("Active State Properties")][SerializeField]
    private RectTransform m_activeState;
    [BoxGroup("Active State Properties")][SerializeField]
    private RectTransform m_chest;

    [BoxGroup("Inactive State Properties")][SerializeField]
    private RectTransform m_inactiveState;
    [BoxGroup("Inactive State Properties")][SerializeField]
    private Text m_timerLabel;

    private DailyChestModel m_dailyChestModel;
    private Sequence m_chestJumpSequence;

    private Vector2 m_chestSize;
    private Vector2 m_chestPosition;

    public void updateView(DailyChestModel p_dailyChestModel)
    {
        m_dailyChestModel = p_dailyChestModel;
        m_chestSize = m_chest.sizeDelta;
        m_chestPosition = m_chest.anchoredPosition;
        updateView();
    }

    public void updateView()
    {
        if (m_dailyChestModel == null)
            return;
        
        if (m_dailyChestModel.isReadyNow())
        {
            m_activeState.gameObject.SetActive(true);
            m_inactiveState.gameObject.SetActive(false);
            startChestJump();
        }
        else
        {
            m_activeState.gameObject.SetActive(false);
            m_inactiveState.gameObject.SetActive(true);
            m_timerLabel.text = m_dailyChestModel.getTimeUntilNextChest();
        }
    }

    private void startChestJump()
    {
        if (m_chestJumpSequence != null && m_chestJumpSequence.IsActive())
            m_chestJumpSequence.Kill(true);

        float l_power = 0.2f;
        float l_duration = 1.52f;
        float l_squashTimePercent = 0.1f;
        float l_jumpTimePercent = 0.2f;
        float l_fallTimePercent = 0.1f;
        float l_punchTimePercent = 0.6f;
        float l_jumpPower = 10f;
        Vector2 l_squashSize = new Vector2(m_chestSize.x * (1f + l_power), m_chestSize.y * (1f - l_power));
        Vector2 l_stretchSize = new Vector2(m_chestSize.x * (1f - l_power), m_chestSize.y * (1f + l_power));

        m_chestJumpSequence = DOTween.Sequence();

        // squash 1/19th
        m_chestJumpSequence.Append(
            m_chest.DOSizeDelta(l_squashSize, l_duration * l_squashTimePercent));

        // get jump duration
        float l_jumpDuration = l_duration * l_jumpTimePercent;

        // jump 
        m_chestJumpSequence.Append(
            m_chest.DOAnchorPosY(m_chest.anchoredPosition.y + l_jumpPower, l_jumpDuration));

        // stretch for half the jump
        m_chestJumpSequence.Join(
            m_chest.DOSizeDelta(l_stretchSize, l_jumpDuration * 0.5f));

        // go normal for the rest
        m_chestJumpSequence.Join(
            m_chest.DOSizeDelta(m_chestSize, l_jumpDuration * 0.5f).SetDelay(l_jumpDuration * 0.5f));


        // get fall duration (about half the jump time)
        float l_fallDuration = l_duration * l_fallTimePercent;

        // fall 
        m_chestJumpSequence.Append(
            m_chest.DOAnchorPos(m_chestPosition, l_fallDuration).SetEase(Ease.InQuad));

        // stretch for half the fall
        m_chestJumpSequence.Join(
            m_chest.DOSizeDelta(l_stretchSize, l_fallDuration * 0.5f).SetEase(Ease.Linear));

        // go normal for the rest
        m_chestJumpSequence.Join(
            m_chest.DOSizeDelta(m_chestSize, l_fallDuration * 0.5f)
            .SetDelay(l_fallDuration * 0.5f).SetEase(Ease.Linear));

        // slight movement
        // recover wiggle
        m_chestJumpSequence.Append(
            m_chest.DOPunchScale(new Vector2(0.15f, -0.15f), l_duration * l_punchTimePercent, 5, 10f));

        m_chestJumpSequence.AppendInterval(1f);
        m_chestJumpSequence.SetLoops(-1, LoopType.Restart);
        m_chestJumpSequence.OnComplete(() =>
            {
                m_chest.anchoredPosition = m_chestPosition;
                m_chest.sizeDelta = m_chestSize;
            });

    }

    public void open(DateTime p_time)
    {
        m_dailyChestModel.open(p_time);
    }
}
