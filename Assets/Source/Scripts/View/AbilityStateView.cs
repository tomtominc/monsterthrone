﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityStateView : MonoBehaviour
{
    [SerializeField]
    private Text m_nameLabel;
    [SerializeField]
    private UISpriteText m_costLabel;
    [SerializeField]
    private RectTransform m_powerStat;
    [SerializeField]
    private UISpriteText m_powerLabel;
    [SerializeField]
    private RectTransform m_healStat;
    [SerializeField]
    private UISpriteText m_healLabel;
    [SerializeField]
    private RectTransform m_countStat;
    [SerializeField]
    private UISpriteText m_countLabel;
    [SerializeField]
    private RectTransform m_turnsStat;
    [SerializeField]
    private UISpriteText m_turnsLabel;
    [SerializeField]
    private RectTransform m_percentStat;
    [SerializeField]
    private UISpriteText m_percentLabel;
    [SerializeField]
    private Text m_descriptionLabel;
    [SerializeField]
    private Image m_portrait;
    private CardStateModel m_abilityState;

    public void initialize(GameServiceProvider p_serviceProvider, CardStateModel p_abilityState)
    {
        m_abilityState = p_abilityState;
    }

    public void updateView()
    {
        if (m_abilityState == null)
            return;
        
        if (m_nameLabel != null)
            m_nameLabel.text = m_abilityState.card.name;
        
        if (m_costLabel != null)
            m_costLabel.Text = m_abilityState.card.cost.ToString();


        if (m_descriptionLabel != null)
            m_descriptionLabel.text = m_abilityState.getDescription();

        if (m_portrait != null)
            m_portrait.sprite = m_abilityState.card.getPortrait();

        updateStats();
        updateStatLabels();
    }

    public void updateViewUpgrade()
    {
        if (m_abilityState == null)
            return;
        
        updateView();
        updateStatsForUpgrade();
    }

    public void updateStats()
    {
        if (m_powerStat != null)
        {
            m_powerStat.gameObject.SetActive(m_abilityState.getHasPower());
        }

        if (m_healStat != null)
        {
            m_healStat.gameObject.SetActive(m_abilityState.getHasHeal());
        }

        if (m_countStat != null)
        {
            m_countStat.gameObject.SetActive(m_abilityState.getHasCount());
        }

        if (m_turnsStat != null)
        {
            m_turnsStat.gameObject.SetActive(m_abilityState.getHasTurns());
        }

        if (m_percentStat != null)
        {
            m_percentStat.gameObject.SetActive(m_abilityState.getHasPercent());
        }
    }

    public void updateStatLabels()
    {
        if (m_powerLabel != null)
        {
            m_powerLabel.Text = m_abilityState.cardLevel.power.ToString();
        }

        if (m_healLabel != null)
        {
            m_healLabel.Text = m_abilityState.cardLevel.healAmount.ToString();
        }

        if (m_countLabel != null)
        {
            m_countLabel.Text = m_abilityState.cardLevel.count.ToString();
        }

        if (m_turnsLabel != null)
        {
            m_turnsLabel.Text = m_abilityState.cardLevel.turns.ToString();
        }

        if (m_percentLabel != null)
        {
            m_percentLabel.Text = string.Format("{0}%", m_abilityState.cardLevel.percent * 100f);
        }
    }

   

    public void updateStatsForUpgrade()
    {
        if (m_powerLabel != null)
        {
            string l_formatted = string.Format("{0} +{1}", m_abilityState.cardLevel.power, 
                                     m_abilityState.getLevelStats(m_abilityState.level + 1).power);
            
            m_powerLabel.Text = l_formatted;
        }

        if (m_healLabel != null)
        {
            string l_formatted = string.Format("{0} +{1}", 
                                     m_abilityState.cardLevel.healAmount, 
                                     m_abilityState.getLevelStats(m_abilityState.level + 1).healAmount);
            
            m_healLabel.Text = l_formatted;
        }

        if (m_countLabel != null)
        {
            string l_formatted = string.Format("{0} +{1}", 
                                     m_abilityState.cardLevel.count, 
                                     m_abilityState.getLevelStats(m_abilityState.level + 1).count);
            
            m_countLabel.Text = l_formatted;
        }

        if (m_turnsLabel != null)
        {
            string l_formatted = string.Format("{0} +{1}", 
                                     m_abilityState.cardLevel.turns, 
                                     m_abilityState.getLevelStats(m_abilityState.level + 1).turns);
            
            m_turnsLabel.Text = l_formatted;
        }

        if (m_percentLabel != null)
        {
            string l_formatted = string.Format("{0}% +{1}%", 
                                     m_abilityState.cardLevel.percent * 100f, 
                                     m_abilityState.getLevelStats(m_abilityState.level + 1).percent * 100f);
            
            m_percentLabel.Text = l_formatted;
        }
    }
}
