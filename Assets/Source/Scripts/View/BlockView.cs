﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class BlockView : GridView, HitBehaviour
{
    [SerializeField] private SpriteRenderer m_spriteRenderer;
    [SerializeField] private GameObject m_projectilePrefab;
    [SerializeField] private Vector2 m_projectileStartOffset;
    [SerializeField] private UISpriteText m_hitPointsLabel;
    [SerializeField] private AudioKey m_damagedSound;
    [SerializeField] private AudioKey m_destroyedSound;
    [SerializeField] private int m_projectileSpawnFrame;

    public event Action < GridView > destroyed = delegate{};

    protected int m_hitPoints;
    protected CardStateModel m_cardState;
    protected int m_turnsLeftForStatus = 0;
    protected Transform m_statusAttachment;

    public override DamageInfo damageInfo
    {
        get{ return m_cardState.card.damageInfo; }
    }

    public override int hitPoints
    {
        get{ return m_hitPoints; }
    }

    public override bool canAttack
    {
        get
        { 
            CardStatus l_status = m_cardState.cardStatusData.cardStatus;
            bool l_canAttack = true;

            switch (l_status)
            {
                case CardStatus.FROZEN:
                    l_canAttack = false;
                    break;
                case CardStatus.SLIMED:
                    l_canAttack = false;
                    break;
                case CardStatus.PARALYZED:
                    l_canAttack = false;
                    break;
            }

            return l_canAttack;
        }
    }

    public override bool canWalk
    {
        get
        { 
            CardStatus l_status = m_cardState.cardStatusData.cardStatus;
            bool l_canWalk = true;

            switch (l_status)
            {
                case CardStatus.FROZEN:
                    l_canWalk = false;
                    break;
                case CardStatus.SLIMED:
                    l_canWalk = false;
                    break;
                case CardStatus.PARALYZED:
                    l_canWalk = false;
                    break;
            }

            return l_canWalk;
        }
    }



    public override State idleState
    {
        get
        {
            return m_cardState.cardStatusData.cardStatus == CardStatus.NORMAL ? 
            State.IDLE : State.IDLE_WEAK;
        }
    }

    public void updateView(CardStateModel p_cardState)
    {
        m_cardState = p_cardState;
    }

    public void updateView()
    {
        m_animator.assets.Clear();
        
        m_animator.assets.Add(m_cardState.card.getAnimationAsset());

        changeState(idleState);
    }

    public void setHitPoints(int p_hitPoints)
    {
        m_hitPoints = p_hitPoints;

        if (m_hitPoints < 0)
            m_hitPoints = 0;
        
        setHitPointsLabel(m_hitPoints);
    }

    public void setHitPointsLabel(int p_hitPoints)
    {
        m_hitPointsLabel.Text = p_hitPoints.ToString("D2");
    }

    public override void attack()
    {
        m_currentState = State.ATTACK;

        m_animator.PlayWithOnKeyFrameEvent(m_currentAnimation, (p_animation, p_data, p_frame, p_name) =>
            {
                if (p_frame == m_projectileSpawnFrame)
                    spawnProjectile();
                
                if (p_frame >= p_data.frameDatas.Count - 1)
                    onAnimationFinished(p_animation, p_data);   
            });
    }

    public void spawnProjectile()
    {
        Transform l_projectile = Instantiate(m_projectilePrefab.transform);
        l_projectile.SetParent(transform);
        l_projectile.localPosition = m_projectileStartOffset;
        Projectile l_projectileScript = l_projectile.GetComponent < Projectile >();
        l_projectileScript.initialize(m_gameController, m_cardState);
        l_projectileScript.fire(Vector2.down);
    }

    public override void endTurn()
    {
        updateStatus();
    }

    public override void move(Vector2 endPosition, float duration)
    {
        changeState(State.WALK);
        transform.DOMove(endPosition, duration);
    }

    public override  void destroy()
    {
        AudioManager.Instance.Play(m_destroyedSound, 0);
        m_collider.enabled = false;
        destroyed(this);
        changeState(State.DEATH);
    }

    public override void updateStatus()
    {
        if (m_turnsLeftForStatus <= 0)
            return;
        
        CardStatusData l_statusData = m_cardState.cardStatusData;
        m_turnsLeftForStatus -= 1;

        switch (l_statusData.cardStatus)
        {
            case CardStatus.BURNED:
                damageNonDestruction(l_statusData.power);
                break;

            case CardStatus.CURSED:
                damageNonDestruction(l_statusData.power);
                break;
        }
      
        if (m_turnsLeftForStatus <= 0)
        {
            setStatus(new CardStatusData() { cardStatus = CardStatus.NORMAL });
        }

    }

    public void setStatus(CardStatusData p_statusData)
    {
        CardStatusData l_lastStatusData = m_cardState.cardStatusData;

        m_cardState.cardStatusData = p_statusData;

        if (l_lastStatusData.cardStatus != CardStatus.NORMAL && p_statusData.cardStatus == CardStatus.NORMAL)
        {
            Destroy(m_statusAttachment.gameObject);
            changeState(idleState);
        }

        if (p_statusData.cardStatus != CardStatus.NORMAL)
        {
            m_turnsLeftForStatus = p_statusData.turns;

            m_statusAttachment = m_gameController.gameWorld.
                statusFactory.getStatus(p_statusData.cardStatus);
            
            m_statusAttachment.SetParent(transform);
            m_statusAttachment.localPosition = Vector3.zero;
        }
    }

    public void hit(DamageInfo p_damageInfo)
    {
        damage(p_damageInfo.amount);
    }

    public void damageNonDestruction(int p_damage)
    {
        setHitPoints(m_hitPoints - p_damage);

        if (m_hitPoints > 0)
        {
            AudioManager.Instance.Play(m_damagedSound, 0);
            changeState(State.DAMAGE);
        }
    }

    public void damage(int p_damage)
    {
        setHitPoints(m_hitPoints - p_damage);

        if (m_hitPoints > 0)
        {
            AudioManager.Instance.Play(m_damagedSound, 0);
            changeState(State.DAMAGE);
        }
        else
        {
            destroy();
        }
    }

    public void ballHit(Ball ball, Vector3 direction)
    {
        damage(1);
    }
}


