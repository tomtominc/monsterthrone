﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class CollectionMenu : View
{
    public enum State
    {
        INFO,
        EDIT
    }

    [SerializeField]
    private CardPopup m_cardPopup;
    [SerializeField]
    private InventoryCard m_inventoryCardPrefab;
    [SerializeField]
    private ScrollRect m_collectionScroll;
    [SerializeField]
    private RectTransform m_deckGridLayout;
    [SerializeField]
    private RectTransform m_cardInventoryLayout;
    [SerializeField]
    private ToggleGroup m_activeCardToggleGroup;
    [SerializeField]
    private CardStateView m_swapCardView;
    [SerializeField]
    private RectTransform m_editCardInstructions;
    [SerializeField]
    private RectTransform m_showingCardContainer;
   
    private State m_state;
    private CardStateDatabase m_cardDatabase;
    private CardStateModel m_swapCardModel;

    public RectTransform showingCardContainer
    {
        get { return m_showingCardContainer; }
    }

    public override void initialize(GameServiceProvider p_serviceProvider)
    {
        base.initialize(p_serviceProvider);

        m_cardDatabase = m_playerData.content.cardStateDatabase;
    }

    public void setActiveCardState(CardStateModel p_swapCardModel)
    {
        m_swapCardModel = p_swapCardModel;
    }

    private void updateViewInfoState()
    {
        m_deckGridLayout.DestroyChildren();
        m_cardInventoryLayout.DestroyChildren();
        m_cardInventoryLayout.gameObject.SetActive(true);
        m_swapCardView.gameObject.SetActive(false);
        m_swapCardView.transform.localPosition = Vector3.zero;
        m_editCardInstructions.gameObject.SetActive(false);

        foreach (CardStateModel l_cardState in m_cardDatabase.cardStates)
        {
            if (l_cardState.card == null)
                continue;
            
            if (l_cardState.card.cardType != CardType.Monster)
                continue;
            
            InventoryCard l_inventoryCard = Instantiate(m_inventoryCardPrefab);
            l_inventoryCard.initialize(m_serviceProvider, this, l_cardState, m_cardPopup);
            bool l_inDeck = m_playerData.content.deck.contains(l_cardState.id);
            RectTransform l_layout = l_inDeck ? m_deckGridLayout : m_cardInventoryLayout;
            l_inventoryCard.transform.SetParent(l_layout, false);
            l_inventoryCard.transform.SetAsLastSibling();
            l_inventoryCard.activeToggle.group = m_activeCardToggleGroup;
            l_inventoryCard.updateView();
        }

        List <InventoryCard> m_deckCards = m_deckGridLayout.
            GetComponentsInChildren < InventoryCard >().ToList();

        for (int i = 0; i < m_playerData.content.deck.contents.Count; i++)
        {
            string m_id = m_playerData.content.deck.contents[i];
            InventoryCard l_inventoryCard = m_deckCards.Find(l_card => l_card.cardState.id == m_id);
            l_inventoryCard.transform.SetSiblingIndex(i);
        }
    }

    private void updateViewEditState()
    {
        if (m_swapCardModel == null)
        {
            switchStates(State.INFO);
            return;
        }

        m_swapCardView.initialize(m_serviceProvider, m_swapCardModel);
        m_swapCardView.updateView();
        m_swapCardView.gameObject.SetActive(true);
        m_cardInventoryLayout.gameObject.SetActive(false);
        m_editCardInstructions.gameObject.SetActive(true);

        InventoryCard[] l_deckCards = m_deckGridLayout.GetComponentsInChildren<InventoryCard>();

        foreach (InventoryCard l_deckCard in l_deckCards)
        {
            l_deckCard.updateViewForEditMode();
        }
    }

    public void cardButtonsActive(InventoryCard p_inventoryCard)
    {
        //TODO: center on selection
        p_inventoryCard.cardContainer.SetParent(m_showingCardContainer, true);
    }

    public void swapCardInDeck(InventoryCard m_deckCard)
    {
        m_playerData.content.deck.swapCards(m_swapCardModel.id, m_deckCard.cardState.id);
        m_swapCardView.GetComponent < RectTransform >()
            .DOMove(m_deckCard.GetComponent<RectTransform>().position, 0.1f);
        m_deckCard.GetComponent < RectTransform >()
            .DOMove(m_swapCardView.GetComponent<RectTransform>().position, 0.1f).OnComplete 
            (() => switchStates(State.INFO));

        m_serviceProvider.updatePlayerData();
    }

    public void switchStates(State p_state)
    {
        switch (p_state)
        {
            case State.INFO:
                updateViewInfoState();
                break;
            case State.EDIT:
                updateViewEditState();
                break;
        }
    }
}
