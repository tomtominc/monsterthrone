﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WinPopup : MonoBehaviour
{
    // components needed
    // StarFxController
    // StarBarController
    // GameRewardsController
    // GameRewardData
    // ChestView (instead of game rewards controller)
    // Tutorial Button Layout (RectTransform)
    // Game Button Layout (RectTransform)
    // Confetti Burst FX (GameObject)
    // Confetti Loop FX (GameObject)

    [SerializeField]
    private StarFxController m_starFxController;
    [SerializeField]
    private GameRewardsController m_rewardsController;
    [SerializeField]
    private RectTransform m_tutorialButtonLayout;
    [SerializeField]
    private RectTransform m_gameButtonLayout;
    [SerializeField]
    private GameObject m_confettiBurstFx;
    [SerializeField]
    private GameObject m_confettiLoopFx;

    public void initialize(GameController p_controller)
    {
        m_controller = p_controller;
        m_playerData = m_controller.playerData;
        open();
    }

    private void open()
    {
        m_tutorialButtonLayout.gameObject.SetActive(false);
        m_gameButtonLayout.gameObject.SetActive(false);
        m_confettiBurstFx.gameObject.SetActive(true);
        m_confettiLoopFx.gameObject.SetActive(true);

        m_starFxController.initialize(m_controller);
        m_rewardsController.initialize(m_controller);

        gameObject.SetActive(true);
        m_openSequence = DOTween.Sequence();

        RectTransform l_buttonLayout = m_playerData.content.tutorial.complete ? 
            m_gameButtonLayout : m_tutorialButtonLayout;
        
        m_openSequence.Append(transform.DOScale(0.1f, 0.25f).From().SetEase(Ease.OutBack));
        m_openSequence.Append(m_starFxController.updateStars());
        m_openSequence.AppendCallback(m_rewardsController.revealChest);
        m_openSequence.AppendInterval(1f);
        m_openSequence.Append(l_buttonLayout.DOScale(0.1f, 0.25f).From().SetEase(Ease.OutBack)
            .OnStart(() => l_buttonLayout.gameObject.SetActive(true)));
    }

    public void tutorialNextButton()
    {
        string l_currentLevel = m_playerData.content.playerProgress.currentLevel;

        if (l_currentLevel.Equals("The Queens Orb"))
        {
            m_playerData.content.playerProgress.currentLevel = "Monster Protection"; 
        }

        m_controller.applicationManager.applicationChangeState(AppState.GAME);

    }

    private GameController m_controller;
    private Sequence m_openSequence;
    private MonsterThronePlayerData m_playerData;

}
