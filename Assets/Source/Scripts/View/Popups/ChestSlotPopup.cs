﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class ChestSlotPopup : SerializedMonoBehaviour
{
    [BoxGroup("Chest Container")][SerializeField]
    private ChestSlotContainer m_container;

    [BoxGroup("Pointer")][SerializeField]
    private RectTransform m_pointer;

    [BoxGroup("Button Groups")][SerializeField]
    private RectTransform m_waitOpenButtonGroup;
    [BoxGroup("Button Groups")][SerializeField]
    private RectTransform m_waitClosedButtonGroup;
    [BoxGroup("Button Groups")][SerializeField]
    private RectTransform m_unlockingButtonGroup;

    [BoxGroup("Unlocking Button Group")][SerializeField]
    private Text m_unlockEarlyCostLabel;
    [BoxGroup("Unlocking Button Group")][SerializeField]
    private Text m_unlockEarlyTimeLabel;

    [BoxGroup("Wait Closed Button Group")][SerializeField]
    private Text m_waitEarlyCostLabel;

    private ChestSlotData m_chestSlotData;
    private ChestSlotView m_chestSlotView;
    private GameServiceProvider m_gameServiceProvider;
    private ChestManager m_chestManager;

    public virtual void open(ChestSlotData p_chestSlotData, ChestSlotView p_chestSlotView)
    {
        m_gameServiceProvider = GameServiceProvider.Instance;
        m_chestManager = m_gameServiceProvider.chestManager;

        m_chestSlotData = p_chestSlotData;
        m_chestSlotView = p_chestSlotView;

        updateView();
    }

    #region button events

    public virtual void close()
    {
        gameObject.SetActive(false);
    }

    public virtual void startChestSlot()
    {
        if (m_chestSlotData.chestStatus == ChestSlotStatus.WAIT)
        {
            m_chestManager.startUnlock(m_chestSlotData, onCompleteUnlock);
        }
    }

    public virtual void buyChestSlot()
    {
        if (m_chestSlotData.chestStatus == ChestSlotStatus.WAIT
            || m_chestSlotData.chestStatus == ChestSlotStatus.UNLOCKING)
        {
            m_chestSlotData.chestStatus = ChestSlotStatus.EMPTY;
            m_container.updateView();
        }

        close();
    }

    #endregion

    #region Callbacks

    public void onCompleteUnlock(bool p_success)
    {
        if (p_success)
        {
            m_container.updateView();
        }
        else
        {
            // do something like an error message?
        }
      
        close();
    }

    #endregion

    #region View Helpers

    private void updateView()
    {
        changePointerPosition();
        changeState();
    }

    private void changePointerPosition()
    {
        RectTransform l_rect = m_chestSlotView.transform as RectTransform;
        m_pointer.anchoredPosition = new Vector2(l_rect.anchoredPosition.x - 13f, 
            m_pointer.anchoredPosition.y);
    }

    private void changeState()
    {
        switch (m_chestSlotData.chestStatus)
        {
            case ChestSlotStatus.WAIT:
                changeStateWait();
                break;
            case ChestSlotStatus.UNLOCKING:
                changeStateUnlocking();
                break;
        }
    }

    private void changeStateWait()
    {
        disableButtonGroups();

        if (m_container.anyChestsUnlocking)
        {
            m_waitClosedButtonGroup.gameObject.SetActive(true);
            int l_totalMinutes = TimeUtils.getHoursForUnlock(m_chestSlotData.chestType) * 60;
            m_waitEarlyCostLabel.text = TimeUtils.getGemCostOfTime(l_totalMinutes).ToString();
        }
        else
        {
            m_waitOpenButtonGroup.gameObject.SetActive(true);
        }

        gameObject.SetActive(true);
    }

    private void changeStateUnlocking()
    {
        disableButtonGroups();
        m_unlockingButtonGroup.gameObject.SetActive(true);
        m_unlockEarlyCostLabel.text = m_chestSlotData.getUnlockEarlyCost().ToString();
        m_unlockEarlyTimeLabel.text = TimeUtils.getTimeFormat(m_chestSlotData.getTimeLeft());
        gameObject.SetActive(true);
    }

    private void disableButtonGroups()
    {
        m_waitOpenButtonGroup.gameObject.SetActive(false);
        m_waitClosedButtonGroup.gameObject.SetActive(false);
        m_unlockingButtonGroup.gameObject.SetActive(false);
    }

    #endregion
}
