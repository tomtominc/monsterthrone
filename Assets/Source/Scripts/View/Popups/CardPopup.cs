﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardPopup : MonoBehaviour
{
    [SerializeField]
    private Button m_closeButton;
    [SerializeField]
    private Button m_upgradeButton;
    [SerializeField]
    private Button m_useButton;
    [SerializeField]
    private RectTransform m_upgradeButtonLayout;
    [SerializeField]
    private RectTransform m_useButtonLayout;
    [SerializeField]
    private RectTransform m_lockedButtonLayout;
    [SerializeField]
    private RectTransform m_noneButtonLayout;

    [SerializeField]
    private CardStateView m_cardView;
    [SerializeField]
    private AbilityStateView m_abilityView;

    private GameServiceProvider m_serviceProvider;
    private MonsterThronePlayerData m_playerData;
    private CardStateModel m_cardState;
    private CardStateModel m_abilityState;
    private CollectionMenu m_collectionMenu;

    private bool m_inDeck
    {
        get { return m_playerData.content.deck.contains(m_cardState.card.name); }
    }

    public virtual void open(GameServiceProvider p_serviceProvider, CardStateModel p_cardState)
    {
        m_serviceProvider = p_serviceProvider;
        m_cardState = p_cardState;
        m_playerData = p_serviceProvider.playerService.playerData as MonsterThronePlayerData;
        m_abilityState = p_cardState.abilityState;
    }

    public virtual void updateView()
    {
        m_cardView.initialize(m_serviceProvider, m_cardState);
        m_cardView.updateView();

        m_abilityView.initialize(m_serviceProvider, m_abilityState);

        m_lockedButtonLayout.gameObject.SetActive(false);
        m_useButtonLayout.gameObject.SetActive(false);
        m_upgradeButtonLayout.gameObject.SetActive(false);
        m_noneButtonLayout.gameObject.SetActive(false);

        if (!m_cardState.unlocked)
        {
            m_lockedButtonLayout.gameObject.SetActive(true);
            m_abilityView.updateView();
        }
        else if (m_cardState.upgradable)
        {
            m_upgradeButtonLayout.gameObject.SetActive(true);
            m_abilityView.updateViewUpgrade();
        }
        else if (m_inDeck)
        {
            m_noneButtonLayout.gameObject.SetActive(true);
            m_abilityView.updateView();
        }
        else
        {
            m_upgradeButtonLayout.gameObject.SetActive(false);
            m_abilityView.updateView();
        }

        setButtonEvents();

        gameObject.SetActive(true);
    }

    public virtual void updateViewCollectionMenu(CollectionMenu p_collectionMenu)
    {
        m_collectionMenu = p_collectionMenu;

        updateView();
    }

    public void close()
    {
        gameObject.SetActive(false);
    }

    public void useButton()
    {
        m_collectionMenu.setActiveCardState(m_cardState);
        m_collectionMenu.switchStates(CollectionMenu.State.EDIT);
        close();
    }

    private void setButtonEvents()
    {
        m_closeButton.onClick.RemoveAllListeners();
        m_upgradeButton.onClick.RemoveAllListeners();
        m_useButton.onClick.RemoveAllListeners();

        m_closeButton.onClick.AddListener(close);
        m_useButton.onClick.AddListener(useButton);
    }
}
