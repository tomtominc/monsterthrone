﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RewardPopup : MonoBehaviour
{
    [SerializeField]
    private RewardDataView m_rewardDataView;
    [SerializeField]
    private RewardCardView m_rewardCardView;
    [SerializeField]
    private RectTransform m_chest;
    [SerializeField]
    private RectTransform m_chestShadow;

    private ViewController m_controller;
    private Vector2 m_chestSize;
    private Vector2 m_chestPosition;
    private List < RewardData > m_rewards;
    private int m_currentReward;

    private Sequence m_cardAnimateSequence;

    private bool m_readyForRewards;
    private bool m_playingCardAnimation;

    public virtual void open(ViewController p_controller, List < RewardData > p_rewards)
    {
        gameObject.SetActive(true);

        m_controller = p_controller;
        m_chestPosition = m_chest.anchoredPosition;
        m_chest.anchoredPosition = new Vector2(0f, 200f);
        m_chestSize = m_chest.sizeDelta;

        m_rewardDataView.gameObject.SetActive(false);
        m_rewardCardView.gameObject.SetActive(false);

        m_rewards = p_rewards;
        m_currentReward = 0;

        animateChestIn();
    }

    public void close()
    {
        gameObject.SetActive(false);
    }

    public void onClick()
    {
        if (m_cardAnimateSequence == null)
            return;

        if (m_cardAnimateSequence.IsActive())
        {
            m_cardAnimateSequence.Complete(true);
            return;
        }

        if (m_currentReward >= m_rewards.Count)
        {
            close();
            return;
        }

        openReward();
    }

    private void animateChestIn()
    {
        float l_power = 0.2f;
        float l_fallDuration = 0.5f;
        float l_punchDuration = 1f;
        Vector2 l_stretchSize = new Vector2(m_chestSize.x * (1f - l_power), m_chestSize.y * (1f + l_power));

        Sequence l_sequence = DOTween.Sequence();

        // fall 
        l_sequence.Append(m_chest.DOAnchorPos(m_chestPosition, l_fallDuration).SetEase(Ease.InQuad));

        // tween the shadow back to normal
        l_sequence.Join(m_chest.DOScale(Vector3.one, l_fallDuration).SetEase(Ease.InQuad));

        // stretch for half the fall
        l_sequence.Join(m_chest.DOSizeDelta(l_stretchSize, l_fallDuration * 0.5f).SetEase(Ease.Linear));

        // go normal for the rest
        l_sequence.Join(m_chest.DOSizeDelta(m_chestSize, l_fallDuration * 0.5f)
            .SetDelay(l_fallDuration * 0.5f).SetEase(Ease.Linear));

        // slight movement
        // recover wiggle
        l_sequence.Append(m_chest.DOPunchScale(new Vector2(0.15f, -0.15f), l_punchDuration, 5, 10f));

        l_sequence.OnComplete(() => openReward());
    }

    private void openReward()
    {
        if (m_currentReward >= m_rewards.Count)
            return;

        RewardData l_reward = m_rewards[m_currentReward];

        m_rewardDataView.close();

        m_rewardDataView.initialize(m_controller, l_reward);
        m_rewardCardView.initialize(m_controller, l_reward);
        m_rewardCardView.cardBack.gameObject.SetActive(true);

        RectTransform l_card = m_rewardCardView.transform as RectTransform;

        float l_moveTime = 0.4f;
        l_card.anchoredPosition = new Vector2(0, -80f);
        l_card.gameObject.SetActive(true);


        m_cardAnimateSequence = DOTween.Sequence();

        m_cardAnimateSequence.Append(l_card.DOAnchorPos(new Vector2(0, -19f), l_moveTime));
        m_cardAnimateSequence.Append(l_card.DOAnchorPos(new Vector2(31f, 29f), l_moveTime));
        m_cardAnimateSequence.Join(l_card.DOScale(new Vector3(0f, 1f, 1f), l_moveTime * 0.5f));
        m_cardAnimateSequence.Join(l_card.DOScale(new Vector3(1f, 1f, 1f), l_moveTime * 0.5f)
            .SetDelay(l_moveTime * 0.5f).OnStart(() => m_rewardCardView.cardBack.gameObject.SetActive(false)));
        m_cardAnimateSequence.AppendCallback(() => m_rewardDataView.open());
        m_cardAnimateSequence.Append(l_card.DOAnchorPos(new Vector2(-31f, 29f), l_moveTime * 0.5f));

        m_cardAnimateSequence.Append(m_rewardDataView.animateView());

        m_currentReward++;
    }

}
