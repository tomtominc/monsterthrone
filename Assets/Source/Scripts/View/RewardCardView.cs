﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardCardView : MonoBehaviour
{
    [SerializeField]
    private Text m_countLabel;
    [SerializeField]
    private RectTransform m_cardBack;

    private ViewController m_controller;
    private MonsterThronePlayerData m_playerData;

    private CardStateView m_cardStateView;
    private RewardData m_rewardData;
    private CardStateModel m_cardState;

    public RectTransform cardBack
    {
        get { return m_cardBack; }
    }

    public void initialize(ViewController p_controller, RewardData p_rewardData)
    {
        m_controller = p_controller;
        m_playerData = m_controller.playerData;
        m_rewardData = p_rewardData;
        m_cardState = m_playerData.content.cardStateDatabase.getCard(p_rewardData.id);
        m_countLabel.text = string.Format("X{0}", m_rewardData.value);
        m_cardStateView = GetComponent < CardStateView >();
        m_cardStateView.initialize(p_controller.gameServiceProvider, m_cardState);
        m_cardStateView.updateView();
    }
}
