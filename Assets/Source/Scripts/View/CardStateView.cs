﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardStateView : MonoBehaviour
{
    [SerializeField]
    private Text m_nameLabel;
    [SerializeField]
    private UISpriteText m_costLabel;
    [SerializeField]
    private UISpriteText m_powerLabel;
    [SerializeField]
    private UISpriteText m_hitPointsLabel;
    [SerializeField]
    private UISpriteText m_levelLabel;
    [SerializeField]
    private UIEnergyBar m_experienceBar;
    [SerializeField]
    private Text m_description;
    [SerializeField]
    private Image m_portrait;

    private CardStateModel m_cardState;

    #region Properties

    public UISpriteText powerLabel
    {
        get { return m_powerLabel; }
    }

    public UISpriteText hitPointsLabel
    {
        get { return m_hitPointsLabel; }
    }

    #endregion

    public void initialize(GameServiceProvider p_serviceProvider, CardStateModel p_cardState)
    {
        m_cardState = p_cardState;
    }

    public void updateView()
    {
        if (m_nameLabel != null)
            m_nameLabel.text = m_cardState.card.name;
        
        if (m_costLabel != null)
            m_costLabel.Text = m_cardState.card.cost.ToString();

        if (m_powerLabel != null)
            m_powerLabel.Text = m_cardState.power.ToString();

        if (m_powerLabel != null && m_cardState.power > m_cardState.cardLevel.power)
            m_powerLabel.setColor(Color.green);
        else if (m_powerLabel != null && m_cardState.power < m_cardState.cardLevel.power)
            m_powerLabel.setColor(Color.red);
        else if (m_powerLabel != null)
            m_powerLabel.setColor(Color.white);

        if (m_hitPointsLabel != null)
            m_hitPointsLabel.Text = m_cardState.hitPoints.ToString();

        if (m_hitPointsLabel != null && m_cardState.hitPoints > m_cardState.cardLevel.hitPoints)
            m_hitPointsLabel.setColor(Color.green);
        else if (m_hitPointsLabel != null && m_cardState.hitPoints < m_cardState.cardLevel.hitPoints)
            m_hitPointsLabel.setColor(Color.red);
        else if (m_hitPointsLabel != null)
            m_hitPointsLabel.setColor(Color.white);

        if (m_levelLabel != null)
            m_levelLabel.Text = m_cardState.level.ToString();

        if (m_experienceBar != null)
            m_experienceBar.SetSliderValues(0, m_cardState.experienceForNextLevel, m_cardState.experience);

        if (m_description != null)
            m_description.text = m_cardState.getDescription();
        
        if (m_portrait != null)
            m_portrait.sprite = m_cardState.card.getPortrait();
            
    }
}
