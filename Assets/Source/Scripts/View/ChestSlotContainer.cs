﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class ChestSlotContainer : MonoBehaviour
{
    private ViewController m_viewController;
    private List < ChestSlotData > m_chestData;
    private List < ChestSlotView > m_chestViews;

    #region Properties

    public bool anyChestsUnlocking
    {
        get { return m_chestData.Any(x => x.chestStatus == ChestSlotStatus.UNLOCKING); }
    }

    public GridLayoutGroup gridLayout
    {
        get { return GetComponent < GridLayoutGroup>(); }
    }

    #endregion

    public void init(ViewController p_viewController)
    {
        m_viewController = p_viewController;
        m_chestData = m_viewController.playerData.content.chestSlots;
        m_chestViews = transform.GetComponentsInChildren<ChestSlotView>(true).ToList();
        updateView();
    }

    public void updateView()
    {
        m_viewController.save();

        for (int i = 0; i < m_chestData.Count; i++)
        {
            ChestSlotView l_view = m_chestViews[i];
            l_view.UpdateView(this, m_chestData[i]);
        }
    }

    public void update()
    {
        m_chestViews.ForEach(l_chest => l_chest.update());
    }
}
