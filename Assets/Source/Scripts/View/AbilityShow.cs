﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TextFx;
using DG.Tweening;

public class AbilityShow : MonoBehaviour
{

    [SerializeField]
    private Image m_monsterPortrait;
    [SerializeField]
    private TextFxUGUI m_abilityName;
    [SerializeField]
    private Image m_abilityScrollBackground;
    [SerializeField]
    private Image m_background;
    [SerializeField]
    private RectTransform m_maskRect;

    private CardStateModel m_cardModel;
    private CardStateModel m_abilityModel;

    public Sequence showAbility(CardStateModel p_cardModel)
    {
        m_cardModel = p_cardModel;
        m_abilityModel = m_cardModel.abilityState;

        m_monsterPortrait.sprite = m_cardModel.card.getPortrait();    
        m_abilityName.text = m_abilityModel.id;

        m_abilityScrollBackground.material.mainTextureOffset = new Vector2(0f, 0f);
        m_abilityScrollBackground.material.DOOffset(new Vector2(1f, 0f), 0.1f).SetLoops(-1, LoopType.Restart)
            .SetEase(Ease.Linear);
        
        Sequence m_showSequence = DOTween.Sequence();
        m_showSequence.Append(m_maskRect.DOSizeDelta(new Vector2(m_maskRect.sizeDelta.x, 48f), 0.5f));
        m_showSequence.Join(m_background.DOFade(0.5f, 0.5f));
        m_showSequence.Join(((RectTransform)m_monsterPortrait.transform).DOAnchorPos(new Vector2(60f, 0f), 0.5f)
            .SetDelay(0.25f).SetEase(Ease.OutBack));
        m_showSequence.AppendCallback(() => m_abilityName.AnimationManager.PlayAnimation());
        m_showSequence.AppendInterval(2f);
        m_showSequence.Append(((RectTransform)m_monsterPortrait.transform).DOAnchorPos(new Vector2(160f, 0f), 0.5f)
            .SetEase(Ease.InBack));
        m_showSequence.Join(m_background.DOFade(0f, 0.5f));
        m_showSequence.Join(m_maskRect.DOSizeDelta(new Vector2(m_maskRect.sizeDelta.x, 0f), 0.5f)
            .SetDelay(0.25f));
        m_showSequence.AppendCallback(() => m_abilityName.AnimationManager.ResetAnimation());

        return m_showSequence;
    }
}
