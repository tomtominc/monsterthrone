﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class StarterCard : MonoBehaviour
{
    [SerializeField]
    private Button m_cardButton;
    [SerializeField]
    private Button m_chooseButton;
    [SerializeField]
    private Text m_monsterName;
    [SerializeField]
    private AbilityStateView m_abilityPopup;
    [SerializeField]
    private RectTransform m_pointer;
    [SerializeField]
    private Image m_lockedOverlay;

    private event Action<StarterCard> cardButtonAction = delegate {};
    private event Action<StarterCard> chooseButtonAction = delegate {};

    public void initialize(ViewController p_controller, string p_cardId)
    {
        m_controller = p_controller;
        m_playerModel = m_controller.playerData;
        m_monster = m_playerModel.content.cardStateDatabase.getCard(p_cardId);
        m_ability = m_monster.abilityState;
        m_rect = transform as RectTransform;

        m_monsterView = GetComponent < CardStateView >();
        m_monsterView.initialize(m_controller.gameServiceProvider, m_monster);
        m_monsterView.updateView();
    }

    public void setupAbilityPopup()
    {
        m_monsterName.text = m_monster.id;
        m_abilityPopup.initialize(m_controller.gameServiceProvider, m_ability);
        m_abilityPopup.updateView();
        m_pointer.anchoredPosition = new Vector2(m_rect.anchoredPosition.x, m_pointer.anchoredPosition.y);
    }

    public void chooseCard()
    {
        Sequence l_sequence = DOTween.Sequence();
        m_rect.SetAsLastSibling();
        l_sequence.Append(m_rect.DOPunchScale(Vector3.one, 0.5f, 3, 10f));
        l_sequence.Append(m_rect.DOAnchorPos(Vector2.zero, 0.5f).SetEase(Ease.InBack));
        l_sequence.AppendInterval(1f);
        l_sequence.Append(m_rect.DOAnchorPos(new Vector2(0f, -200f), 0.5f).SetEase(Ease.InBack));
    }

    public void removeCard()
    {
        Sequence l_sequence = DOTween.Sequence();
        l_sequence.Append(m_lockedOverlay.DOFade(1f, 0.5f));
        l_sequence.Join(m_rect.DOScale(0.8f, 0.5f));
        l_sequence.Append(m_rect.DOAnchorPos(new Vector2(m_rect.anchoredPosition.x, 160f), 0.5f));
    }

    public void cardButtonPressed()
    {
        cardButtonAction(this);
    }

    public void chooseButtonPressed()
    {
        chooseButtonAction(this);
    }

    public void addCardButtonAction(Action < StarterCard > p_action)
    {
        cardButtonAction += p_action;
    }

    public void addChooseButtonAction(Action < StarterCard > p_action)
    {
        chooseButtonAction += p_action;
    }

    public void removeCardButtonAction(Action < StarterCard > p_action)
    {
        cardButtonAction -= p_action;
    }

    public void removeChooseButtonAction(Action < StarterCard > p_action)
    {
        chooseButtonAction -= p_action;
    }

    public CardStateModel getMonsterCard()
    {
        return m_monster;
    }

    private CardStateModel m_monster;
    private CardStateModel m_ability;
    private ViewController m_controller;
    private CardStateView m_monsterView;
    private AbilityStateView m_abilityView;
    private RectTransform m_rect;
    private MonsterThronePlayerData m_playerModel;
}
