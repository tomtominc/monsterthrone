﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using System;

public class ChestSlotView : MonoBehaviour
{
    #region Members

    [BoxGroup("Status Views")][SerializeField]
    private RectTransform m_emptyStatusView;
    [BoxGroup("Status Views")][SerializeField]
    private RectTransform m_waitStatusView;
    [BoxGroup("Status Views")][SerializeField]
    private RectTransform m_unlockingStatusView;
    [BoxGroup("Status Views")][SerializeField]
    private RectTransform m_readyStatusView;

    [BoxGroup("Chest Properties")][SerializeField]
    private RectTransform m_chest;
    [BoxGroup("Chest Properties")][SerializeField]
    private RectTransform m_chestLabel;

    [BoxGroup("Popup Properties")][SerializeField]
    private ChestSlotPopup m_chestPopup;

    [BoxGroup("Wait Status Properties")][SerializeField]
    private RectTransform m_handPointer;
    [BoxGroup("Wait Status Properties")][SerializeField]
    private RectTransform m_lockedLabel;
    [BoxGroup("Wait Status Properties")][SerializeField]
    private Text m_constantTimeLabel;

    [BoxGroup("Unlocking Status Properties")][SerializeField]
    private RectTransform m_clockIcon;
    [BoxGroup("Unlocking Status Properties")][SerializeField]
    private RectTransform m_timeBackground;
    [BoxGroup("Unlocking Status Properties")][SerializeField]
    private RectTransform m_timeLabel;
    [BoxGroup("Unlocking Status Properties")][SerializeField]
    private Text m_unlockEarlyLabel;

    private ChestSlotContainer m_container;
    private ChestSlotData m_chestSlotData;
    private Dictionary < ChestSlotStatus, RectTransform > m_statusViews;

    private Vector2 m_chestPositionLower = new Vector2(-1f, -4f);
    private Vector2 m_chestPositionUpper = new Vector2(-1f, 10f);
    private Sequence m_jumpSeqeunce;

    #endregion

    #region Properties

    public ChestSlotData chestSlotData
    {
        get { return m_chestSlotData; }
    }

    #endregion

    #region View Updaters

    public void UpdateView(ChestSlotContainer p_container, ChestSlotData p_chestSlotData)
    {
        m_container = p_container;
        m_chestSlotData = p_chestSlotData;

        m_statusViews = new Dictionary < ChestSlotStatus, RectTransform >()
        {
            { ChestSlotStatus.EMPTY, m_emptyStatusView },
            { ChestSlotStatus.WAIT, m_waitStatusView },
            { ChestSlotStatus.UNLOCKING, m_unlockingStatusView },
            { ChestSlotStatus.READY, m_readyStatusView }
        };

        UpdateView();
    }

    public void UpdateView()
    {
        switch (m_chestSlotData.chestStatus)
        {
            case ChestSlotStatus.EMPTY:
                goToEmptyStatus();
                break;
            case ChestSlotStatus.WAIT:
                goToWaitStatus();
                break;
            case ChestSlotStatus.UNLOCKING:
                goToUnlockingStatus();
                break;
            case ChestSlotStatus.READY:
                goToReadyStatus();
                break;
        }
    }

    #endregion

    #region State Updaters

    private void goToEmptyStatus()
    {
        disableAllViews();
        enableView(ChestSlotStatus.EMPTY);

        m_chest.gameObject.SetActive(false);
        m_chestLabel.gameObject.SetActive(false);
    }

    private void goToWaitStatus()
    {
        disableAllViews();
        enableView(ChestSlotStatus.WAIT);

        m_chest.gameObject.SetActive(true);
        m_chest.anchoredPosition = m_chestPositionLower;
        m_chest.SetParent(m_waitStatusView, true);
        m_chest.SetSiblingIndex(0);
        m_chestLabel.gameObject.SetActive(true);

        m_handPointer.anchoredPosition = new Vector2(-10f, 10f);
        m_constantTimeLabel.text = string.Format("{0}H", TimeUtils.getHoursForUnlock(m_chestSlotData.chestType));

        if (m_container.anyChestsUnlocking)
        {
            m_handPointer.gameObject.SetActive(false);   
            m_lockedLabel.gameObject.SetActive(true);
        }
        else
        {
            m_handPointer.gameObject.SetActive(true);
            m_lockedLabel.gameObject.SetActive(false);

            m_handPointer.DOAnchorPos(new Vector2(-7.5f, 7.5f), 0.25f)
                .SetEase(Ease.Linear)
                .SetLoops(-1, LoopType.Yoyo);
        }
      
    }

    private void goToUnlockingStatus()
    {
        disableAllViews();
        enableView(ChestSlotStatus.UNLOCKING);

        m_chest.gameObject.SetActive(true);
        m_chestLabel.gameObject.SetActive(false);

        m_chest.SetParent(m_unlockingStatusView, true);
        m_chest.SetSiblingIndex(0);
        m_chest.DOAnchorPos(m_chestPositionUpper, 0.25f)
            .SetEase(Ease.OutBack);

        m_clockIcon.anchoredPosition = new Vector2(16f, 2f);
        m_clockIcon.DOAnchorPos(new Vector2(-16f, 2f), 0.25f)
            .SetEase(Ease.OutBack);
        m_timeBackground.sizeDelta = new Vector2(0f, 7f);
        m_timeBackground.DOSizeDelta(new Vector2(34f, 7f), 0.25f)
            .SetEase(Ease.OutBack);

    }

    private void goToReadyStatus()
    {
        disableAllViews();
        enableView(ChestSlotStatus.READY);

        m_chest.gameObject.SetActive(true);
        m_chest.SetParent(transform, true);
        m_chest.SetAsLastSibling();
        m_chest.anchoredPosition = m_chestPositionUpper;
        m_jumpSeqeunce = m_chest.DoJumpSequence(0.2f, 1.52f, 20f);
        m_jumpSeqeunce.PrependInterval(0.5f);

//        m_statusViews[ChestSlotStatus.READY]
//            .DOSizeDelta(Vector2.one * 2f, 0.5f)
//            .SetEase(Ease.InOutBack)
//            .SetLoops(-1, LoopType.Yoyo);
    }

    #endregion

    #region State Update

    public void update()
    {
        switch (m_chestSlotData.chestStatus)
        {
            case ChestSlotStatus.UNLOCKING:
                updateUnlockStatus();
                break;
        }
    }

    private void updateUnlockStatus()
    {
        TimeSpan l_timeLeft = m_chestSlotData.getTimeLeft();

        if (l_timeLeft.CompareTo(TimeSpan.Zero) <= 0)
        {
            m_chestSlotData.chestStatus = ChestSlotStatus.READY;
            UpdateView();
        }
        else
        {
            Text l_timeLabel = m_timeLabel.GetComponent<Text>();
            l_timeLabel.text = TimeUtils.getTimeFormat(l_timeLeft);
            m_unlockEarlyLabel.text = TimeUtils.getGemCostOfTime((int)l_timeLeft.TotalMinutes).ToString();
        }
    }

    #endregion

    #region Button Callbacks

    public void openChestPopup()
    {
        m_chestPopup.open(m_chestSlotData, this);
    }

    #endregion

    #region Helpers

    private void disableAllViews()
    {
        m_statusViews.Values.ToList().ForEach(x => x.gameObject.SetActive(false));
    }

    private void enableView(ChestSlotStatus p_status)
    {
        m_statusViews[p_status].gameObject.SetActive(true);
    }

    #endregion
}
