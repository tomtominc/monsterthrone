﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectView : MonoBehaviour
{
    [SerializeField]
    private Text m_castleLabel;
    [SerializeField]
    private Text m_chapterLabel;
    [SerializeField]
    private RectTransform m_levelContent;
    [SerializeField]
    private RectTransform m_levelViewPrefab;
    [SerializeField]
    private ScrollRect m_scrollRect;

    [SerializeField]
    private Button m_nextButton;
    [SerializeField]
    private Button m_previousButton;

    private LevelSelectController m_controller;
    private MonsterThroneGameModel m_gameModel;
    private MonsterThronePlayerData m_playerData;
    private PlayerProgress m_playerProgress;
    private CastleProgress m_castleProgress;
    private ChapterProgress m_chapterProgress;
    private CastleSettings m_castle;
    private Chapter m_chapter;

    public void initialize(LevelSelectController p_controller)
    {
        m_controller = p_controller;
        m_playerData = p_controller.playerData;
        m_playerProgress = m_playerData.content.playerProgress;
        m_gameModel = p_controller.gameModel;
        m_castleProgress = m_playerProgress.getCastleProgress(m_playerProgress.currentCastle);

        updateChapter(m_playerProgress.currentChapter);
    }

    public void updateChapter(string p_chapter)
    {
        m_scrollRect.verticalNormalizedPosition = 1;
        m_castle = m_gameModel.getCastle(m_castleProgress.name);

        if (string.IsNullOrEmpty(p_chapter))
            p_chapter = m_castle.chapters[0].name;
                
        m_chapter = m_castle.getChapter(p_chapter);
        m_chapterProgress = m_castleProgress.getChapterProgress(m_chapter.name);
        m_levelContent.DestroyChildren();

        m_levelContent.sizeDelta = new Vector2(m_levelContent.sizeDelta.x, 
            (m_chapter.levels.Count * (m_levelViewPrefab.sizeDelta.y + 4f)) + 4f);

        for (int i = 0; i < m_chapter.levels.Count; i++)
        {
            Level l_level = m_chapter.levels[i];

            LevelProgress l_levelProgress = m_chapterProgress.getLevelProgress(l_level.name);
            RectTransform l_levelViewRect = Instantiate(m_levelViewPrefab);
            l_levelViewRect.SetParent(m_levelContent, false);
            LevelView l_levelView = l_levelViewRect.GetComponent < LevelView >();
            l_levelView.initialize(i, l_level, l_levelProgress);
            l_levelView.button.onClick.AddListener
            (
                () =>
                {
                    m_controller.changeCastle(m_castle, m_chapter, l_level);
                    m_controller.goToGame();
                }
            );
        }

        int l_chapterIndex = m_castle.getChapterIndex(m_chapter);

        m_chapterLabel.text = string.Format("Chapter {0}", l_chapterIndex + 1);

        if (l_chapterIndex >= m_castle.chapters.Count - 1)
            m_nextButton.gameObject.SetActive(false);
        else
            m_nextButton.gameObject.SetActive(true);
        
        if (l_chapterIndex <= 0)
            m_previousButton.gameObject.SetActive(false);
        else
            m_previousButton.gameObject.SetActive(true);
    }

    public void nextChapter()
    {
        int l_chapterIndex = m_castle.getChapterIndex(m_chapter);
        m_chapter = m_castle.chapters[++l_chapterIndex];
        updateChapter(m_chapter.name);
    }

    public void previousChapter()
    {
        int l_chapterIndex = m_castle.getChapterIndex(m_chapter);
        m_chapter = m_castle.chapters[--l_chapterIndex];
        updateChapter(m_chapter.name);
    }
}
