﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using TextFx;

public class GameCardView : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    protected RectTransform m_container;
    [SerializeField]
    protected RectTransform m_cardFront;
    [SerializeField]
    protected RectTransform m_cardBack;
    [SerializeField]
    protected Animator m_cardAnimator;
    [SerializeField]
    protected Image m_cardOverlay;
    [SerializeField]
    protected Sprite m_cardActiveSprite;
    [SerializeField]
    protected Sprite m_cardInactiveSprite;
    [SerializeField]
    protected GameObject m_damagedOverlay;
    [SerializeField]
    protected GameObject m_damagedInfo;
    [SerializeField]
    protected TextFxUGUI m_damagedLabel;
    [SerializeField]
    protected GameObject m_knockedOutOverlay;
    [SerializeField]
    protected TextFxUGUI m_knockedOutLabel;
  
    private GameController m_gameController;
    private GameStateManager m_gameStateManager;
    private GameHud m_gameHud;
    private GameWorld m_gameWorld;
    private CardStateModel m_cardState;
    private CardStateView m_cardStateView;
    private CanvasGroup m_canvasGroup;

    private Sequence m_moveInSequence;
    private Sequence m_damageSequence;
    private bool m_playable;



    #region Public Properties

    public CardStateModel cardState
    {
        get { return m_cardState; }
    }

    public bool playable
    {
        get { return m_playable; }
    }

    public int siblingIndex
    {
        get { return transform.GetSiblingIndex(); }
        set { transform.SetSiblingIndex(value); }
    }

    public Vector3 position
    {
        get { return transform.position; }
    }

    #endregion

    public void initialize(GameController p_gameController, CardStateModel p_cardState)
    {
        m_gameController = p_gameController;
        m_gameStateManager = m_gameController.gameStateManager;
        m_gameWorld = m_gameController.gameWorld;
        m_gameHud = m_gameController.gameHud;
        m_cardState = p_cardState;
        m_cardStateView = GetComponent < CardStateView >();
        m_canvasGroup = GetComponent < CanvasGroup >();
        m_cardStateView.initialize(m_gameController.gameServiceProvider, m_cardState);
    }

    public void updateView()
    {
        m_cardStateView.updateView();

        bool l_knockedOut = m_cardState.hitPoints <= 0;
        m_knockedOutLabel.gameObject.SetActive(l_knockedOut);
        m_knockedOutOverlay.gameObject.SetActive(l_knockedOut);
        m_cardStateView.powerLabel.gameObject.SetActive(!l_knockedOut);
        m_cardStateView.hitPointsLabel.gameObject.SetActive(!l_knockedOut);
    }

    public void setCardActive()
    {
        Image l_front = m_cardFront.GetComponent < Image >();
        l_front.sprite = m_cardActiveSprite;
    }

    public virtual void setCardInactive()
    {
        Image l_front = m_cardFront.GetComponent < Image >();
        l_front.sprite = m_cardInactiveSprite;
    }

    public virtual void resetContainerPosition()
    {
        m_container.anchoredPosition = Vector2.zero;
    }

    public virtual void setEssenceAmount(int p_essenceAmount)
    {
        m_playable = m_cardState.card.cost <= p_essenceAmount;

        if (m_playable && !m_gameHud.activeCardView.Equals(this))
        {
            m_cardAnimator.Play("PLAYABLE");
        }
        else
        {
            m_cardAnimator.Play("NONE");
        }
    }

    #region Animations

    public Sequence moveIn()
    {
        m_cardBack.gameObject.SetActive(true);
        m_cardFront.gameObject.SetActive(false);

        m_moveInSequence = DOTween.Sequence();
        m_moveInSequence.Append(m_cardBack.
            DOAnchorPos(new Vector2(160f, m_cardBack.anchoredPosition.y), 0.5f)
            .SetEase(Ease.OutSine).From());

        m_moveInSequence.Append(m_cardBack.DOScaleX(0f, 0.25f));

        m_moveInSequence.AppendCallback(() =>
            {
                m_cardBack.gameObject.SetActive(false);
                m_cardFront.gameObject.SetActive(true);
            });

        m_moveInSequence.Append(m_cardFront.DOScaleX(0f, 0.25f).From());
        return m_moveInSequence;
    }

    public Sequence moveInHand(GameCardView p_placement)
    {
        Sequence l_sequence = DOTween.Sequence();
        Vector3 l_endPosition = p_placement.position;
        float l_duration = 0.5f;
        float l_scaleDuration = 0.1f;

        l_sequence.Append(m_container.DOScaleX(0.8f, l_scaleDuration));
        l_sequence.Join(m_container.DOMove(l_endPosition, l_duration));
        l_sequence.Join(m_container.DOScaleX(1f, l_scaleDuration).SetDelay(l_duration - l_scaleDuration));

        return l_sequence;
    }


    public Sequence setAsActiveCard()
    {
        setCardActive();

        Sequence l_activeCardSequence = DOTween.Sequence();
        float l_duration = 0.25f;

        m_container.SetParent(transform, true);
        m_container.anchoredPosition = Vector2.zero;
        m_canvasGroup.alpha = 0;

        l_activeCardSequence.Append(m_canvasGroup.DOFade(1f, l_duration));
        l_activeCardSequence.Join(m_cardOverlay.DOFade(0f, l_duration));
        l_activeCardSequence.PrependInterval(1f);

        return l_activeCardSequence;

    }

    public Sequence summon()
    {
        Sequence l_summonSequence = DOTween.Sequence();

        float l_duration = 0.25f;
        Vector2 l_monsterPosition = m_gameWorld.monsterWorldPosition;

        l_summonSequence.Append(m_container.DOMove(l_monsterPosition, l_duration));
        l_summonSequence.Join(m_cardOverlay.DOFade(1f, l_duration));
        l_summonSequence.AppendCallback(() =>
            {
                m_container.gameObject.SetActive(false);
                m_gameWorld.swapSpark.Play("Execute");
            });

        l_summonSequence.AppendInterval(l_duration);
        l_summonSequence.AppendCallback(() =>
            {
                m_container.gameObject.SetActive(true); 
            });
        return l_summonSequence;
    }

    public Sequence swapCardPosition(GameCardView p_viewToSwap)
    {
        Sequence l_swapSequence = DOTween.Sequence();

        float l_duration = 0.5f;
        float l_scaleDuration = 0.25f;

        m_container.SetParent(m_gameHud.dragContainer, true);

        l_swapSequence.Append(m_container.DOScaleX(0.9f, l_scaleDuration));
        l_swapSequence.Join(m_container.DOAnchorPos(Vector2.zero, l_duration));
        l_swapSequence.Insert(l_duration - l_scaleDuration, m_container.DOScaleX(1f, l_scaleDuration));

        l_swapSequence.OnComplete 
        (() =>
            {
                int l_siblingIndex = transform.GetSiblingIndex();
                int l_otherSiblingIndex = p_viewToSwap.transform.GetSiblingIndex();

                p_viewToSwap.transform.SetSiblingIndex(l_siblingIndex);
                transform.SetSiblingIndex(l_otherSiblingIndex);

                m_container.SetParent(transform, true);
                m_container.anchoredPosition = Vector2.zero;

                setCardInactive();
            });

        return l_swapSequence;
    }

    public Sequence damage(DamageInfo p_damage)
    {
        m_damageSequence = DOTween.Sequence();

        m_damageSequence.AppendCallback(() =>
            {
                m_damagedOverlay.SetActive(true);
                m_damagedInfo.SetActive(true);
                m_damagedLabel.text = string.Format("-{0}", p_damage.amount);
            });

        m_damageSequence.Append(m_damagedInfo.transform.DOPunchScale(Vector2.one * 0.1f, 1f));

        m_damageSequence.OnComplete(() =>
            {
                m_damagedOverlay.SetActive(false);
                m_damagedInfo.SetActive(false);
                m_damagedInfo.transform.localScale = Vector3.one;
            });

        return m_damageSequence;
    }

    public Sequence heal(int p_amount)
    {
        Sequence l_healSequence = DOTween.Sequence();

        m_cardState.modifyHitPoints(p_amount);

        return l_healSequence;
    }


    #endregion

    #region Event Handlers

    public void OnBeginDrag(PointerEventData p_data)
    {
        if (m_cardState.hitPoints <= 0)
            return;
        
        if (!m_gameStateManager.isState(GameStateKey.WAIT_FOR_PLAYER_INTERACTION)
            && !m_gameStateManager.isState(GameStateKey.KNOCKED_OUT_SWAP))
            return;

        m_gameStateManager.changeState(GameStateKey.DRAG_CARD);
        m_gameHud.currentlyDraggedCard = this;
        m_container.SetParent(m_gameHud.dragContainer, true);
    }

    public void OnDrag(PointerEventData p_data)
    {
        if (m_gameHud.currentlyDraggedCard != this)
            return;

        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_container.position = mousePosition;
    }

    public void OnEndDrag(PointerEventData p_data)
    {
        if (!m_gameStateManager.isState(GameStateKey.DRAG_CARD))
            return;

        m_gameStateManager.changeState(GameStateKey.WAIT_FOR_PLAYER_INTERACTION);
        m_container.SetParent(transform, true);
        m_container.anchoredPosition = Vector2.zero;
        m_gameHud.currentlyDraggedCard = null;
    }

    #endregion
}
