﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerCutout : MonoBehaviour
{
    [SerializeField]
    private Animator m_pointer;

    public void open(string animation)
    {
        gameObject.SetActive(true);
        m_pointer.Play(animation);
    }

    public void close()
    {
        gameObject.SetActive(false);
    }
}
