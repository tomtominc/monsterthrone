﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View : MonoBehaviour
{
    protected GameServiceProvider m_serviceProvider;
    protected MonsterThronePlayerData m_playerData;

    public virtual void initialize(GameServiceProvider p_serviceProvider)
    {
        m_serviceProvider = p_serviceProvider;    
        m_playerData = p_serviceProvider.playerService.playerData as MonsterThronePlayerData;
    }
}
