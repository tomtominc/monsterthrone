﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ExperienceBar : MonoBehaviour
{
    [SerializeField]
    private Image m_fill;
    [SerializeField]
    private Image m_overlay;
    [SerializeField]
    private Sprite m_unFilledSprite;
    [SerializeField]
    private Sprite m_filledSprite;

    private Tweener m_overlayTween;
    private CardStateModel m_cardState;

    public void updateView(CardStateModel p_cardState)
    {
        m_cardState = p_cardState;

        if (m_overlayTween != null && m_overlayTween.IsActive())
        {
            m_overlayTween.Kill();
            m_overlay.color = new Color(1f, 1f, 1f, 0f);
          
        }

        if (m_cardState.experience >= m_cardState.experienceForNextLevel)
        {
            m_fill.sprite = m_filledSprite;
            m_overlayTween = m_overlay.DOFade(1f, 0.2f).SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            m_fill.sprite = m_unFilledSprite;

        }
    }

}
