﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

public class ItemView : BlockView
{
    [SerializeField]
    private List < ItemDrop > m_itemDrops;

    public override bool shouldRemoveAtBottom
    {
        get{ return true; }
    }

    public override IEnumerator doBottomGridAction()
    {
        destroy();

        yield return new WaitForSeconds(m_animator.GetCurrentAnimationLength());
    }

    public ItemDrop getItem()
    {
        return new Selector().Single(m_itemDrops);
    }

 
}

[System.Serializable]
public class ItemDrop : ISelectable
{
    public string itemId;
    public int count;
    public float dropPercent;

    public float GetSelectionWeight()
    {
        return dropPercent;
    }

    public float AdjustedSelectionWeight { get; set; }
}
