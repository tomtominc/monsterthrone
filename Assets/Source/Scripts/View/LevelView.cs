﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LevelView : MonoBehaviour
{
    [SerializeField]
    private Text m_levelTitleLabel;
    [SerializeField]
    private List < Toggle > m_stars;

    private int m_levelIndex;
    private Level m_level;
    private LevelProgress m_levelProgress;
    private Button m_button;

    public Button button
    {
        get { return m_button; }
    }

    public void initialize(int p_levelIndex, Level p_level, LevelProgress p_levelProgress)
    {
        m_levelIndex = p_levelIndex;
        m_level = p_level;
        m_levelProgress = p_levelProgress;

        updateView();

        m_button = GetComponent < Button >();

    }

    public void updateView()
    {
        m_levelTitleLabel.text = string.Format("{0}. {1}", m_levelIndex + 1, m_level.name);

        for (int i = 0; i < m_levelProgress.stars; i++)
        {
            m_stars[i].isOn = true;
        }


    }
}
