﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class Ball : MonoBehaviour
{
    [SerializeField] protected Transform m_hitSparkPrefab;
    [SerializeField] protected LayerMask m_collisionLayers;
    [SerializeField] protected AudioKey m_hitWallSound;

    protected SpriteAnimation m_animator;
    protected int m_stuckCount = 0;
    protected int m_stuckMaxTimes = 2;

    public Action<Ball> HitFloor;

    public float Speed { get; set; }

    public Vector3 Direction { get; set; }

    public LayerMask collisionLayers
    {
        get { return m_collisionLayers; }
    }

    void OnEnable()
    {
        m_animator = GetComponent < SpriteAnimation >();
        m_animator.Play("IDLE");
    }

    void FixedUpdate()
    {
        transform.position += Direction * Time.deltaTime * Speed;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Direction, Speed * Time.deltaTime * 1.2f, m_collisionLayers);

        if (!hit)
            return;

        if (hit.collider.CompareTag(Constants.HITABLE_TAG))
        {
            BlockView hitable = hit.collider.GetComponent<BlockView>();
            hitable.ballHit(this, Direction);
        }

        if (hit.collider.CompareTag(Constants.WALL_TAG))
            AudioManager.Instance.Play(m_hitWallSound, 3);

        if (hit.collider.CompareTag(Constants.FLOOR_TAG))
            return;

        if (hit.collider.CompareTag(Constants.PICKABLE_TAG))
            return;
        
        Transform l_hitSpark = Instantiate(m_hitSparkPrefab);
        l_hitSpark.position = hit.point;
        l_hitSpark.GetComponent < SpriteAnimation >().PlayWithCallBack("Hit", (x, y) => Destroy(l_hitSpark.gameObject));

        Direction = Vector3.Reflect(Direction, hit.normal);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Constants.FLOOR_TAG))
        {
            if (HitFloor != null)
                HitFloor(this);
        }
    }
			
}
