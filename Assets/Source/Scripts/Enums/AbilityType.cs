﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityType
{
    None,
    LightningExplosion,
    VerticalFireExplosion,
    HorizontalFireExplosion,
    Electrify,
    Bomber,
    BubbleWrap
}
