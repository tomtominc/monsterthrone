﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SongOfHealingAbilityController : AbilityController
{
    private bool m_finished = false;

    public override IEnumerator useAbility()
    {
        m_gameWorld.monsterView.changeState(MonsterView.State.ATTACK, songOfHealing);

        while (!m_finished)
            yield return false;

        Destroy(gameObject);
    }

    private void songOfHealing()
    {
        Sequence l_sequence = DOTween.Sequence();

        foreach (GameCardView l_cardView in m_playerHand.cardsInHand)
        {
            l_sequence.Join(l_cardView.heal(m_ability.cardLevel.healAmount));    
        }

        l_sequence.OnComplete(() => m_finished = true);
    }
}
